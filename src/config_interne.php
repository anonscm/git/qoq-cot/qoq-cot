<?php

// ----------- Pour l'accès à la base QoQ-CoT
// On commence par vérifier que toutes les constantes nécessaires à l'élaboration de la chaîne d'accès SQL_DSN ont bien été définies, condition nécessaire pour que QoQ-CoT fonctionne...
if ( ! (defined('SQL_HOST') and defined('SQL_PORT') and defined('SQL_DBNAME')) )
{
	print("Vous devez définir dans config.php SQL_HOST ET SQL_PORT ET SQL_DBNAME pour que QoQ-CoT puisse se connecter à la base de données qoq-cot et déployer ses ailes !!!\n");
	exit(0);
}
// Toutes les constantes ont donc été définies, on créé la chaîne d'accès à la base de données QoQ-CoT
define('SQL_DSN', 'mysql:host='.SQL_HOST.';port='.SQL_PORT.';dbname='.SQL_DBNAME);


// ----------- Constantes pour le calcul du taux d'utilisation annuel avec choix libre du nombre de jours ouvrés
# Combien de semaines par an, en moyenne
define('NB_SEMAINES_MOYEN_PAR_AN', 52.18);
# Combien de lundis (ou de mardis, mercredis, etc.) par mois, en moyenne
define('NB_JOURS_SEMAINE_MOYEN_PAR_MOIS', 4.35);

// ----------- Infos de version
# Pour construire dynamiquement les liens vers la doc
define('VERSION_FORMAT_WIKI', "v5.0_mac_bec");
