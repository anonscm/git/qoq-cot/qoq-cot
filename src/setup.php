<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
  /**
   * Script de récupération des salles à superviser dans l'application
   */
require_once 'config.php';
require_once 'config_interne.php';

/**
 * Affiche l'utilisation du setup dans la ligne de commande
 */
function usage()
{
    global $nb_sites,$sites;
    $liste_sites="";
    if ($nb_sites>0) {
        $liste_sites.="{";        
        for ($i=0;$i<$nb_sites;$i++) {
            $liste_sites.=$sites[$i];
            if ($i<$nb_sites-1) {
                $liste_sites.=",";
            }
        }
        $liste_sites.="}";        
    }
    print "Utilisation : php setup.php nom-du-fichier-csv $liste_sites\n";
    print "Utilisation : php setup.php -c\n";
    exit;
}

function connexion_base() {
    $dsn      = SQL_DSN;
    $user     = SQL_USERNAME;
    $password = SQL_PASSWORD;
    
    try {
        $dbh = new PDO($dsn, $user, $password);
        $dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        print 'Echec de la connexion : ' . $e->getMessage()."\n";
        exit;
    }
    return $dbh;
}

function sql_wrapper($dbh,$sql,$msg) {
    $query = $dbh->prepare($sql);
    try {
        $query->execute();
    } catch (PDOException $e) {
        printf("\e[31mER \e[39m%s : \e[33m%s\e[39m\n",$msg,$e->getMessage());
        print("$sql\n");
        exit;
    }
    printf("\e[32mOK \e[39m%s : \e[33m%d élements affectés\e[39m\n",$msg,$query->rowCount());
    return $dbh->lastInsertId();
}

if (defined('SITES')) {
	$sites=unserialize(SITES);
}  else {
	$sites=array();
}
$nb_sites=sizeof($sites);

if ($argc==2 && $argv[1]==="-c") {
    goto cache;
}

if ($argc<2 || ($argc<3 && $nb_sites>0) || ($argc>2 && $nb_sites==0) || (isset($argv[2]) && !in_array($argv[2],$sites)) || $argc>3) {
    usage();
}

if (! file_exists($argv[1])) {
	print "Le fichier $argv[1] n'existe pas !\n";
	exit;
}

if (isset($argv[2])) {
    $site=$argv[2];
} else {
    $site='';
}

$ligne = file($argv[1]);

foreach ($ligne as $l) {

    $salles[] = str_getcsv($l);
}

$i = 0;

$machineArray = array();
$NomMachine_tab = array();

foreach ($salles as $salle) {

    $machineArray[$i]['machine']    = $salle[0];
    $machineArray[$i]['composante'] = $salle[2];
    $machineArray[$i]['salle']      = $salle[1];
    if(!isset($salle[3])){
      $machineArray[$i]['Date_DEBUT'] = '1970-01-01';
    }else{
      $machineArray[$i]['Date_DEBUT'] = $salle[3];
    }
    if(!isset($salle[4])){
      $machineArray[$i]['Date_FIN'] = '2300-01-01';
    }else{
      $machineArray[$i]['Date_FIN'] =   $salle[4];
    }

    $NomMachine_tab[$i]=$salle[0];

    $i++;
}

$NbOccurrencesNomMachine_tab = array_count_values($NomMachine_tab);

$dbh = connexion_base();

$version=$dbh->query('select version()')->fetchColumn();
if ($version>='8') {
    $engine='InnoDB';
} else { 
    $engine='MyISAM';
}

$sql = 'CREATE TABLE IF NOT EXISTS `Connexions` ('.
        '`IdConnexion` bigint(20) NOT NULL auto_increment, '.
        '`DateDebutInitiale` datetime NOT NULL, '.
        '`DateDebut` datetime NOT NULL, '.
        '`HeureDebut` time NOT NULL, '.
        '`DateFin` datetime default NULL, '.
        '`HeureFin` time NOT NULL, '.
        '`Login` varchar(32) NOT NULL, '.
        '`NomMachine` varchar(64) NOT NULL, '.
        '`IPMachine` varchar(15) default NULL, '.
        '`NomOs` varchar(32) NOT NULL, '.
        '`Duree` time default NULL, '.
        '`Jour` date NOT NULL, '.
        '`JourSemaine` tinyint(4) NOT NULL, '.
        '`NomVM` varchar(64) default NULL, '.
        '`IPVM` varchar(15) default NULL, '.
        '`Pool` varchar(64) default NULL, '.
        'PRIMARY KEY  (`IdConnexion`), '.
        'UNIQUE KEY `DateMachineLogin` (`DateDebutInitiale`,`DateDebut`,`NomMachine`,`Login`), '.
        'KEY `NomMachine` (`NomMachine`) '.
        ') ENGINE='.$engine.'  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

sql_wrapper($dbh,$sql,"création de la table Connexions");

$sql = 'CREATE TABLE IF NOT EXISTS `Salles` ('.
        '`IdSalle` int NOT NULL AUTO_INCREMENT, '.
        '`NomSalle` varchar(50) NOT NULL, '.
        '`Composante` varchar(20) NOT NULL, '.
        '`Site` varchar(20) default NULL, '.
        'PRIMARY KEY (`IdSalle`), '.
        'UNIQUE KEY `NomSalle` (`NomSalle`,`Composante`,`Site`) '.
        ') ENGINE='.$engine.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

sql_wrapper($dbh,$sql,"création de la table sqlTableSalles");

$sql = 'CREATE TABLE IF NOT EXISTS `MachinesToSalles` ('.
        '`NomMachine` varchar(50) NOT NULL, '.
        '`RefSalle` int NOT NULL, '.
        '`Date_DEBUT` date NOT NULL DEFAULT \'1970-01-01\', '.
        '`Date_FIN` date NOT NULL DEFAULT \'2300-01-01\', '.
        'KEY `RefSalle` (`RefSalle`), '.
        'KEY `NomMachine` (`NomMachine`) '.
        ') ENGINE='.$engine.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

sql_wrapper($dbh,$sql,"crétion de la table MachinesToSalles");

$sql = 'CREATE TABLE IF NOT EXISTS `Admins` ('.
        '`login` varchar(20) NOT NULL '.
        ') ENGINE='.$engine.' DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';

sql_wrapper($dbh,$sql,"création de la table Admins");
 
$sql = 'DROP TRIGGER IF EXISTS `Verifie_DateFin`';
sql_wrapper($dbh,$sql,"destruction du trigger Verifie_DateFin");

$sql=  'CREATE TRIGGER `Verifie_DateFin` 
        BEFORE UPDATE ON `Connexions` 
        FOR EACH ROW 
        BEGIN 
         IF (NEW.DateFin<OLD.DateFin) THEN 
          SET @errorMsg = CONCAT("ATTENTION : tentative de piratage depuis la machine ",OLD.NomMachine);
          SIGNAL SQLSTATE "45000" SET MESSAGE_TEXT = @errorMsg;
         END IF;
        END';
sql_wrapper($dbh,$sql,"création du trigger Verifie_DateFin");

$sql = 'SELECT COUNT(*) AS total FROM `Admins`;';

$query = $dbh->prepare($sql);
$query->execute();

$results = $query->fetch(PDO::FETCH_OBJ);

# Si pas de valeur dans la table Admins alors on est à la première installation
# et on met FIRST_ADMIN, sinon on ne fait rien (on garde les admins existants)
if ( $results->total == 0) {
        $sql = sprintf("INSERT INTO `Admins` VALUES ('%s')",FIRST_ADMIN); 
	    sql_wrapper($dbh,$sql,"insertion du premier compte administrateur");
}

$sql =  'DELETE `MachinesToSalles` FROM `MachinesToSalles` LEFT JOIN `Salles` ON `RefSAlle`=`idSalle` WHERE (`Site`="'.$site.'" OR  `Site` IS NULL);';
sql_wrapper($dbh,$sql,"vidage de la table MachinesToSalle");
$sql = 'DELETE FROM `Salles` WHERE (`Site`="'.$site.'" OR  `Site` IS NULL);';
sql_wrapper($dbh,$sql,"vidage de la table Salle");

$composantes = array();

$sql = 'SELECT DISTINCT Composante FROM `Salles`;';

$query = $dbh->prepare($sql);
$query->execute();

$results = $query->fetchAll(PDO::FETCH_OBJ);

foreach ($results as $result) {
    $composantes[$result->Composante]=false;
}

$sql = '';
$i = 0;
$salleUnique = '';
$composanteUnique = '';
$idSalle = 0;

foreach ($machineArray as $salle) {

    if ($salle['salle'] !== $salleUnique || ($salle['salle'] === $salleUnique && $salle['composante'] !== $composanteUnique)) {

        if ($sql !== '') {
            sql_wrapper($dbh,$sql,sprintf("insertions de machines dans la salle %s ( %s )",$salleUnique,$composante));
        } 

        $composante = $salle['composante'];

        if ($nb_sites>0) {
            if (isset($composantes[$salle['composante']])) {
             	$composante = $salle['composante']." ".$site;
                if (! $composantes[$salle['composante']]) {
                    printf("\e[34mIN \e[39mDoublon : \e[33mLa composante %s a été renommée en %s\e[39m\n",$salle['composante'],$composante);
                    $composantes[$salle['composante']]=true;
                }
            }
            $sql = sprintf("INSERT INTO `Salles` (NomSalle,Composante,Site) VALUES ('%s','%s','%s')",$salle['salle'],$composante,$site);
        } else {
            $sql = sprintf("INSERT INTO `Salles` (NomSalle,Composante) VALUES ('%s','%s')",$salle['salle'],$composante);
        }

        $idSalle = sql_wrapper($dbh,$sql,sprintf("insertion de la salle %s ( %s )",$salle['salle'],$composante));

        $sql = 'INSERT INTO `MachinesToSalles` VALUES ';
    }
   
    if ($sql !== 'INSERT INTO `MachinesToSalles` VALUES ') {
        $sql .= ',';
    } 
    $sql .= sprintf("('%s',%d,'%s','%s')",$salle['machine'],$idSalle,$salle['Date_DEBUT'],$salle['Date_FIN']);

    $salleUnique      = $salle['salle'];
    $composanteUnique = $salle['composante'];
}
if ($sql !== '') {
     sql_wrapper($dbh,$sql,sprintf("insertions de machines dans la salle %s ( %s )",$salleUnique,$composante));
} 

print "\nConfiguration au poil de la DB MySQL réussie !!\nToutes les tables qui n'existaient pas déjà ont été créées au bon format.\nLes tables 'Salles' et 'MachinesToSalles' ont été vidées, puis remplies selon les données du fichier csv.\nYou're ready for QoQ-CoTing now !!!\n\n";
exit;

cache:

$dbh = connexion_base();

if (defined('CONNEXIONS_CACHE')) {
    $connexionsCache=CONNEXIONS_CACHE;
    $sql = 'DROP TABLE IF EXISTS Connexions_Cache';
    sql_wrapper($dbh,$sql,"destruction de la table Connexions_Cache");
    $sql = 'DROP TRIGGER IF EXISTS Insert_Cache';
    sql_wrapper($dbh,$sql,"destruction du trigger Insert_Cache");
    $sql = 'DROP TRIGGER IF EXISTS Update_Cache';
    sql_wrapper($dbh,$sql,"destruction du trigger Update_Cache");
    $sql = 'DROP EVENT IF EXISTS Purge_cache';
    sql_wrapper($dbh,$sql,"destruction de la tâche planifiée Purge_Cache");
    if ($connexionsCache>0) {
        $sql = 'CREATE TABLE Connexions_Cache LIKE Connexions';
        sql_wrapper($dbh,$sql,"création table Connexions_Cache");
        $sql = 'INSERT INTO Connexions_Cache SELECT * FROM Connexions WHERE DateDebut>=CURDATE() - INTERVAL '.$connexionsCache.' DAY';
        sql_wrapper($dbh,$sql,"remplissage de la table Connexions_Cache");
        $sql = 'CREATE TRIGGER `Insert_Cache`
                AFTER INSERT ON `Connexions`
                FOR EACH ROW 
                BEGIN 
                   INSERT INTO Connexions_Cache SELECT * FROM Connexions WHERE IdConnexion = NEW.IdConnexion;
                END';
        sql_wrapper($dbh,$sql,"création du trigger Insert_Cache");
        $sql = 'CREATE TRIGGER `Update_Cache`
                AFTER UPDATE ON `Connexions`
                FOR EACH ROW 
                BEGIN 
                   UPDATE Connexions_Cache SET DateFin=NEW.DateFin,HeureFin=NEW.HeureFin,Duree=NEW.Duree WHERE IdConnexion = NEW.IdConnexion;
                END';
        sql_wrapper($dbh,$sql,"création du trigger Update_Cache");
        $sql = 'CREATE EVENT `Purge_Cache`
                ON SCHEDULE
                    EVERY 1 DAY
                    STARTS CONCAT(DATE(NOW()+INTERVAL 1 DAY ), " 01:00:00") 
                    DO
                        DELETE FROM Connexions_Cache WHERE DateDebut<CURDATE() - INTERVAL '.$connexionsCache.' DAY';
        sql_wrapper($dbh,$sql,"création de la tâche planifiée Purge_Cache");
    }
}
