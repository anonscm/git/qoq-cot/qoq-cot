<!DOCTYPE html>
<html lang="fr">
  <head>
    <meta charset="utf-8">
    <title>QoQ-CoT Codec</title>
    <link rel="stylesheet" href="style/style.css"  type="text/css" media="screen" />
    <link rel="stylesheet" href="js/jquery-ui/css/smoothness/jquery-ui.css">
    <link rel="stylesheet" href="js/jquery-ui/css/smoothness/jquery-ui-timepicker-addon.css">
    <link rel="stylesheet" href="js/tablesorter/css/theme.default.css">
    <!-- librairie d'icones libre de droits-->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <!--[if IE]><link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /><![endif]-->
    <link rel="icon" type="image/png" href="img/favicon.png">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!--permet de changer la couleur du navigateur sur certains appareils mobile (ex: chrome Android)-->
    <meta name="theme-color" content="#e67e22" id="theme" />

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
    <script src="js/menumobile.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.css"/>
 
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.18/fc-3.2.5/fh-3.1.4/datatables.min.js"></script>

    <script type="text/javascript" src="js/changeSite.js"></script>

  </head>
  <body>
<!--
/**
*
* ********************************* ENGLISH *********************************
*
*    Copyright notice :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
*     Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
*     Notice de Copyright :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
*     Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
-->
    <nav>
      <img src="img/bandeau.png" class="imgnav"/>
      <h1>
<?php
    print("<a href='index.php'>QoQ-CoT</a><BR>\n");
    print('<a href="https://sourcesup.renater.fr/wiki/qoq-cot/user_guide_'.VERSION_FORMAT_WIKI.'" TARGET=NEW>');
    print('<i style="font-size: 0.5em;">'.VERSION_FORMAT_WIKI.'</i></a>');
?>
      </h1>
      <div id="corps">
      <div id="toggle-menu">
    		<a onclick="togglemenu()" onblur="togglemenu()">Menu<i class="fa fa-bars fa-2x" aria-hidden="true"></i></i></a>
      </div>
       <ul id="menu">
<?php
    require_once 'config.php';
    if (defined('SITES')) {
        $sites=unserialize(SITES);
        $nb_sites=sizeof($sites);
        if ($nb_sites>0) {
            print("<li>Site : <select name='sites' id='sites' onchange='changeSite(this)'>");
            print("<option value=''></option>");
            foreach ($sites as $site) {
                print("<option value='$site'");
                if (isset($_SESSION['site']) && $site==$_SESSION['site']) {
                    print(" selected");
                }
                print(">$site</option>");
            }
            print("</select></li>\n");
        }
    }	
    print("           <li id='first'><a");
    if (isset($_GET['app'])) {
        if ($_GET['app'] === 'graphe') {
            print (' class="active"');
        }
    }
    print (" href='index.php?app=graphe'><i class='fa fa-bar-chart' aria-hidden='true' style='padding-right: 10px'></i>Graphes</a></li>\n");
    if ($_SESSION['role'] === 'ROLE_ADMIN') { 
        print("           <li><a");
        if (isset($_GET['app'])) {
            if ($_GET['app'] === 'suivi') {
                print(" class='active'");
            }
        }
        print (" href='index.php?app=suivi'><i class='fa fa-plug' aria-hidden='true' style='padding-right: 5px'></i>&nbsp; Suivi des connexions</a></li>\n");
    }
    if ($_SESSION['role'] === 'ROLE_ADMIN') { 
        print("           <li id='last'><a");
        if (isset($_GET['app'])) {
            if ($_GET['app'] === 'admin') {
                print(" class='active'");
            }
        }
        print(" href='index.php?app=admin'><i class='fa fa-lock' aria-hidden='true' style='padding-right: 10px'></i>&nbsp; Admin</a></li>\n");
    }
    print("<li><a href='logout.html'><i class='fa fa-sign-out' aria-hidden='true' style='padding-right: 10px'></i>&nbsp; Déconnexion</a></li>\n");
?>
    </ul>
    </nav>
    <div id="document">
      <div class="clear"><hr /></div>
	 <div>
	     <?php echo $wrapper; ?>
	 </div>
     </div>

    <div id="pied"><span> &copy; Copyright <a href="http://dosi.univ-amu.fr" target="_blank">Dosi AMU</a></span></div>

   </div>
     <?php echo $script; ?>


     </body>
</html>
