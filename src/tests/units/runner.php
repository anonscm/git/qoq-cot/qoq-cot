<?php
/**
*
* ********************************* ENGLISH *********************************
* 
* --- Copyright notice :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Statement of copying permission
* 
* This file is part of QoQ-CoT.
* 
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
* 
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Déclaration de permission de copie
* 
* Ce fichier fait partie de QoQ-CoT.
* 
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
* 
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
* 
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
*/
namespace Dosicalu\QoQCoT\App\Graphe\tests\units;

require_once 'mageekguy.atoum.phar';

include 'app/graphe/DureeGraph.class.php';
include 'app/graphe/DureeMap.class.php';
include 'app/graphe/CumulDureeGraph.class.php';
include 'app/graphe/CpuGraph.class.php';
include 'app/graphe/CpuDetailGraph.class.php';
include 'app/graphe/MaxComposanteGraph.class.php';
include 'app/graphe/HumainGraph.class.php';
include 'app/graphe/MaxGraph.class.php';
include 'app/graphe/OsGraph.class.php';

use \mageekguy\atoum;
use Dosicalu\QoQCoT\App\Graphe\DureeGraph as DureeGraphTest;
use Dosicalu\QoQCoT\App\Graphe\DureeMap as DureeMapTest;
use Dosicalu\QoQCoT\App\Graphe\CumulDureeGraph as CumulDureeGraphTest;
use Dosicalu\QoQCoT\App\Graphe\CpuGraph as CpuGraphTest;
use Dosicalu\QoQCoT\App\Graphe\CpuDetailGraph as CpuDetailGraphTest;
use Dosicalu\QoQCoT\App\Graphe\MaxComposanteGraph as MaxComposanteGraphTest;
use Dosicalu\QoQCoT\App\Graphe\HumainGraph as HumainGraphTest;
use Dosicalu\QoQCoT\App\Graphe\MaxGraph as MaxGraphTest;
use Dosicalu\QoQCoT\App\Graphe\OsGraph as OsGraphTest;

class DureeGraph extends atoum\test
{
    private $_date = '2013-05-02';

    private $_salle = 35;

    private $_pas = 60;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';


    public function testgetYAxis()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($dureeGraph->getYAxis())->isNotEmpty();
    }

    public function testgetNomMachine()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($dureeGraph->getNomMachine())->isNotEmpty();
    }

    public function testgetDateDebut()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($dureeGraph->getDateDebut('08:00-09:00'))->isEqualTo($this->_date.' 08:00:00');
    }

    public function testgetDateFin()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($dureeGraph->getDateFin('08:00-09:00'))->isEqualTo($this->_date.' 09:00:00');
    }

    public function testfetchData()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($dureeGraph->fetchData())->isNotEmpty();
    }

    public function testgetXAxis()
    {
        $dureeGraph = new DureeGraphTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($dureeGraph->getXAxis())->isNotEmpty();
    }
}


class DureeMap extends atoum\test
{
    private $_date = '2013-05-02';

    private $_salle = 35;

    private $_pas = 60;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';


    public function testfetchData()
    {
        $dureeMap = new DureeMapTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($dureeMap->fetchData())->isNotEmpty();
    }

    public function testDisplay()
    {
        $dureeMap = new DureeMapTest($this->_date, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($dureeMap->display())->isNotEmpty();
        $this->string($dureeMap->display())->contains('map');
    }
}

class CumulDureeGraph extends atoum\test
{
    private $_date = '2013-05-02';

    private $_datefin = '2013-06-13';

    private $_salle = 35;

    private $_pas = 60;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';




    public function testgetXAxis()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cumulDureeGraph = new CumulDureeGraphTest($this->_date, $this->_datefin, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->array($cumulDureeGraph->getXAxis())->isNotEmpty();
    }

    public function testgetNomMachine()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cumulDureeGraph = new CumulDureeGraphTest($this->_date, $this->_datefin, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->array($cumulDureeGraph->getNomMachine())->isNotEmpty();
    }

    public function testfetchData()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cumulDureeGraph = new CumulDureeGraphTest($this->_date, $this->_datefin, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->array($cumulDureeGraph->fetchData())->isNotEmpty();
    }

    public function testgetDateDebut()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cumulDureeGraph = new CumulDureeGraphTest($this->_date, $this->_datefin, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->string($cumulDureeGraph->getDateDebut())->isEqualTo($this->_date.' 00:00:00');
    }

    public function testgetDateFin()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cumulDureeGraph = new CumulDureeGraphTest($this->_date, $this->_datefin, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->string($cumulDureeGraph->getDateFin())->isEqualTo($this->_datefin.' 23:59:59');
    }
}


class CpuGraph extends atoum\test
{
    private $_month = 01;

    private $_year  = 2013;

    private $_composante = '';

    private $_salle = 35;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';

    const NB_JOURS_OUVRES   = 20;


    public function testgetNbMachine()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cpuGraph = new CpuGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->string($cpuGraph->getNbMachine())->isNotNull();
    }

    public function testgetXAxis()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cpuGraph = new CpuGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->array($cpuGraph->getXAxis())->isNotEmpty();
    }

    public function testfetchData()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cpuGraph = new CpuGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $graphData = $cpuGraph->fetchData();

        $this->array($cpuGraph->fetchData())->isNotEmpty();
        $this->array($graphData['data'])->isNotEmpty();
        $this->array($graphData['label'])->isNotEmpty();
        $this->array($graphData['complement'])->isNotEmpty();
    }
}

class CpuDetailGraph extends atoum\test
{
    private $_month = 01;

    private $_year = 2013;
 
    private $_composante = '';

    private $_salle = 35;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';

    
    public function testgetNbMachine()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $cpuDetailGraph = new CpuDetailGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $this->string($cpuDetailGraph->getNbMachine('Salle Batiment B 0.02B'))->isNotNull();
    }
}


class MaxComposanteGraph extends atoum\test
{
    private $_month = 01;

    private $_year = 2013;

    private $_composante = 'COM';

    private $_salle = '';

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';


    public function testgetNbMachine()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $maxComposanteGraph = new MaxComposanteGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $nomMachine = 'salle 548';

        $this->string($maxComposanteGraph->getNbMachine($nomMachine))->isNotNull();
    }

    public function testgetXAxis()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $maxComposanteGraph = new MaxComposanteGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);
        
        $this->array($maxComposanteGraph->getXAxis())->isNotEmpty();
    }

    public function testfetchData()
    {
        $joursOuvres = serialize(array(1, 2, 3));

        $maxComposanteGraph = new MaxComposanteGraphTest($this->_month, $this->_year, $this->_composante, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin, $joursOuvres);

        $graphData = $maxComposanteGraph->fetchData();

        $this->array($maxComposanteGraph->fetchData())->isNotEmpty();
        $this->array($graphData['data'])->isNotEmpty();
        $this->array($graphData['complement'])->isNotEmpty();
    }
}

class HumainGraph extends atoum\test
{
    private $_date = '2013-05-02';

    private $_salle = 35;

    private $_composante = 'ESIL';

    private $_pas = 60;

    private $_largeur = 900;

    private $_heureDebut = '08:00';

    private $_heureFin = '18:00';



    public function testgetXAxis()
    {
        $humainGraph = new HumainGraphTest($this->_date, $this->_composante, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($humainGraph->getXAxis())->isNotempty();
    }

    public function testgetDateDebut()
    {
        $humainGraph = new HumainGraphTest($this->_date, $this->_composante, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($humainGraph->getDateDebut('08:00-09:00'))->isEqualTo($this->_date.' 08:00:00');
    }

    public function testgetDateFin()
    {
        $humainGraph = new HumainGraphTest($this->_date, $this->_composante, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($humainGraph->getDateFin('08:00-09:00'))->isEqualTo($this->_date.' 09:00:00');
    }

    public function testgetNbMachine()
    {
        $humainGraph = new HumainGraphTest($this->_date, $this->_composante =  null, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->string($humainGraph->getNbMachine())->isNotNull();
    }

    public function testfetchData()
    {
        $humainGraph = new HumainGraphTest($this->_date, $this->_composante = null, $this->_pas, $this->_salle, $this->_largeur, $this->_heureDebut, $this->_heureFin);

        $this->array($humainGraph->fetchData())->isNotEmpty();
    }
}


class OsGraph extends atoum\test
{
    private $_composante = '';

    private $_salle = 35;

    public function testfetchData()
    {
        $osGraph = new OsGraphTest($this->_salle, $this->_composante);

        $osArray = $osGraph->fetchData();

        $this->array($osGraph->fetchData())->isNotEmpty();
        $this->array($osArray['c'])->isNotEmpty();
        $this->array($osArray['nomos'])->isNotEmpty();
    }
}
