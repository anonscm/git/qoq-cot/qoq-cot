function nouveauSeuil(seuil,colspan,critere) {
	var bg_libre = ['#bada6a','#badaaa'];
	var bg_occupe = ['#e31f0c','#e31f5c'];
	var table = document.getElementById('tableau');
	var roffset = 3;
	var coffset = 1; 
	var eroffset = 2;
	var rtotal=2;
	for (var c = coffset, m = table.rows[roffset].cells.length; c < m; c++) {
		total=parseInt(table.rows[rtotal].cells[0].innerHTML);
		for (var r = roffset, n = table.rows.length; r < n-eroffset; r++) {
			if (table.rows[r].cells[c].innerHTML!='') {
				data=table.rows[r].cells[c].innerHTML.split('<br>');
				if (critere=='PourcentageMachines') {
					if ((parseInt(data[0])/parseInt(data[2]))*100>=seuil) { 
						table.rows[r].cells[c].style.backgroundColor=bg_occupe[Math.trunc((c-coffset)/colspan)%2];	
						total--;
					}
					else 
						table.rows[r].cells[c].style.backgroundColor=bg_libre[Math.trunc((c-coffset)/colspan)%2];
				} 
				else {
					if (parseInt(data[0])>=seuil) { 
						table.rows[r].cells[c].style.backgroundColor=bg_occupe[Math.trunc((c-coffset)/colspan)%2];	
						total--;
					}
					else 
						table.rows[r].cells[c].style.backgroundColor=bg_libre[Math.trunc((c-coffset)/colspan)%2];
				}
			}
		}
		table.rows[rtotal].cells[c].innerHTML=total;
	}
}
