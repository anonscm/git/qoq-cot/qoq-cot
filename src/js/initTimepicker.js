/**
*
* ********************************* ENGLISH *********************************
* 
* --- Copyright notice :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Statement of copying permission
* 
* This file is part of QoQ-CoT.
* 
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
* 
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Déclaration de permission de copie
* 
* Ce fichier fait partie de QoQ-CoT.
* 
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
* 
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
* 
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
*/
$('#dated').datetimepicker({ dateFormat: "dd/mm/yy", timeFormat: 'HH:mm'});
$('#datef').datetimepicker({
  dateFormat: "dd/mm/yy", 
  timeFormat: 'HH:mm', 
  beforeShow : function(input, inst) {
    var tmpdate = $('#dated').val().split(' '); 
    var tmp = tmpdate[0].split('/'); 
    $('#datef').datetimepicker('setDate',new Date(tmp[2],tmp[1]-1,tmp[0],'23','59'))
  }
})
