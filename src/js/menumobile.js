var menu = 0;
  $(document).on('click', function(e){
    var $this = $(e.target);
    if($this.closest('#menu').length == 0 && $this[0].id != "toggle"){
        hidemenu();
    }
  });

  function togglemenu(){
  	$('#menu').slideToggle("medium");
    if(menu==0){
      setTimeout(function () {
          menu = 1
      }, 500);
    } else {
      menu = 0;
    }
  }

  function hidemenu(){
    if(menu==1){
      $('#menu').slideToggle("medium");
      menu=0;
    }
  }
