$(document).ready(function() {

    $('#add_admin').click(function() {

	var newadmin = $('#login').val();

	$.post('index.php?app=admin&action=add', {login: newadmin}, function(data) {

	    if (data == "1" || data == "2") {

		if (data == "1") {

		    alert("Cet admin existe déjà");
		    $('#login').val('');
		} 

		if (data == "2") {

		    alert("Veuiller saisir un admin");
		}

	    } else {

		$('#login').val('');
		$('tbody').empty();
		$('tbody').append(data);
		initSuppr();
	    }
	});
    });

    $( "#dialog-confirm" ).dialog({ autoOpen: false });
 
    function initSuppr() {

	$('.del_admin').on("click", function() {

	    var admin = $(this).attr('id');
	    triggerDialog(admin);
	});
    }

    function triggerDialog(admin) {

	$('#nomadmin').empty();
	$('#nomadmin').append(admin);

	$( "#dialog-confirm" ).dialog("open").dialog({
	    resizable: false,
	    modal: true,
	    buttons: {
		"Supprimer": function() {
		    $.get('index.php?app=admin&action=delete', {login: admin}, function(data) {

			$('tbody').empty();
			$('tbody').append(data);
			
			initSuppr();
		    });
		    $( this ).dialog( "close" );
		},
		"Annuler": function() {
		    $( this ).dialog( "close" );
		}
	    }
	});
    }

    initSuppr();
});