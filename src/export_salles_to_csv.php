<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
  /**
   * Script d'export en csv des salles actuellement dans la base
   */
require_once 'config.php';
require_once 'config_interne.php';

/**
 * Affiche l'utilisation du setup dans la ligne de commande
 */
function usage()
{
    global $nb_sites,$sites;
    $liste_sites="";
    if ($nb_sites>0) {
        $liste_sites.="{";        
        for ($i=0;$i<$nb_sites;$i++) {
            $liste_sites.=$sites[$i];
            if ($i<$nb_sites-1) {
                $liste_sites.=",";
            }
        }
        $liste_sites.="}";        
    }
    print "Utilisation : php export_salles_en_csv.php $liste_sites > mon_fichier_csv\n";
    exit;
}

if (defined('SITES')) {
	$sites=unserialize(SITES);
}  else {
	$sites=array();
}
$nb_sites=sizeof($sites);

if ($argc<1 || ($argc<2 && $nb_sites>0) || ($argc>1 && $nb_sites==0) || (isset($argv[1]) && !in_array($argv[1],$sites)) || ($argc>2) ) {
    usage();
}

$dsn      = SQL_DSN;
$user     = SQL_USERNAME;
$password = SQL_PASSWORD;

try {
    $dbh = new PDO($dsn, $user, $password);

} catch (PDOException $e) {
    echo 'La connexion a échoué : ' . $e->getMessage();
    exit;
}

$site_condition='';
if ($nb_sites>0) {
    $site_condition='AND `Site`="'.$argv[1].'"';
}

$sql = "select NomMachine, NomSalle, Composante, Date_DEBUT, Date_FIN from `MachinesToSalles`, `Salles` where `RefSalle`=`IdSalle` $site_condition ORDER BY Composante, NomSalle, NomMachine";

$query = $dbh->prepare($sql);

$query->execute();
$results = $query->fetchall(PDO::FETCH_OBJ);

foreach ($results as $line)
{
	print '"'.$line->NomMachine.'","'.$line->NomSalle.'","'.$line->Composante.'","'.$line->Date_DEBUT.'","'.$line->Date_FIN.'"'."\n";
}
