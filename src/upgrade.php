<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/

require_once 'config.php';
require_once 'config_interne.php';

if ($argc==2 && $argv[1]==="-i") {
	$dsn      = SQL_DSN;
	$host     = SQL_HOST; 
	$db       = SQL_DBNAME;
	$user     = SQL_USERNAME;
	$password = SQL_PASSWORD;
	    
	try {
		$dbh = new PDO($dsn, $user, $password);
		$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	} catch (PDOException $e) {
		print 'Echec de la connexion : ' . $e->getMessage()."\n";
		exit;
	}
	
	$updates = array (
	"DROP TABLE IF EXISTS Aliases",
	"UPDATE IGNORE Connexions SET NomMachine = SUBSTRING_INDEX(NomMachine, '.', 1) WHERE NomMachine LIKE '%.%'",
	"UPDATE IGNORE MachinesToSalles SET NomMachine = SUBSTRING_INDEX(NomMachine, '.', 1) WHERE NomMachine LIKE '%.%'",
	"ALTER TABLE MachinesToSalles DROP INDEX NomNetBios",
	"ALTER TABLE MachinesToSalles DROP NomNetBios",
	"ALTER TABLE MachinesToSalles ADD Date_DEBUT date NOT NULL DEFAULT '1970-01-01'",
	"ALTER TABLE MachinesToSalles ADD Date_FIN date NOT NULL DEFAULT '2300-01-01'",
	"ALTER TABLE MachinesToSalles ADD INDEX RefSalle(RefSalle)",
	"ALTER TABLE MachinesToSalles ADD INDEX NomMachine(NomMachine)",
	"ALTER TABLE Connexions CHANGE NomMachine NomMachine VARCHAR(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL",
	"ALTER TABLE Connexions CHANGE NomOs NomOs VARCHAR(32) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL",
	"ALTER TABLE Connexions ADD IPMachine VARCHAR(15) NULL AFTER NomMachine",
	"ALTER TABLE Connexions ADD NomVM VARCHAR(64) NULL AFTER JourSemaine",
	"ALTER TABLE Connexions ADD IPVM VARCHAR(15) NULL AFTER NomVM",
	"ALTER TABLE Connexions DROP IdProcess",
	"ALTER TABLE Connexions ADD INDEX NomMachine(NomMachine)",
	"ALTER TABLE Salles ADD Site VARCHAR(20) NULL AFTER Composante",
	"ALTER TABLE Salles DROP INDEX NomSalle",
	"ALTER TABLE Salles ADD UNIQUE NomSalle (NomSalle, Composante, Site)",
	"ALTER TABLE Connexions DROP IdProcess",
	"ALTER TABLE Connexions ADD Pool VARCHAR(64) NULL AFTER IPVM",
	);
	
	foreach ($updates as $update) {
		$query = $dbh->prepare($update);
		try {
			$query->execute();
		} catch (PDOException $e) {
			# Pour palier à la directive IF EXISTS et IF NOT EXISTS
			if ( !(str_starts_with($e->getMessage(),"SQLSTATE[42000]: Syntax error or access violation: 1091 Can't DROP") || str_starts_with($e->getMessage(),"SQLSTATE[42S21]: Column already exists: 1060 Duplicate column name") || str_starts_with($e->getMessage(),"SQLSTATE[42000]: Syntax error or access violation: 1061 Duplicate key name")) ) {
				printf("\e[31mER Upgrade \e[39m : \e[33m%s\e[39m\n",$e->getMessage());
	    			exit;
			}
		}
	}
	
	print "\e[32mUpgrade réalisé avec succés\e[39m\n";
} else {
	print "Utilisation : php upgrade.php -i\n";
}
