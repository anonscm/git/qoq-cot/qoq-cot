<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2013-2021 DOSI AMU (Arnaud Salvucci, Fr�d�ric Bloise, Fr�d�ric Giudicelli, G�rard Milhaud)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of QoQ-CoT.
 * 
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRAN�AISE PERSONNELLE SANS VALEUR L�GALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2013-2021 DOSI AMU (Arnaud Salvucci, Fr�d�ric Bloise, Fr�d�ric Giudicelli, G�rard Milhaud)
 * 
 * 
 * --- D�claration de permission de copie
 * 
 * Ce fichier fait partie de QoQ-CoT.
 * 
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique G�n�rale GNU telle qu'elle est
 * publi�e par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (� votre choix) une quelconque version ult�rieure.
 * 
 * QoQ-CoT est distribu� dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique G�n�rale GNU
 * pour plus de d�tails.
 * 
 * Vous devriez avoir re�u une copie de la Licence Publique G�n�rale GNU avec 
 * QoQ-CoT ; si �a n'�tait pas le cas, �crivez � la Free Software Foundation,
 *
 */
require_once 'config.php';
require_once 'config_interne.php';
require_once 'utils.php';

$dsn = SQL_DSN;
$user = SQL_USERNAME;
$password = SQL_PASSWORD;

try {
	$dbh = new PDO($dsn, $user, $password);

} catch (PDOException $e) {
	echo 'Connection failed: ' . $e->getMessage();
}

$sql = 'SET NAMES utf8;';
$query = $dbh->prepare($sql);
$query->execute();

// Connexions dont le d�but et la fin sont remont�s avec une ann�e de trop

print '<b>Traitement des connexions dont la date de d�but et de fin sont remont�es avec une ann�e de trop</b><br/>';
$sql='UPDATE IGNORE Connexions SET DateDebutInitiale=DATE_SUB(DateDebutInitiale,INTERVAL 1 YEAR),DateDebut=DATE_SUB(DateDebut,INTERVAL 1 YEAR),DateFin=DATE_SUB(DateFin,INTERVAL 1 YEAR),Jour=DATE_FORMAT(DateDebut,"%Y-%m-%d"),JourSemaine=DATE_FORMAT(Jour,"%w") WHERE DateDebutInitiale > DATE_ADD(Now(),INTERVAL 1 WEEK)';
$query = $dbh->prepare($sql);
$query->execute();
print 'Nombre de lignes mises � jour : '.$query->rowCount().'<br/>';

$sql="DELETE FROM Connexions WHERE DateDebutInitiale > DATE_ADD(Now(),INTERVAL 1 WEEK)";
$query = $dbh->prepare($sql);
$query->execute();
print 'Nombre de lignes d�truites : '.$query->rowCount().'<br/>';

// Connexions dont la date de fin est remont�e avec une ann�e de trop
print '<b>Traitement des connexions dont la date de fin est remont�e avec une ann�e de trop</b><br/>';
$sql = 'SELECT DateDebutInitiale,NomMachine,Login,Max(DateFin) AS DateFin,IdProcess FROM Connexions WHERE DateDebut > DATE_ADD(Now(),INTERVAL 1 WEEK) GROUP BY DateDebutInitiale,NomMachine,Login';
$query = $dbh->prepare($sql);
$query->execute();

$results = $query->fetchAll(PDO::FETCH_OBJ);

$nbd=0;
$nbu=0;
foreach ($results as $result) {
	if ($result->DateFin!="") {
		$ObjNewDateFin=new DateTime($result->DateFin);
		$ObjNewDateFin->sub(new DateInterval('P1Y'));
		$NewDateFin= $ObjNewDateFin->format('Y-m-d H:i:s');
		$sql2 = 'UPDATE Connexions SET DateFin="'.$NewDateFin.'" WHERE DateFin="'.$result->DateFin.'"';
		$query2 = $dbh->prepare($sql2);
		$query2->execute();
		$nbu+=$query2->rowCount();

		$sql2 = 'DELETE FROM Connexions WHERE DateDebutInitiale="'.$result->DateDebutInitiale.'" AND NomMachine="'.$result->NomMachine.'" AND Login="'.$result->Login.'" AND DateDebut>"'.$NewDateFin.'" AND DateFin<>"'.$result->DateFin.'"';
		$query2 = $dbh->prepare($sql2);
		$query2->execute();
		$nbd+=$query2->rowCount();
	}
}
print 'Nombre de lignes d�truites : '.$nbd.'<br/>';
print 'Nombre de lignes mises � jour : '.$nbu.'<br/>';

exit;

