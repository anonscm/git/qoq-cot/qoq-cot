<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
  /**
   * Ce fichier fait partie du projet QoQ-CoT
   *
   * @category Administration
   * @package  QoQ-CoT
   * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
   * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
   */

namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Humain
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Nombre de machines utilisées" (le 3e dans l'interface)
 * qui s'obtient lorsque l'on spécifie seulement une date de début DD.
 * Il s'agit d'un graphe donnant l'évolution du nombre de machines utilisées
 * dans la salle -- ou le groupe de salles -- tout au long de la journée DD
 * pour chaque créneau horaire (en abscisse) composant la
 * fenêtre journalière spécifiée. Le nombre de valeurs dépend de la taille
 * des créneaux (appelé "pas") spécifié.
 * IDGraphe : 3 sans date de fin
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
class HumainGraph
{
    private $_date;

    private $_composante;

    private $_pas;

    private $_salles;

    private $_largeur;

    private $_heureDebut;

    private $_heureFin;

    private $_titre;

    /**
     * Constructeur
     *
     * @param string  $date       la date du graphe
     * @param string  $composante la composante observée
     * @param integer $pas        le pas des échantillons en minute
     * @param string  $salles     un array serialize de salles
     * @param integer $largeur    la largeur du graphe
     * @param string  $heureDebut heure de début d'observation des données
     * @param string  $heureFin   heure de fin d'observation des données
     */
    public function __construct($date, $composante, $pas, $salles, $largeur, $heureDebut, $heureFin)
    {
        $this->_date       = $date;
        $this->_composante = $composante;
        $this->_pas        = $pas;
        $this->_salles     = unserialize($salles);
        $this->_largeur    = $largeur;
        $this->_heureDebut = $heureDebut;
        $this->_heureFin   = $heureFin;
        $this->_titre      = $this->initTitre();
    }


    /**
     * Retourne les labels de l'axe des abscisses
     *
     * @return array $arrayHeure les labels des abscisses
     */
    public function getXAxis()
    {
        $pas = $this->_pas;
        $d = explode(':', $this->_heureDebut);
        $debut = $d[0]*60+$d[1];

        $f = explode(':', $this->_heureFin);
        $fin = $f[0]*60+$f[1];

        $i          = $debut;
        $arrayHeure = array();

        while ($i < $fin) {

            $item = sprintf("%02d:%02d-", $i/60, $i%60);

            $i = $i + $pas;

            $item .= sprintf("%02d:%02d", $i/60, $i%60);

            $arrayHeure[] = $item;
        }

        return $arrayHeure;
    }


    /**
     * Retourne la date du créneau
     *
     * @param string $creneau un créneau horaire
     *
     * @return string l'heure de début du créneau
     */
    public function getDateDebut($creneau)
    {
        $heure = explode('-', $creneau);

        return $this->_date.' '.$heure[0].':00';
    }


    /**
     * Récupère l'heure de fin du créneau
     *
     * @param string $creneau un créneau horaire
     *
     * @return sting l'heure de fin du créneau
     */
    public function getDateFin($creneau)
    {
        $heure = explode('-', $creneau);

        return $this->_date.' '.$heure[1].':00';
    }

  /**
     * Retourne le nombre de machines de la salle ou de la composante pour le jour requêté
     *
     * @return integer $nbMachine le nombre de machine de la salle ou de la composante
     */
    public function getNbMachine()
    {
        $pdo = \Dao::getInstance();

        $machineArray  = array();

        $sql = 'SELECT COUNT(NomMachine) AS total '.
                   'FROM MachinesToSalles AS ms '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle WHERE ';

            if ($this->_composante != '') {

                $sql .= 's.Composante=:composante ';

            } else {

                $clauseIn = implode(",", $this->_salles);

                $sql .= 's.IdSalle IN ('.$clauseIn.') ';
            }

            $sql .= 'AND ms.Date_FIN>= :date AND ms.date_DEBUT<= :date';


            $query = $pdo->prepare($sql);


            if ($this->_composante != '')
                $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

            $query->bindParam(':date', $this->_date, \PDO::PARAM_STR);

            $query->execute();
            $results = $query->fetch(\PDO::FETCH_OBJ);

        return $results->total;
    }


    /**
     * Initialise le titre du graphe
     *
     *  @return string $titre le titre du graphe
     */
    public function initTitre()
    {
        $pdo = \Dao::getInstance();

        if ($this->_composante != '') {

            $sql = 'SELECT DISTINCT Composante FROM Salles '.
                   'WHERE Composante = :composante';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql = 'SELECT NomSalle FROM Salles '.
                   'WHERE IdSalle IN ('.$clauseIn.');';
        }

        $query = $pdo->prepare($sql);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll();

        if ($this->_composante != '') {

            $titre = 'Nb terminaux utilisés pour le groupe de salles '.$results[0]['Composante'].' au '.date('d/m/Y', strtotime($this->_date));

        } else {

            $count = $query->rowCount();

            if ($count > 1) {

                $salles = '[ ';

                foreach ($results as $salle) {

                    $salles .= $salle['NomSalle'].' ';
                }

                $salles .= ']';

                $titre = 'Nb terminaux utilisés pour le groupe de salles '.$salles.' au '.date('d/m/Y', strtotime($this->_date));

            } else {

                $titre = 'Nb terminaux utilisés pour la salle '.$results[0]['NomSalle'].' au '.date('d/m/Y', strtotime($this->_date));
            }
        }

        return $titre;
    }


    /**
     * Récupère les données du graphe
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $dataArray  = array();

        $xAxisArray = $this->getXAxis();

        $pdo = \Dao::getInstance();

        $tableConnexions = \Dao::getTableConnexions(new \DateTime($this->_date));

        foreach ($xAxisArray as $heure) {

            $sql = 'SELECT COUNT(DISTINCT ms.NomMachine) AS somme '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE NOT (DateFin < :dateDebut OR DateDebut > :dateFin) ';


            if ($this->_composante != '') {

                $sql .= 'AND s.Composante = :composante ';

            } else {

                $clauseIn = implode(",", $this->_salles);

                $sql .= 'AND s.IdSalle IN ('.$clauseIn.') ';
            }


            $sql .= 'AND ms.Date_FIN>= :date AND ms.date_DEBUT<= :date';

            $query = $pdo->prepare($sql);

            $dateDebut = $this->getDateDebut($heure);
            $dateFin   = $this->getDateFin($heure);

            $query->bindParam(':dateDebut', $dateDebut, \PDO::PARAM_STR);
            $query->bindParam(':dateFin', $dateFin, \PDO::PARAM_STR);
            $query->bindParam(':date', $this->_date, \PDO::PARAM_STR);

            if ($this->_composante != '')
                $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

            $query->execute();
            $results = $query->fetch(\PDO::FETCH_OBJ);

            $dataArray[] = $results->somme;
        }

        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $nbMachine = $this->getNbMachine();
        $graphData = $this->fetchData();

        # Calcul de la moyenne
        $moyenne = 0;
        foreach ($graphData as $key => $value)
        {
                $moyenne+=$value;
        }
        $moyenne /= sizeof($graphData);

        /* Create and populate the pData object */
        $myData = new \pData();
        $myData->addPoints($graphData, 'Nombre terminaux utilisés');
        $myData->setSerieWeight('Nombre terminaux utilisés', 1);
        $myData->setSerieWeight('Nombre de terminaux', 1);
        $myData->setAxisName(0, 'Nb terminaux utilisés');
        $myData->addPoints($this->getXAxis(), 'Labels');
        $myData->setSerieDescription('Labels', 'Months');
        $myData->setAbscissa('Labels');

        /* Create the pChart object */
        $myPicture = new \pImage($this->_largeur, 320, $myData);

        /* Turn of Antialiasing */
        $myPicture->Antialias = false;

        /* Draw the background */
        $settings = array('R' => 170, 'G' => 183, 'B' => 87, 'Dash' => 1, 'DashR' => 190, 'DashG' => 203, 'DashB' => 107);
        $myPicture->drawFilledRectangle(0, 0, $this->_largeur, 320, $settings);

        /* Overlay with a gradient */
        $settings = array('StartR' => 219, 'StartG' => 231, 'StartB' => 139, 'EndR' => 1, 'EndG' => 138, 'EndB' => 68, 'Alpha' => 50);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 320, DIRECTION_VERTICAL, $settings);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

        /* Add a border to the picture */
        $myPicture->drawRectangle(0, 0, $this->_largeur-1, 319, array('R' => 0, 'G' => 0, 'B' => 0));

        /* Write the chart title */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11, 'R' => 255, 'G' => 255, 'B' => 255));
        $myPicture->drawText(10, 20, $this->_titre, array('FontSize' => 11, 'Align' => TEXT_ALIGN_BOTTOMLEFT));

        /* Set the default font */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Forgotte.ttf', 'FontSize' => 11, 'R' => 0, 'G' => 0, 'B' => 0));

        /* Define the chart area */
        $myPicture->setGraphArea(60, 40, $this->_largeur-50, 240);

        /* Draw the scale */
        $axisBoundaries = array(0 => array('Min' => 0, 'Max' => $nbMachine+round(0.2*$nbMachine)));
        $scaleSettings  = array('XMargin' => 10, 'YMargin' => 10, 'Floating' => true, 'GridR' => 200, 'GridG' => 200, 'GridB' => 200, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $axisBoundaries, 'DrawSubTicks' => true, 'DrawArrows' => true, 'ArrowSize' => 6, 'LabelRotation' => 45);
        $myPicture->drawScale($scaleSettings);


        $myPicture->drawThreshold($nbMachine, array('WriteCaption' => true, 'Caption' => $nbMachine, 'BoxAlpha' => 100, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));
        # La moyenne...
        $myPicture->drawThreshold($moyenne, array('WriteCaption' => true, 'Caption' => "Moyenne : ".round($moyenne,2), 'BoxAlpha' => 40, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));

        /* Turn on Antialiasing */
        $myPicture->Antialias = true;

        /* Enable shadow computing */
        $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));

        /* Draw the line chart */
        $myPicture->drawLineChart();
        $myPicture->drawPlotChart(array('DisplayValues' => true, 'PlotBorder' => true, 'BorderSize' => 2, 'Surrounding' => -60, 'BorderAlpha' => 80));

        /* Write the chart legend */
        $myPicture->drawLegend(590, 303, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL, 'FontR' => 255, 'FontG' => 255, 'FontB' => 255));

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('pictures/example.drawLineChart.plots.png');
    }
}
