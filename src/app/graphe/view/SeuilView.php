<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */

$content .= '<div id="container">';

$content .= '<h2>Utilisation des salles en fonction d\'un critère seuil<A HREF="https://sourcesup.renater.fr/wiki/qoq-cot/graphes_'.VERSION_FORMAT_WIKI.'" TARGET=_blank> <FONT SIZE=-1><I>(?)</I></FONT></A></h2>';

$content .= '<form action="index.php" method="get" autocomplete="off" id="seuilform">';
$content .= '<p>';
$content .= '<label id="l_datepicker" for="datepicker">Date début : </label>';
$content .= '<input type="text" name="datepicker" id="datepicker" value="'.$datepicker.'" />';

$content .= '<label for="heuredebut" id="l_heuredebut">Entre : </label>';
$content .= '<input type="text" name="heuredebut" id="heuredebut" value="'.$heureDebut.'" />';

$content .= '<label for="composante" id="l_composante">Groupe de salles : </label>';
$content .= '<select name="composante" id="composante" class="large">';
$content .= '<option value=""> -- </option>';

foreach ($listeComposantes as $c) {

    $content .= '<option value="'.$c->composante.'" ';

    if (isset($_GET['composante']))
        if ($c->composante == $_GET['composante'])
            $content .= 'selected="selected"';

    $content .= '>'.$c->composante.'</option>';
}

$content .= '</select>';
$content .= '</p>';


$content .= '<p>';
$content .= '<label for="datepickerfin" id="l_datepickerfin">Date fin : </label>';
$content .= '<input type="text" name="datepickerfin" id="datepickerfin" value="'.$datepickerfin.'" />';

$content .= '<label for="heurefin" id="l_heurefin">Et : </label>';
$content .= '<input type="text" name="heurefin" id="heurefin" value="'.$heureFin.'" />';



$content .= '<label for="salle" id="l_salle">Salle : </label>';

$content .= '<select name="salles[]" id="salle" multiple="multiple" class="large">';
$content .= '<option value=""> -- </option>';

$optgroup = '';

foreach ($listeSalles as $s) {

    if ($s->Composante !== $optgroup) {
        if ($optgroup != '') {
            $content .= '</optgroup>';
        } 
        $content .= '<optgroup label="'.$s->Composante.'">';
        $optgroup = $s->Composante;
    }

    $content .= '<option value="'.$s->IdSalle.'" ';

    if (isset($_GET['salles']))
        if (in_array($s->IdSalle, $_GET['salles']))
            $content .= 'selected="selected"';

    $content .= '>'.$s->NomSalle.'</option>';
}

if ($optgroup != '') {
    $content .= '</optgroup>';
}

$content .= '</select>';
$content .= '</p>';

$content .= '<label for="pas" id="l_pas">Pas (en mn) : </label>';
$content .= '<select name="pas" id="pas">';

$arrayPas = array(60,90,120);

foreach ($arrayPas as $defaultPas) {

    $content .= '<option value="'.$defaultPas.'" ';

    if ($pas == $defaultPas) {

            $content .= 'selected="selected"';
    }

    $content .= ' >'.$defaultPas.'</option>';
}

$content .= '</select>';
$content .= '</p>';

$content .= '<p id="zonejoursouvres">';

$content .= 'Jours pris en compte : <br />';

for ($i=0;$i<7;$i++) {

    $content .= '<input type="checkbox" name="joursouvres[]" id="'.$arrayJours[($i+1)%7].'" value="'.(($i+1)%7).'"';

    if (in_array(($i+1)%7, $arrayJoursOuvres))
         $content .= ' checked="checked"';

    $content .= ' />';

    $content .= '<label for="'.$arrayJours[($i+1)%7].'" id="l_'.$arrayJours[($i+1)%7].'">'.$arrayJours[($i+1)%7].'</label>';

}

$content .= '</p>';

$content .= '<p>';

$content .= '<label for="critereSeuil" id="l_critereSeuil">Critère seuil : </label>';
$content .= '<select name="critereSeuil" id="critereSeuil" class="large">';
$content .= '<option value=""> -- </option>';
foreach ($arrayCriteresSeuil as $key=>$value) {
    $content .= '<option value="'.$key.'" ';

    if (isset($_GET['critereSeuil']))
        if ($key == $_GET['critereSeuil'])
            $content .= 'selected="selected"';

    $content .= '>'.$value.'</option>';
}
$content .= '</select>';

$content .= '<label for="valeurSeuil" id="l_valeurSeuil">Valeur du seuil : </label>';
$content .= '<input type="text" name="valeurSeuil" id="valeurSeuil" value="'.$valeurSeuil.'" />';

$content .= '</p>';

$content .= '<p>';
$content .= '<input type="submit" id="submit" name="rechercher" value="Rechercher" />';
$content .= '<input type="hidden" id="date" name="date" value="'.$date.'" />';
$content .= '<input type="hidden" id="datefin" name="datefin" value="'.$datefin.'" />';
$content .= '<input type="hidden" name="menu" value="seuil" />';
$content .= '<input type="hidden" name="app" value="graphe" />';
$content .= '<input type="hidden" id="largeur" name="largeur" />';
$content .= '</p>';
$content .= '</form>';
$content .= '</div>';
$content .= '<center> <div>';

$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui-timepicker-addon.js"></script>';
$script .= '<script type="text/javascript" src="js/initDatepicker.js"></script>';
$script .= '<script type="text/javascript" src="js/initTimePickerSeuil.js"></script>';
$script .= '<script type="text/javascript" src="js/initDimension.js"></script>';
$script .= '<script type="text/javascript" src="js/toogleselect.js"></script>';
$script .= '<script type="text/javascript" src="js/nouveauSeuil.js"></script>';
