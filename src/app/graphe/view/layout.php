<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
$wrapper  = '<ul id="menugraphe">';

$wrapper .= '<li><a href="index.php?app=graphe&amp;menu=duree" id="mduree" ';
if (isset($_GET['menu']))
    if ($_GET['menu'] === 'duree')
        $wrapper .= 'class="active"';
$wrapper .= ' title="Utilisation détaillée par terminal"><span>Utilisation détaillée par terminal</span></a></li>';

$wrapper .= '<li><a href="index.php?app=graphe&amp;menu=cpu" id="mcpu" ';
if (isset($_GET['menu']))
    if ($_GET['menu'] === 'cpu')
        $wrapper .= 'class="active"';
$wrapper .= ' title="Taux d\'utilisation annuel"><span>Taux d\'utilisation annuel</span></a></li>';

$wrapper .= '<li><a href="index.php?app=graphe&amp;menu=humain" id="mhumain" ';
if (isset($_GET['menu']))
    if ($_GET['menu'] === 'humain')
        $wrapper .= 'class="active"';
$wrapper .= ' title="Nombre de terminaux utilisés / Pic de connexions"><span>Nombre de terminaux utilisés / Pic de connexions</span></a></li>';

$wrapper .= '<li><a href="index.php?app=graphe&amp;menu=os" id="mos" ';
if (isset($_GET['menu']))
    if ($_GET['menu'] === 'os')
        $wrapper .= 'class="active"';
$wrapper .= ' title="Répartition des connexions par Os"><span>Répartition des connexions par Os</span></a></li>';


$wrapper .= '<li><a href="index.php?app=graphe&amp;menu=seuil" id="mseuil" ';
if (isset($_GET['menu']))
    if ($_GET['menu'] === 'seuil')
        $wrapper .= 'class="active"';
$wrapper .= ' title="Utilisation des salles en fonction d\'un critère seuil"><span>Utilisation des salles en fonction d\'un critère seuil</span></a></li>';

$wrapper .= '</ul>';
$wrapper .= '<div id="forms">';
$wrapper .= $content.$image.$map;
$wrapper .= '</div></center>'; /*ajout version 4.0 Rocky pour corriger un problème d'affichage du graphe et afficher correctement l'icone de chargement*/
$wrapper .= '</div>';
