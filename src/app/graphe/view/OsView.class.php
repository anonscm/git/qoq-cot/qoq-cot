<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
$content .= '<div id="container">';

$content .= '<h2>Répartition des connexions par système d\'exploitation<A HREF="https://sourcesup.renater.fr/wiki/qoq-cot/graphes_'.VERSION_FORMAT_WIKI.'" TARGET=_blank> <FONT SIZE=-1><I>(?)</I></FONT></A></h2>';

$content .= '<form action="index.php" method="get" autocomplete="off" id="osform">';

$content .= '<p>';
$content .= '<label id="l_datepicker" for="datepicker">Date début : </label>';
$content .= '<input type="text" name="datepicker" id="datepicker" value="'.$datepicker.'" />';


$content .= '<label for="composante" id="l_composante">Groupe de salles : </label>';
$content .= '<select name="composante" id="composante" class="large">';


# On fait en sorte que si aucune composante n'est choisie, on ait bien comme option SELECTIONNEE l'option vide. Ceci afin qu'on aie bien
# la special secret feature vendue dans la doc. : si rien n'est sélectionné, alors on affiche les stats de toutes les salles.
$selected = "";
if (! isset($_GET['composante']))
{
	$selected = 'selected="selected" ';
}
$content .= '<option '.$selected.'value=""> -- </option>';

foreach ($listeComposantes as $c) {

    $content .= '<option value="'.$c->composante.'" ';

    if (isset($_GET['composante']))
        if ($c->composante == $_GET['composante'])
            $content .= 'selected="selected"';

    $content .= '>'.$c->composante.'</option>';
}

$content .= '</select>';
$content .= '</p>';



$content .= '<p>';
$content .= '<label for="datepickerfin" id="l_datepickerfin">Date fin : </label>';
$content .= '<input type="text" name="datepickerfin" id="datepickerfin" value="'.$datepickerfin.'" />';

$content .= '<label for="salle" id="l_salle">Salle : </label>';

$content .= '<select name="salles[]" id="salle" multiple="multiple" class="large">';

# On fait en sorte que si aucune salle n'est choisie, on ait bien comme option SELECTIONNEE l'option vide. Ceci afin qu'on aie bien
# la special secret feature vendue dans la doc. : si rien n'est sélectionné, alors on affiche les stats de toutes les salles.
$selected = "";
if (! isset($_GET['salles']))
{
	$selected = 'selected="selected" ';
}
$content .= '<option '.$selected.'value=""> -- </option>';

$optgroup = '';

foreach ($listeSalles as $s) {

    if ($s->Composante !== $optgroup) {
        if ($optgroup != '') {
            $content .= '</optgroup>';
        } 
        $content .= '<optgroup label="'.$s->Composante.'">';
        $optgroup = $s->Composante;
    }

    $content .= '<option value="'.$s->IdSalle.'" ';

    if (isset($_GET['salles']))
        if (in_array($s->IdSalle, $_GET['salles']))
            $content .= 'selected="selected"';

    $content .= '>'.$s->NomSalle.'</option>';
}

if ($optgroup != '') {
    $content .= '</optgroup>';
}

$content .= '</select>';
$content .= '</p>';

$content .= '<p>';
$content .= '<input type="submit" id="submit" name="rechercher" value="Rechercher" />';
$content .= '<input type="hidden" name="menu" value="os" />';
$content .= '<input type="hidden" name="app" value="graphe" />';
$content .= '<input type="hidden" id="date" name="date" value="'.$date.'" />';
$content .= '<input type="hidden" id="datefin" name="datefin" value="'.$datefin.'" />';
$content .= '</p>';
$content .= '</form>';

$content .= '</div>';
$content .= '<center> <div class="graphe">';

$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui.js"></script>';
$script .= '<script type="text/javascript" src="js/initDatepicker.js"></script>';
$script .= '<script type="text/javascript" src="js/toogleselect.js"></script>';
