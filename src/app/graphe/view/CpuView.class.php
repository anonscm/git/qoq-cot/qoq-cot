<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
if (isset($_GET['joursouvres'])) {
    $joursouvres = $_GET['joursouvres'];

} else {

    $joursouvres = unserialize(JOURS_OUVRES);
}

$content .= '<div id="container">';

$content .= '<h2>Taux d\'utilisation annuel<A HREF="https://sourcesup.renater.fr/wiki/qoq-cot/graphes_'.VERSION_FORMAT_WIKI.'" TARGET=_blank> <FONT SIZE=-1><I>(?)</I></FONT></A></h2>';

$content .= '<form action="index.php" method="get" autocomplete="off" id="cpuform">';
$content .= '<p>';
$content .= '<label for="mois" id="l_mois">Mois : </label>';
$content .= '<select name="mois" id="mois">';

foreach ($arrayMonth as $num => $mois) {

    $content .= '<option value="'.$num.'" ';

    if (isset($_GET['mois']))
        if ($num == $_GET['mois'])
            $content .= 'selected="selected"';

    $content .= '>'.$mois.'</option>';
}

$content .= '</select>';


$content .= '<label for="heuredebut" id="l_heuredebut">Entre : </label>';
$content .= '<input type="text" name="heuredebut" id="heuredebut" value="'.$heureDebut.'" />';

$content .= '<label for="composante" id="l_composante">Groupe de salles : </label>';
$content .= '<select name="composante" id="composante" class="large">';
$content .= '<option value=""> -- </option>';

foreach ($listeComposantes as $c) {

    $content .= '<option value="'.$c->composante.'" ';

    if (isset($_GET['composante']))
        if ($c->composante == $_GET['composante'])
            $content .= 'selected="selected"';

    $content .= '>'.$c->composante.'</option>';
}

$content .= '</select>';
$content .= '</p>';

$content .= '<p>';
$content .= '<label for="annee" id="l_annee">Année : </label>';
$content .= '<select name="annee" id="annee">';

$anneeListe = range($annee, date('Y'));

foreach ($anneeListe as $a) {

    $content .= '<option value="'.$a.'" ';

    if (isset($_GET['annee'])) {

        if ($a == $_GET['annee']) {

            $content .= 'selected="selected"';
        }

    } else if ($a == date('Y')) {

        $content .= 'selected="selected"';
    }

    $content .= '>'.$a.'</option>';
}

$content .= '</select>';

$content .= '<label for="heurefin" id="l_heurefin">Et : </label>';
$content .= '<input type="text" name="heurefin" id="heurefin" value="'.$heureFin.'" />';

$content .= '<label for="salle" id="l_salle">Salle : </label>';

$content .= '<select name="salles[]" id="salle" multiple="multiple" class="large">';
$content .= '<option value=""> -- </option>';

$optgroup = '';

foreach ($listeSalles as $s) {

    if ($s->Composante !== $optgroup) {
        if ($optgroup != '') {
            $content .= '</optgroup>';
        } 
        $content .= '<optgroup label="'.$s->Composante.'">';
        $optgroup = $s->Composante;
    }

    $content .= '<option value="'.$s->IdSalle.'" ';

    if (isset($_GET['salles']))
        if (in_array($s->IdSalle, $_GET['salles']))
            $content .= 'selected="selected"';

    $content .= '>'.$s->NomSalle.'</option>';
}

if ($optgroup != '') {
    $content .= '</optgroup>';
}

$content .= '</select>';

$content .= '</p>';


$content .= '<p id="zonedateexclu">';

$content .= 'Plage d\'exclusion : <br />';

$content .= '<label for="excludebut" class="l_excludebut">Entre : </label>';
$content .= '<input type="text" name="excludebut" id="excludebut" value="'.$excluDebut.'" />';

$content .= '<label for="exclufin" class="l_exclufin">Et : </label>';
$content .= '<input type="text" name="exclufin" id="exclufin" value="'.$excluFin.'" />';

$content .= '</p>';


$content .= '<p id="zonejoursouvres">';

$content .= 'Jours pris en compte : <br />';

$content .= '<input type="checkbox" name="joursouvres[]" id="lundi" value="1"';

if (in_array(1, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="lundi" id="l_lundi">Lundi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="mardi" value="2"';

if (in_array(2, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="mardi" id="l_mardi">Mardi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="mercredi" value="3"';

if (in_array(3, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="mercredi" id="l_mercredi">Mercredi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="jeudi" value="4"';

if (in_array(4, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="jeudi" id="l_jeudi">Jeudi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="vendredi" value="5"';

if (in_array(5, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="vendredi" id="l_vendredi">Vendredi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="samedi" value="6"';

if (in_array(6, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="samedi" id="l_samedi">Samedi</label>';

$content .= '<input type="checkbox" name="joursouvres[]" id="dimanche" value="0"';

if (in_array(0, $joursouvres))
    $content .= 'checked="checked"';

$content .= ' />';

$content .= '<label for="dimanche" id="l_dimanche">Dimanche</label>';

$content .= '</p>';

$content .= '<p>';
$content .= '<input type="submit" class="bouton" id="submit" name="rechercher" value="Rechercher" />';
$content .= '<input type="hidden" name="menu" value="cpu" />';
$content .= '<input type="hidden" name="app" value="graphe" />';
$content .= '<input type="hidden" id="largeur" name="largeur" />';
$content .= '</p>';
$content .= '</form>';

$content .= '</div>';
$content .= '<center> <div class="graphe">';

$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery.js"></script>';
$script .= '<script type="text/javascript" src="js/initDimension.js"></script>';
$script .= '<script type="text/javascript" src="js/toogleselect.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui-timepicker-addon.js"></script>';
$script .= '<script type="text/javascript" src="js/initTimePickerCumul.js"></script>';
