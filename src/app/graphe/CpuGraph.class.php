<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Ce fichier fait partie du projet QoQ-CoT
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Cpu
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Taux d'utilisation annuel" (le 2e dans l'interface)
 * qu'on choisisse une seule salle (c'est alors le seul graphe affiché)
 * ou un groupe de salles (c'est alors le premier des 3 graphes affichés).
 * Il s'agit d'un graphe donnant l'évolution du taux d'utilisation de la
 * salle -- ou du groupe de salles -- sur 12 mois, avec une valeur
 * en abscisse pour chaque mois, le mois initial étant fixé par le formulaire.
 * IDGraphe : 2.1
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */

class CpuGraph
{
    private $_month;

    private $_year;

    private $_composante;

    private $_salles;

    private $_largeur;

    private $_heureDebut;

    private $_heureFin;

    private $_joursOuvres;

    private $_excluDebut;

    private $_excluFin;

    private $_titre;

    /**
     * Constructeur
     *
     * @param string  $month       le mois choisi
     * @param string  $year        l'année choisie
     * @param string  $composante  la composante observée
     * @param string  $salles      un array serialize de salles
     * @param integer $largeur     la largeur du graphe
     * @param string  $heureDebut  l'heure d'ouverture des salles
     * @param string  $heureFin    l'heure de fermeture des salles
     * @param string  $joursOuvres array des jours ouvrés serializé
     * @param string  $exclutDebut l'heure de début de la plage à exclure
     * @param string  $excluFin    l'heure de fin de la plage à exclure
     */
    public function __construct($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvres, $excluDebut, $excluFin)
    {
        $this->_month       = $month;
        $this->_year        = $year;
        $this->_composante  = $composante;
        $this->_salles      = unserialize($salles);
        $this->_largeur     = $largeur;
        $this->_heureDebut  = $heureDebut;
        $this->_heureFin    = $heureFin;
        $this->_joursOuvres = $joursOuvres;
        $this->_excluDebut  = $excluDebut;
        $this->_excluFin    = $excluFin;
        $this->_titre       = $this->initTitre();
    }

    /**
     * retourne le nombre de jours dans le mois
     *  @param  le 1er jour du mois (ex: 2017-06-01)
     *  @return integer $nombre de jours dans le mois
     */
    public function getJoursMois($date)
    {
      $nbJoursMois = cal_days_in_month(CAL_GREGORIAN, $date->format('m'), $date->format('y'));
      return $nbJoursMois;
    }

    /**
     * Retourne le nombre de machines pour le mois en argument
     * @param  le 1er jour du mois (ex: 2017-06-01)
     * @return integer $nbMachine le nombre de machines
     */
    public function getNbMachine($date)
    {
        $pdo = \Dao::getInstance();

        $sql = 'SELECT SUM((DATEDIFF(LEAST(:dateFin,Date_FIN),GREATEST(:dateDebut,Date_DEBUT))+1)/:nbJoursMois) AS total ';
       
        $sql .= 'FROM MachinesToSalles as ms ';
        $sql .= 'INNER JOIN Salles s ON ms.RefSalle = s.IdSalle ';
        $sql .= 'WHERE NOT (ms.Date_FIN < :dateDebut OR ms.date_DEBUT > :dateFin) ';

        if ($this->_composante != '') {

            $sql .= 'AND Composante = :composante ';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql .= 'AND RefSalle IN ('.$clauseIn.')';
        }

        $query = $pdo->prepare($sql);

        $dateDebut = $date->format("Y-m-d");
        $query->bindParam(":dateDebut", $dateDebut, \PDO::PARAM_STR);

        $dateFinMois = clone $date;
        $dateFinMois->add(new \DateInterval('P1M'));
        $dateFinMois->sub(new \DateInterval('P1D'));
        $dateFin = $dateFinMois->format("Y-m-d");
        $query->bindParam(":dateFin", $dateFin, \PDO::PARAM_STR);
        $nbJoursMois =  $this->getJoursMois($date);
        $query->bindParam(":nbJoursMois", $nbJoursMois, \PDO::PARAM_STR);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetch(\PDO::FETCH_OBJ);

        return $results->total;
    }

    /**
     * Initialise le titre du graphe
     *
     *  @return string $titre le titre du graphe
     */
    public function initTitre()
    {
        $pdo = \Dao::getInstance();

        if ($this->_composante != '') {

            $sql = 'SELECT DISTINCT Composante FROM Salles '.
                   'WHERE Composante = :composante';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql = 'SELECT NomSalle FROM Salles '.
                   'WHERE IdSalle IN ('.$clauseIn.');';
        }

        $query = $pdo->prepare($sql);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll();

        if ($this->_composante != '') {

            $titre = 'Taux d\'utilisation annuel pour le groupe de salles '.$results[0]['Composante'];

        } else {

            $count = $query->rowCount();

            if ($count > 1) {

                $salles = '[ ';

                foreach ($results as $salle) {

                    $salles .= $salle['NomSalle'].' ';
                }

                $salles .= ']';

                $titre = 'Taux d\'utilisation annuel pour le groupe de salles '.$salles;

            } else {

                $titre = 'Taux d\'utilisation annuel pour la salle '.$results[0]['NomSalle'];
            }
        }

        return $titre;
    }


    /**
     * Retourne un array contenant les labels des abscisses
     *
     * @return array $arrayMois les labels des abscisses
     */
    public function getXAxis()
    {
        $arrayMois = array('01' => 'Janvier',
                           '02' => 'Fevrier',
                           '03' => 'Mars',
                           '04' => 'Avril',
                           '05' => 'Mai',
                           '06' => 'Juin',
                           '07' => 'Juillet',
                           '08' => 'Août',
                           '09' => 'Septembre',
                           '10' => 'Octobre',
                           '11' => 'Novembre',
                           '12' => 'Décembre');
        return $arrayMois;
    }


    /**
     * Récupère les données
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $pdo = \Dao::getInstance();

        $date = new \DateTime($this->_year.'-'.$this->_month.'-01');

        $tableConnexions = \Dao::getTableConnexions($date);

        $Montharray = $this->getXAxis();

        //incrémentation pour obtenir un graphe avec 12 points
        for ($i = 0; $i < 12; $i++) {

            $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :heureFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :heureDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE DateDebut LIKE "'.$date->format('Y-m').'%" '.
                   'AND NOT (HeureFin < :heureDebut OR HeureDebut > :heureFin) ';

            $listeJO = implode(',', unserialize($this->_joursOuvres));

            $sql .= 'AND JourSemaine IN ('.$listeJO.') ';
            $sql .= 'AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN) ';

            if ($this->_composante != '') {

                $sql .= 'AND s.Composante=:composante';

            } else {

                $clauseIn = implode(",", $this->_salles);

                $sql .= 'AND s.IdSalle IN ('.$clauseIn.')';
            }

            $query = $pdo->prepare($sql);

            $query->bindParam(':heureDebut', $this->_heureDebut, \PDO::PARAM_STR);
            $query->bindParam(':heureFin', $this->_heureFin, \PDO::PARAM_STR);



            if ($this->_composante != '')
                $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

            $query->execute();

            $res = $query->fetch(\PDO::FETCH_OBJ);

            $result = $res->total;

            # Le nombre d'heures de la fenetre, servira pour le calcul du complement
            $nb_heures_par_jour = strtotime($this->_heureFin) - strtotime($this->_heureDebut);

            # Cas avec plage d'exclusion
            if ($this->_excluDebut !== '' && $this->_excluFin !== '') {

                $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :excluFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :excluDebut), DateDebut)))) AS total '.
                       'FROM '.$tableConnexions.' AS c '.
                       'INNER JOIN MachinesToSalles AS ms '.
                       'ON c.NomMachine = ms.NomMachine '.
                       'INNER JOIN Salles AS s '.
                       'ON ms.RefSalle = s.IdSalle '.
                       'WHERE DateDebut LIKE "'.$date->format('Y-m').'%" '.
                       'AND NOT (HeureFin < :excluDebut OR HeureDebut > :excluFin) ';

                $listeJO = implode(',', unserialize($this->_joursOuvres));

                $sql .= 'AND JourSemaine IN ('.$listeJO.') ';
                $sql .= 'AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN) ';

                if ($this->_composante != '') {

                    $sql .= 'AND s.Composante=:composante';

                } else {

                    $clauseIn = implode(",", $this->_salles);

                    $sql .= 'AND s.IdSalle IN ('.$clauseIn.')';
                }

                $query = $pdo->prepare($sql);

                $query->bindParam(':excluDebut', $this->_excluDebut, \PDO::PARAM_STR);
                $query->bindParam(':excluFin', $this->_excluFin, \PDO::PARAM_STR);

                if ($this->_composante != '')
                    $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

                $query->execute();

                $res = $query->fetch(\PDO::FETCH_OBJ);

                $result = $result - $res->total;

                # Le nombre d'heures de la fenetre, servira pour le calcul du complement
                # Si plage d'exclusion, il faut lui retrancher la plage d'exclusion, qui ne doit pas etre comptabilisée dans le nombre d'heures...
                $nb_heures_par_jour -= strtotime($this->_excluFin) - strtotime($this->_excluDebut);
            }

            $dataArray['data'][]       = $result;
            $nbMachinesParMois[]  = $this->getNbMachine($date);
            $dataArray['label'][]      = sprintf("        %s \n[%s terminaux]",$Montharray[$date->format('m')],round($nbMachinesParMois[$i],2));
            $dataArray['complement'][] = $nbMachinesParMois[$i] *  NB_JOURS_SEMAINE_MOYEN_PAR_MOIS * count(unserialize($this->_joursOuvres)) * $nb_heures_par_jour - $result;


            //incrémentation du mois (ajout d'un intervalle de 1 mois)
            $date->add(new \DateInterval('P1M'));
        }


        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $graphData = $this->fetchData();

        /* Create and populate the pData object */
        $myData = new \pData();
        $myData->addPoints($graphData['data'], 'Taux d\'utilisation');
        $myData->addPoints($graphData['complement'], 'Taux de vacance');
        $myData->setAxisName(0, 'Taux d\'utilisation');
        $myData->addPoints($graphData['label'], 'Labels');
        $myData->setSerieDescription('Labels');
        $myData->setAbscissa('Labels');

        $serieSettings = array('R' => 255, 'G' => 0, 'B' => 0, 'Alpha' => 80);
        $myData->setPalette('Taux d\'utilisation', $serieSettings);

        $serieSettings = array('R' => 90, 'G' => 148, 'B' => 30, 'Alpha' => 70);
        $myData->setPalette('Taux de vacance', $serieSettings);

        /* Normalize the data series to 100% */
        $myData->normalize(100, '%');

        /* Create the pChart object */
        $myPicture = new \pImage($this->_largeur, 370, $myData);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 370, DIRECTION_VERTICAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 100));
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 370, DIRECTION_HORIZONTAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 20));
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

        /* Write the picture title */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
        $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));

        /* Set the default font properties */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 9));

        /* Draw the scale and the chart */
        $myPicture->setGraphArea(80, 40, $this->_largeur-100, 210);
        $myPicture->drawScale(array('XMargin' => 1, 'DrawSubTicks' => true, 'LabelRotation' => 45, 'Mode' => SCALE_MODE_ADDALL_START0, 'GridR' => 0, 'GridG' => 0, 'GridB' => 0, 'GridAlpha' => 100, 'DrawXLines' => true));


        /* Turn on shadow processing */
        $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));

        /* Draw the stacked area chart */
        $myPicture->drawStackedAreaChart(array('DrawPlot' => true, 'DrawLine' => true, 'LineSurrounding' => -20, 'ForceTransparency' => 80 ));

        /* Write the chart legend */
        $myPicture->drawLegend(480, 340, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL));

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('example.drawStackedAreaChart.normalized.png');
    }


    /**
     * Exporte les données du graphe
     *
     * @return le tableau des données du graphe
     */
    public function export()
    {
        $graphData = $this->fetchData(); 
        $label     = $this->getXAxis();
        $export=array();
				for ($i=0;$i<12;$i++) 
        {
             $index=sprintf("%02d",($this->_month+$i-1)%12+1);
             $export[$i]["mois"] = $label[$index];
             $export[$i]["nombre_terminaux"] = preg_replace("/.([0-9\.]+) terminaux./","$1",explode("\n", $graphData['label'][$i])[1]);
             if (($graphData['data'][$i]+$graphData['complement'][$i])==0) {
                $export[$i]["taux"] = 0;
             } 
             else {
                $export[$i]["taux"] = round(($graphData['data'][$i]*100)/($graphData['data'][$i]+$graphData['complement'][$i]),1);
             }
        }
        return $export;
    }
}
