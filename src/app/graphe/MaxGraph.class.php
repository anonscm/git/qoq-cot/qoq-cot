<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Ce fichier fait partie du projet QoQ-CoT
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Humain
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Nombre de connexions simultanées" (le 3e dans l'interface)
 * qui s'obtient lorsqu'on spécifie une date de début DD ET une date de fin DF.
 * Il s'agit d'un graphe donnant l'évolution du nombre max de connexions
 * simultanées survenue dans la journée dans la salle -- ou le groupe
 * de salles -- pour chaque jour (en abscisses) entre DD et DF.
 * IDGraphe : 3 avec date de fin
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */


class MaxGraph
{
    private $_dateDebut;

    private $_dateFin;

    private $_heureDebut;

    private $_heureFin;

    private $_composante;

    private $_salles;

    private $_largeur;

    private $_uniqueConn;

    private $_titre;

    /**
     * Constructeur
     *
     * @param string  $dateDebut  la date de début de la période observée
     * @param string  $dateFin    la date de fin de la période observée
     * @param string  $heureDebut l'heure de début de la période observée
     * @param string  $heureFin   l'heure de fin de la période observée
     * @param string  $composante la composante observée
     * @param string  $salles     un array serialize de salles
     * @param integer $largeur    la largeur du graphe
     * @param string  $uniqueConn pour savoir si on affiche toutes les connexions sur une machine
     */
    public function __construct($dateDebut, $dateFin, $heureDebut, $heureFin, $composante, $salles, $largeur, $uniqueConn)
    {
        $this->_dateDebut    = $dateDebut;
        $this->_dateFin      = $dateFin;
        $this->_heureDebut   = $heureDebut;
        $this->_heureFin     = $heureFin;
        $this->_composante   = $composante;
        $this->_salles       = unserialize($salles);
        $this->_largeur      = $largeur;
        $this->_uniqueConn   = $uniqueConn;
        $this->_titre        = $this->initTitre();
        $this->_arrayMachine = array();
    }


    /**
     * Retourne les labels de l'axe des abscisses
     *
     * @return array $arrayX les labels des abscisses
     */
    public function getXAxis()
    {
        $debut = new \DateTime($this->_dateDebut);

        $fin = new \DateTime($this->_dateFin);

        $d = $debut->format('Y-m-d');
        $arrayX= array($d);

        while ($debut < $fin) {

            $d = $debut->add(new \DateInterval('P1D'))->format('Y-m-d');
            $arrayX[] = $d;
        }

        return $arrayX;
    }


    /**
     * Retourne le nombre de machine de la salle ou de la composante
     *
     * @return integer $nbMachine le nombre de machine de la salle ou de la composante
     */
    public function getNbMachine()
    {
        $pdo = \Dao::getInstance();

        $machineArray  = array();

        $jourDebut = new \DateTime($this->_dateDebut);
        $jourFin = new \DateTime($this->_dateFin);

        for ($jour = clone($jourDebut); $jour <= $jourFin; $jour->add(new \DateInterval('P1D'))) {

            $machineArray[$jour->format('Y-m-d')] = 0;

            $sql = 'SELECT NomMachine '.
                   'FROM MachinesToSalles AS ms '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle WHERE ';

            if ($this->_composante != '') {

                $sql .= 's.Composante=:composante ';

            } else {

                $clauseIn = implode(",", $this->_salles);

                $sql .= 's.IdSalle IN ('.$clauseIn.') ';
            }

            $sql .= "AND NOT (ms.Date_FIN < '".$jour->format("Y-m-d")."' OR ms.date_DEBUT > '".$jour->format("Y-m-d")."');";

            $query = $pdo->prepare($sql);


            if ($this->_composante != '')
                $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

            $query->execute();
            $results = $query->fetchAll(\PDO::FETCH_OBJ);

            $machineArray[$jour->format('Y-m-d')] = count($results);
        }

        return $machineArray;
    }


    /**
     * Initialise le titre du graphe
     *
     *  @return string $titre le titre du graphe
     */
    public function initTitre()
    {
        $pdo = \Dao::getInstance();

        if ($this->_composante != '') {

            $sql = 'SELECT DISTINCT Composante FROM Salles '.
                   'WHERE Composante = :composante';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql = 'SELECT NomSalle FROM Salles '.
                   'WHERE IdSalle IN ('.$clauseIn.');';
        }

        $query = $pdo->prepare($sql);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll();

        if ($this->_composante != '') {

            $titre = 'Pic de connexions simultanées pour le groupe de salles '.$results[0]['Composante'].' du '.date('d/m/Y', strtotime($this->_dateDebut)).' au '.date('d/m/Y', strtotime($this->_dateFin));

        } else {

            $count = $query->rowCount();

            if ($count > 1) {

                $salles = '[ ';

                foreach ($results as $salle) {

                    $salles .= $salle['NomSalle'].' ';
                }

                $salles .= ']';

                $titre = 'Pic de connexions simultanées dans le groupe de salles '.$salles.' du '.date('d/m/Y', strtotime($this->_dateDebut)).' au '.date('d/m/Y', strtotime($this->_dateFin));

            } else {

                $titre = 'Pic de connexions simultanées dans la salle '.$results[0]['NomSalle'].' du '.date('d/m/Y', strtotime($this->_dateDebut)).' au '.date('d/m/Y', strtotime($this->_dateFin));
            }
        }

        return $titre;
    }


    /**
     * méthode qui permet de comparer deux créneaux horaire d'une même journée
     *
     * @param mixed $a premier créneau horaire
     * @param mixed $b deuxième créneau horaire
     *
     * @return float le résultat de la comparaison
     */
    public function cmp($a,$b)
    {
        if ($a->creneau == $b->creneau) {
            if ($a->deboufin == $b->deboufin) {
                return 0;
            }
            return ($a->deboufin < $b->deboufin) ? -1 : 1;
        }
        return ($a->creneau < $b->creneau) ? -1 : 1;
    }


    /**
     * méthode qui retourne le maximum de connexion par jour
     * en prenant en compte les connexions simultanées sur une machine
     *
     * @param mixed $intervalles un objet contenant les intervalles de temps à comparer
     *
     * @return integer le max des connexions
     */
    public function getBigMax($intervalles)
    {
        usort($intervalles, array($this, 'cmp'));

        $max = 0;
        $ouverture = 0;

        foreach ($intervalles as $intervalle) {


            if ($intervalle->deboufin==0) {

                $ouverture++;

            } else {

                $ouverture--;
            }
            if ($ouverture>$max) $max++;
        }

        return $max;
    }


    /**
     * Retourne le max de connexion simultanées du jour
     *
     * @param mixed $intervalles un array contenant les dates de début et de fin de connexion
     *
     * @return integer $max le nombre max de connexion simultanées du jour
     */
    public function getMax($intervalles)
    {
        usort($intervalles, array($this, 'cmp'));

        $max = 0;
        $ouverture = 0;


        foreach ($intervalles as $intervalle) {


            if ($intervalle->deboufin == 0) {

                $this->_arrayMachine[$intervalle->nomMachine]++;



                if ($this->_arrayMachine[$intervalle->nomMachine]<2)
                     $ouverture++;

            } else {
                $this->_arrayMachine[$intervalle->nomMachine]--;
                if ($this->_arrayMachine[$intervalle->nomMachine]<2)
                     $ouverture--;
            }
            if ($ouverture>$max) $max++;
        }

        return $max;
    }


    /**
     * Récupère les données du graphe
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $pdo = \Dao::getInstance();

        $dataArray  = array();

        $jourDebut = new \DateTime($this->_dateDebut);
        $jourFin = new \DateTime($this->_dateFin);

        for ($jour = clone($jourDebut); $jour <= $jourFin; $jour->add(new \DateInterval('P1D'))) {

            $dataArray[$jour->format('Y-m-d')] = 0;
        }

        $tableConnexions = \Dao::getTableConnexions($jourDebut);

        $sql = 'SELECT DateDebut, DateFin, Jour, ms.NomMachine '.
               'FROM '.$tableConnexions.' AS c, MachinesToSalles AS ms, Salles AS s '.
               'WHERE ms.RefSalle = s.IdSalle '.
               'AND ms.NomMachine = c.NomMachine '.
               'AND NOT (DateFin < :dateDebut '.
               'OR DateDebut > :dateFin) '.
               'AND NOT (HeureFin < :heureDebut '.
               'OR HeureDebut > :heureFin) '.
               'AND NOT (DateFin < ms.Date_DEBUT '.
               'OR DateDebut > ms.Date_FIN) '.
               'AND DateFin IS NOT NULL ';

        if ($this->_composante != '') {

            $sql .= 'AND Composante = :composante ';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql .= 'AND s.IdSalle IN ('.$clauseIn.') ';
        }

        $sql .= 'ORDER BY Jour';

        $query = $pdo->prepare($sql);

        $dateDebut = $this->_dateDebut.' '.$this->_heureDebut;
        $dateFin   = $this->_dateFin.' '.$this->_heureFin;

        $query->bindParam(':dateDebut', $dateDebut, \PDO::PARAM_STR);
        $query->bindParam(':dateFin', $dateFin, \PDO::PARAM_STR);
        $query->bindParam(':heureDebut', $this->_heureDebut, \PDO::PARAM_STR);
        $query->bindParam(':heureFin', $this->_heureFin, \PDO::PARAM_STR);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_OBJ);

        if (!empty($results)) {

            $j = 0;
            $intervalles = array();
            $intervalles[$j++] = new Intervalle($results[0]->DateDebut, 0, $results[0]->NomMachine);
            $intervalles[$j++] = new Intervalle($results[0]->DateFin, 1, $results[0]->NomMachine);

            $jour = $results[0]->Jour;

            for ($i = 1; $i < count($results); $i++) {

                if ($jour != $results[$i]->Jour) {

                    if ($this->_uniqueConn === 'unique') {

                        $dataArray[$jour] =  $this->getMax($intervalles);

                    } else {

                        $dataArray[$jour] = $this->getBigMax($intervalles);
                    }

                    $intervalles = array();
                    $jour = $results[$i]->Jour;
                    $j = 0;
                }

                $intervalles[$j++] = new Intervalle($results[$i]->DateDebut, 0, $results[$i]->NomMachine);
                $intervalles[$j++] = new Intervalle($results[$i]->DateFin, 1, $results[$i]->NomMachine);
            }

            if ($this->_uniqueConn === 'unique') {

                $dataArray[$results[count($results)-1]->Jour] = $this->getMax($intervalles);

            } else {

                $dataArray[$results[count($results)-1]->Jour] = $this->getBigMax($intervalles);
            }
        }
        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $nbMachine_array = $this->getNbMachine();
        $nbMachine = max($nbMachine_array);
        $graphData = $this->fetchData();

	# Calcul de la moyenne
	$moyenne = 0;
	foreach ($graphData as $key => $value)
	{
		$moyenne+=$value;
	}
	$moyenne /= sizeof($graphData);

        /* Create and populate the pData object */
        $myData = new \pData();
        $myData->addPoints($graphData, 'Maximum de connexions simultanées');
        $myData->addPoints($nbMachine_array, 'Nombre de terminaux');
        $myData->setSerieWeight('Maximum de connexions simultanées', 1);
        $myData->setSerieWeight('Nombre de terminaux', 1);
        $myData->setAxisName(0, 'Pic de connexions');
        $myData->addPoints($this->getXAxis(), 'Labels');
        $myData->setSerieDescription('Labels', 'Months');
        $myData->setAbscissa('Labels');

        /* Create the pChart object */
        $myPicture = new \pImage($this->_largeur, 320, $myData);

        /* Turn of Antialiasing */
        $myPicture->Antialias = false;

        /* Draw the background */
        $settings = array('R' => 170, 'G' => 183, 'B' => 87, 'Dash' => 1, 'DashR' => 190, 'DashG' => 203, 'DashB' => 107);
        $myPicture->drawFilledRectangle(0, 0, $this->_largeur, 320, $settings);

        /* Overlay with a gradient */
        $settings = array('StartR' => 219, 'StartG' => 231, 'StartB' => 139, 'EndR' => 1, 'EndG' => 138, 'EndB' => 68, 'Alpha' => 50);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 320, DIRECTION_VERTICAL, $settings);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

        /* Add a border to the picture */
        $myPicture->drawRectangle(0, 0, $this->_largeur-1, 319, array('R' => 0, 'G' => 0, 'B' => 0));

        /* Write the chart title */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11, 'R' => 255, 'G' => 255, 'B' => 255));
        $myPicture->drawText(10, 20, $this->_titre, array('FontSize' => 11, 'Align' => TEXT_ALIGN_BOTTOMLEFT));

        /* Set the default font */
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Forgotte.ttf', 'FontSize' => 11, 'R' => 0, 'G' => 0, 'B' => 0));

        /* Define the chart area */
        $myPicture->setGraphArea(60, 40, $this->_largeur-50, 240);

        /* Draw the scale */
        $axisBoundaries = array(0 => array('Min' => 0, 'Max' => $nbMachine+round(0.2*$nbMachine)));
        $scaleSettings  = array('XMargin' => 10, 'YMargin' => 10, 'Floating' => true, 'GridR' => 200, 'GridG' => 200, 'GridB' => 200, 'Mode' => SCALE_MODE_MANUAL, 'ManualScale' => $axisBoundaries, 'DrawSubTicks' => true, 'DrawArrows' => true, 'ArrowSize' => 6, 'LabelRotation' => 45);
        $myPicture->drawScale($scaleSettings);


        //$myPicture->drawThreshold($nbMachine, array('WriteCaption' => true, 'Caption' => $nbMachine, 'BoxAlpha' => 100, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));
	# La moyenne...
        $myPicture->drawThreshold($moyenne, array('WriteCaption' => true, 'Caption' => "Moyenne : ".round($moyenne,2), 'BoxAlpha' => 40, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));

        /* Turn on Antialiasing */
        $myPicture->Antialias = true;

        /* Enable shadow computing */
        $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));

        /* Draw the line chart */
        $myPicture->drawLineChart();
        $myPicture->drawPlotChart(array('DisplayValues' => true, 'PlotBorder' => true, 'BorderSize' => 2, 'Surrounding' => -60, 'BorderAlpha' => 80));

        /* Write the chart legend */
        $myPicture->drawLegend(590, 303, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL, 'FontR' => 255, 'FontG' => 255, 'FontB' => 255));

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('pictures/example.drawLineChart.plots.png');
    }
}


/**
 * Classe décrivant les intervalles de temps à comparer
 *
 */
class Intervalle
{
    var $creneau;

    var $deboufin;

    var $nomMachine;

    /**
     * Constructeur de la classe Intervalle
     *
     * @param string   $creneau    un creneau horaire
     * @param interger $deboufin   0 pour le début d'une connexion, 1 pour la fin
     * @param string   $nomMachine le nom de la machine
     */
    function __construct($creneau,$deboufin,$nomMachine)
    {
        $this->creneau    = $creneau;
        $this->deboufin   = $deboufin;
        $this->nomMachine = $nomMachine;
    }
}
