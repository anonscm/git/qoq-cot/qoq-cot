<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of QoQ-CoT.
 * 
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Déclaration de permission de copie
 * 
 * Ce fichier fait partie de QoQ-CoT.
 * 
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */
/**
 * Fichier qui va contenir l'image du graphe
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once '../../lib/pChart/class/pData.class.php';
require_once '../../lib/pChart/class/pDraw.class.php';
require_once '../../lib/pChart/class/pImage.class.php';
require_once '../../lib/pChart/class/pSurface.class.php';
require_once '../../lib/pChart/class/pPie.class.php';

require_once 'DureeGraph.class.php';
require_once 'CumulDureeGraph.class.php';
require_once 'CpuGraph.class.php';
require_once 'CpuDetailGraph.class.php';
require_once 'MaxComposanteGraph.class.php';
require_once 'HumainGraph.class.php';
require_once 'MaxGraph.class.php';
require_once 'OsGraph.class.php';


$validTypeArray = array('duree', 'cumulduree', 'cpu', 'cpudetail', 'maxcomposante', 'humain', 'max', 'os');

$date       = date('Y-m-d');
$datefin    = '';
$mois       = '01';
$salle      = null;
$salles     = array();
$composante = null;
$pas        = null;
$year       = null;
$largeur    = '';
$heureDebut = '';
$heureFin   = '';
$excluDebut = '';
$excluFin   = '';

if (isset($_GET['uniqueconn'])) {
    $uniqueConn = $_GET['uniqueconn'];
}


if (in_array($_GET['type'], $validTypeArray)) {

    $type = $_GET['type'];

} else {

    echo 'Type invalide';
}

if (isset($_GET['date'])) {

    if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['date'], $arrayDate)) {

        $date = $arrayDate[0];

    } else {

        echo 'Date invalide';
    }
}

if (isset($_GET['datefin'])) {

    if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['datefin'], $arrayDate)) {

        $datefin = $arrayDate[0];

    } else {

        echo 'Date invalide';
    }
}


$validMoisArray = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');

if (isset($_GET['mois'])) {

    if (in_array($_GET['mois'], $validMoisArray)) {

        $month = $_GET['mois'];

    } else {

        echo 'Mois invalide';
    }
}

if (isset($_GET['salle'])) {

    $salle = intval($_GET['salle']);
}

if (isset($_GET['salles'])) {

    $salles = $_GET['salles'];
}

if (isset($_GET['composante'])) {

    $composante = htmlspecialchars($_GET['composante'], ENT_QUOTES);
}

if (isset($_GET['pas'])) {

    $pas = intval($_GET['pas']);
}

if (isset($_GET['annee'])) {

    $year = intval($_GET['annee']);
}


if (isset($_GET['largeur'])) {

    $largeur = intval($_GET['largeur']);
}

if (isset($_GET['heuredebut'])) {

    $heureDebut = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
}

if (isset($_GET['heurefin'])) {

    $heureFin = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
}

if (isset($_GET['excludebut'])) {

    $excluDebut = htmlspecialchars($_GET['excludebut'], ENT_QUOTES);
}

if (isset($_GET['exclufin'])) {

    $excluFin = htmlspecialchars($_GET['exclufin'], ENT_QUOTES);
}

if (isset($_GET['joursouvres'])) {

    $joursOuvresArray = $_GET['joursouvres'];
}

$graph = '';


switch ($type) {

case 'duree':
    $graph = new DureeGraph($date, $pas, $salle, $largeur, $heureDebut, $heureFin, 1);
    break;

case 'cumulduree':
    $graph = new CumulDureeGraph($date, $datefin, $pas, $salle, $largeur, $heureDebut, $heureFin, $joursOuvresArray);
    break;

case 'cpu':
    $graph = new CpuGraph($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
    break;

case 'cpudetail':
    $graph = new CpuDetailGraph($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
    break;

case 'maxcomposante':
    $graph = new MaxComposanteGraph($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
    break;

case 'humain':
    $graph = new HumainGraph($date, $composante, $pas, $salles, $largeur, $heureDebut, $heureFin);
    break;

case 'max':
    $graph = new MaxGraph($date, $datefin, $heureDebut, $heureFin, $composante, $salles, $largeur, $uniqueConn);
    break;

case 'os':
    $graph = new OsGraph($date, $datefin, $salles, $composante);
    break;
}

$graph->display();
