<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of QoQ-CoT.
 * 
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Déclaration de permission de copie
 * 
 * Ce fichier fait partie de QoQ-CoT.
 * 
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */
/**
 * Ce fichier fait partie du projet QoQ-CoT
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Cpu
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Taux d'utilisation annuel" (le 2e dans l'interface)
 * lorsqu'on choisit un groupe de salles : c'est alors le 3e des 3 graphes
 * affichés.
 * Il s'agit d'un graphe donnant, pour chaque salle du groupe, sous forme
 * d'histogrammes, le taux d'utilisation annuel sous forme de pourcentage.
 * IDGraphe : 2.3
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
class MaxComposanteGraph
{
    private $_month;

    private $_year;
 
    private $_composante;

    private $_salles;

    private $_largeur;

    private $_heureDebut;

    private $_heureFin;

    private $_joursOuvres;

    private $_excluDebut;

    private $_excluFin;

    private $_titre;

    /**
     * Constructeur
     *
     * @param string  $month       le mois choisi
     * @param string  $year        l'année choisie
     * @param string  $composante  la composante observée
     * @param array   $salles      un array serialize de salles
     * @param integer $largeur     la largeur du graphe
     * @param string  $heureDebut  l'heure d'ouverture des salles
     * @param string  $heureFin    l'heure de fermeture des salles
     * @param string  $joursOuvres array des jours ouvrés serializé
     */
    public function __construct($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvres, $excluDebut, $excluFin)
    {
        $this->_month       = $month;
        $this->_year        = $year;
        $this->_composante  = $composante;
        $this->_salles      = unserialize($salles);
        $this->_largeur     = $largeur;
        $this->_heureDebut  = $heureDebut;
        $this->_heureFin    = $heureFin;
        $this->_joursOuvres = $joursOuvres;
        $this->_excluDebut  = $excluDebut;
        $this->_excluFin    = $excluFin;
    }

    /**
     * Retourne le nombre de machine
     *
     * @param string $nomSalle le nom de la salle
     *
     * @return integer $nbMachine le nombre de machine
     */
    public function getNbMachine($nomSalle, $date)
    {
      $pdo = \Dao::getInstance();

      $datestr1 = $date->format("Y-m-d");
      $dateFinAnnee = clone $date;
      $dateFinAnnee->add(new \DateInterval('P1Y'));
      $datestr2 = $dateFinAnnee->format("Y-m-d");
      $diff = $dateFinAnnee->diff($date);

      //cette requete calcule le nombre moyen de machines présentes dans l'année
      //prend donc en compte le fait qu'une machine ne soit présente qu'à une
      // certaine période de l'année
      $sql = "SELECT sum(datediff(if(date_FIN > '".$datestr2."', '".$datestr2."', date_FIN), if(date_DEBUT < '".$datestr1."', '".$datestr1."', date_DEBUT))/".$diff->days.")".
             " FROM MachinesToSalles as ms INNER JOIN Salles s ON ms.RefSalle = s.IdSalle WHERE NomSalle = '".$nomSalle."' ".
             "AND NOT (ms.Date_FIN < '".$datestr1."' OR ms.date_DEBUT > '".$datestr2."');";
      $query = $pdo->prepare($sql);
      $query->execute();


      $results = $query->fetch();
      $result = $results[0];


      return $result;

    }


    /**
     * Retourne un array contenant les labels des abscisses
     *
     * @return array $arrayMois les labels des abscisses
     */
    public function getXAxis()
    {
        $pdo = \Dao::getInstance();

        $sql = 'SELECT NomSalle, IdSalle AS id ';

        $sql .= 'FROM MachinesToSalles as ms '.
                'INNER JOIN Salles s '.
                'ON ms.RefSalle = s.IdSalle '.
                'WHERE NOT (ms.Date_FIN < :dateDebut OR ms.date_DEBUT > :dateFin) ';

        if ($this->_composante != '') {

            $sql .= 'AND Composante = :composante ';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql .= 'AND RefSalle IN ('.$clauseIn.') ';
        }

        $sql .= 'GROUP BY s.IdSalle';

        $query = $pdo->prepare($sql);

        $date = new \DateTime($this->_year.'-'.$this->_month.'-01');
	$dateDebut = $date->format("Y-m-d");
        $query->bindParam(":dateDebut", $dateDebut, \PDO::PARAM_STR);

        $date->add(new \DateInterval('P1Y'));
        $date->sub(new \DateInterval('P1D'));
	$dateFin = $date->format("Y-m-d");
        $query->bindParam(":dateFin", $dateFin, \PDO::PARAM_STR);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll(\PDO::FETCH_COLUMN);

        if ($this->_composante != '') {

            $this->_titre = 'Taux d\'utilisation annuel par salle pour le groupe de salles '.$this->_composante;

        } else {

            $salles = '[ ';

            foreach ($results as $key => $val) {

                $salles .= $val.' ';
            }

            $salles .= ' ]';
            
            $this->_titre = 'Taux d\'utilisation annuel par salle pour le groupe de salles '.$salles;
        }

        return $results;
    }


    /**
     * Récupère les données
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $pdo = \Dao::getInstance();

        $debut = new \DateTime($this->_year.'-'.$this->_month.'-01');

        $tableConnexions = \Dao::getTableConnexions($debut);

        $d = clone($debut);

        $fin = $d->add(new \DateInterval('P1Y'));

        $arraySalle = $this->getXAxis();

        foreach ($arraySalle as $nomSalle) {

		$sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :heureFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :heureDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE NOT (DateFin < "'.$debut->format('Y-m-d').'" OR DateDebut > "'.$fin->format('Y-m-d').'") '.
                   'AND NOT (HeureFin < :heureDebut OR HeureDebut > :heureFin) '.
                   'AND NomSalle = "'.$nomSalle.'" ';

            $listeJO = implode(',', unserialize($this->_joursOuvres));

            $sql .= 'AND JourSemaine IN ('.$listeJO.') ';
            $sql .= 'AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN)';
            $query = $pdo->prepare($sql);

            $query->bindParam(':heureDebut', $this->_heureDebut, \PDO::PARAM_STR);
            $query->bindParam(':heureFin', $this->_heureFin, \PDO::PARAM_STR);

            $query->execute();

            $results = $query->fetch(\PDO::FETCH_OBJ);

            $result = $results->total;

            # Le nombre d'heures de la fenetre, servira pour le calcul du complement
            $nb_heures_par_jour = strtotime($this->_heureFin) - strtotime($this->_heureDebut);

            # Cas avec plage d'exclusion
            if ($this->_excluDebut !== '' && $this->_excluFin !== '') {

                $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :excluFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :excluDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE NOT (DateFin < "'.$debut->format('Y-m-d').'" OR DateDebut > "'.$fin->format('Y-m-d').'") '.
                   'AND NOT (HeureFin < :excluDebut OR HeureDebut > :excluFin) '.
                   'AND NomSalle = "'.$nomSalle.'" ';

                $listeJO = implode(',', unserialize($this->_joursOuvres));

                $sql .= 'AND JourSemaine IN ('.$listeJO.');';

                $query = $pdo->prepare($sql);

                $query->bindParam(':excluDebut', $this->_excluDebut, \PDO::PARAM_STR);
                $query->bindParam(':excluFin', $this->_excluFin, \PDO::PARAM_STR);

                $query->execute();

                $res = $query->fetch(\PDO::FETCH_OBJ);

                $result = $result - $res->total;

                # Le nombre d'heures de la fenetre, servira pour le calcul du complement
		# Si plage d'exclusion, il faut lui retrancher la plage d'exclusion, qui ne doit pas etre comptabilisée dans le nombre d'heures...
                $nb_heures_par_jour -= strtotime($this->_excluFin) - strtotime($this->_excluDebut);
            }

            $dataArray['data'][]       = $result;
            $dataArray['complement'][] = $this->getNbMachine($nomSalle,$debut) * NB_SEMAINES_MOYEN_PAR_AN * count(unserialize($this->_joursOuvres)) * $nb_heures_par_jour - $result;
        }

        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $graphData = $this->fetchData();
        $label     = $this->getXAxis();
        $debut = new \DateTime($this->_year.'-'.$this->_month.'-01');

        # On ajoute aux noms de salles, sur les labels des abscisses, le nombre de machines de la salle entre crochets en bout de chaîne
        foreach ($label as $key => $value)
        {
                $label[$key] = $value." \n[".round($this->getNbMachine($value,$debut),2)." terminaux]";
                #$label[$key] = $value;
        }

        $myData = new \pData();  
        $myData->addPoints($graphData['data'], 'Taux d\'utilisation');
        $myData->addPoints($graphData['complement'], 'Taux de vacance');

        $myData->setAxisName(0, 'Taux d\'utilisation');
        $myData->addPoints($label, 'Labels');
        $myData->setSerieDescription('Labels', 'Months');
        $myData->setAbscissa('Labels');

        $serieSettings = array('R' => 255, 'G' => 0, 'B' => 0, 'Alpha' => 80);
        $myData->setPalette('Taux d\'utilisation', $serieSettings);

        $serieSettings = array('R' => 90, 'G' => 148, 'B' => 30, 'Alpha' => 70);
        $myData->setPalette('Taux de vacance', $serieSettings);

        /* Normalize all the data series to 100% */
        $myData->normalize(100, '%');
        
        /* Create the pChart object */
        $myPicture = new \pImage($this->_largeur, 400, $myData);
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 400, DIRECTION_VERTICAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 100));
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 400, DIRECTION_HORIZONTAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 20));
        $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

        /* Write the picture title */ 
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
        $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));

        /* Set the default font properties */
	# Ici on choisit la fonte pour les abscisses et les ordonnées qu'on va tracer avec drawScale
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 9));

        /* Draw the scale and the chart */
        $myPicture->setGraphArea(150, 40, $this->_largeur - 20, 190);
        $myPicture->drawScale(array('DrawSubTicks' => true, 'Mode' => SCALE_MODE_ADDALL_START0, 'LabelRotation' => 45));
        $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));
        $myPicture->drawStackedBarChart(array('DisplayValues' => true, 'DisplayColor' => DISPLAY_AUTO, 'Gradient' => true, 'Surrounding' => 30, 'InnerSurrounding' => 20));
        $myPicture->setShadow(false);
        
        /* Write the chart legend */
        $myPicture->drawLegend(480, 370, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL));
        
        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('pictures/example.drawStackedBarChart.shaded.png'); 
    }


    /**
     * Exporte les données du graphe
     *
     * @return le tableau des données du graphe
     */
    public function export()
    {
        $graphData = $this->fetchData(); 
        $debut     = new \DateTime($this->_year.'-'.$this->_month.'-01');
        $label     = $this->getXAxis();
        $i         = 0;
	$export=array();
        foreach ($label as $key => $value)
        {
             $export[$i]["salle"] = $value;
             $export[$i]["nombre_terminaux"] = round($this->getNbMachine($value,$debut),2);
             if (($graphData['data'][$i]+$graphData['complement'][$i])==0) {
                $export[$i]["taux"] = 0;
             } 
             else {
                $export[$i]["taux"] = round(($graphData['data'][$i]*100)/($graphData['data'][$i]+$graphData['complement'][$i]),1);
             }
             $i++;
        }
        return $export;
    }
}
