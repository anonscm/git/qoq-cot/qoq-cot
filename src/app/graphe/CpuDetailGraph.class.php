<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Ce fichier fait partie du projet QoQ-CoT
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type CpuDetail
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Taux d'utilisation annuel" (le 2e dans l'interface)
 * lorsqu'on choisit un groupe de salles : c'est alors le 2e des 3 graphes
 * affichés.
 * Il s'agit d'un graphe donnant, pour chaque salle du groupe, sous forme
 * d'histogrammes, le nombre moyen d'heures d'utilisation des machines de la salle.
 * Ainsi que, sous forme d'une ligne, le nombre d'heures d'utilisation moyen de l'ensemble des machines considerees.
 * IDGraphe : 2.2
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */


class CpuDetailGraph
{
    private $_month;

    private $_year;

    private $_composante;

    private $_salles;

    private $_largeur;

    private $_heureDebut;

    private $_heureFin;

    private $_joursOuvres;

    private $_excluDebut;

    private $_excluFin;

    private $_titre;


    /**
     * Constructeur
     *
     * @param string  $month       le mois choisi
     * @param string  $year        l'année choisie
     * @param string  $composante  la composante observée
     * @param array   $salles      un array serialize de salles
     * @param integer $largeur     la largeur du graphe
     * @param string  $heureDebut  l'heure d'ouverture des salles
     * @param string  $heureFin    l'heure de fermeture des salles
     * @param string  $joursOuvres array des jours ouvrés serializé
     * @param string  $exclutDebut l'heure de début de la plage à exclure
     * @param string  $excluFin    l'heure de fin de la plage à exclure
     */
    public function __construct($month, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvres, $excluDebut, $excluFin)
    {
        $this->_month       = $month;
        $this->_year        = $year;
        $this->_composante  = $composante;
        $this->_salles      = unserialize($salles);
        $this->_largeur     = $largeur;
        $this->_heureDebut  = $heureDebut;
        $this->_heureFin    = $heureFin;
        $this->_joursOuvres = $joursOuvres;
        $this->_excluDebut  = $excluDebut;
        $this->_excluFin    = $excluFin;
    }

    /**
     * méthode qui permet de comparer deux créneaux horaire d'une même journée
     *
     * @param mixed $a premier créneau horaire
     * @param mixed $b deuxième créneau horaire
     *
     * @return float le résultat de la comparaison
     */
    public function tri($a,$b)
    {
        if ($a->creneau == $b->creneau) {
            if ($a->deboufin == $b->deboufin) {
                return 0;
            }
            return ($a->deboufin < $b->deboufin) ? -1 : 1;
        }
        return ($a->creneau < $b->creneau) ? -1 : 1;
    }


    /**
     *  Cette fonction détermine si il est possible ou  non de tracer le graphe,
     *  dans le cas où ça ne l'est pas, il affiche une phrase indiquant que le 2e
     *  graphe ne peu pas être affiché
     *
     *  @return retourne 1 si le graphe est faisable, 0 sinon.
     */

    public function faisable()
    {
      $pdo = \Dao::getInstance();

      $date = new \DateTime($this->_year.'-01-01');
      $datestrDebut = $date->format("Y-m-d");
      $dateFinAnnee = clone $date;
      $dateFinAnnee->add(new \DateInterval('P1Y'));
      $dateFinAnnee->sub(new \DateInterval('P1D'));
      $datestrFin = $dateFinAnnee->format("Y-m-d");

      $arraySalle = $this->getXAxis();

      $faisable_bool = 1;
      foreach ($arraySalle as $nomSalle) {
          if($faisable_bool){
            $intervalles = array();
            //requete SQL qui récupère toutes les machines de la salle présentes
            //sur l'intervalle séléctionné (ici l'année), et qui en extrait les
            //date_DEBUT et date_FIN de chaque machine, celles-ci étant remplacées par
            //les bornes de l'intervalle demandé si date_DEBUT ou date_FIN se situent en dehors de cet intervalle.
            $sql = "SELECT if(date_DEBUT < '".$datestrDebut."', '".$datestrDebut."', date_DEBUT),if(date_FIN > '".$datestrFin."', '".$datestrFin."', date_FIN)".
                   "FROM MachinesToSalles as ms INNER JOIN Salles s ON ms.RefSalle = s.IdSalle ".
                   "WHERE NomSalle = '".$nomSalle."' AND NOT (ms.Date_FIN < '".$datestrDebut."' OR ms.date_DEBUT > '".$datestrFin."')";
            $query = $pdo->prepare($sql);
            $query->execute();

            $results = $query->fetchAll();
            $j=0;
            //boucle qui remplit un objet IntervalleCPU avec toutes les dates
            //récupérées par la requête SQL
            for($i=0;$i<count($results);$i++){
              $intervalles[$j++] = new IntervalleCPU($results[$i][0],0);
              $intervalles[$j++] = new IntervalleCPU($results[$i][1],1);
            }
            //trie les dates contenues dans la variable intervalles grâce à la
            //fonction tri($a,$b).
            usort($intervalles, array($this, 'tri'));
            //test si premiere machine <= debut de l'annee ou derniere machine >= fin de l'annee
            if($intervalles==NULL || $intervalles[0]->creneau > $datestrDebut || $intervalles[count($intervalles)-1]->creneau < $datestrFin ){
              $faisable_bool = 0;
            }else {

              //compteur qui s'incrémente quand il y a une date de Debut
              //et qui se décrémente sinon
              //quand il retombe a 0, il faut que la dernière date testée
              //soit celle de la date de fin d'année
              $i = 0;
              $cpt = 0;
              do{
                if($intervalles[$i]->deboufin == 0){
                  $cpt++;
                }else{
                  $cpt--;
                }
                $i++;
              }while($cpt > 0 && $i != count($intervalles));
              if($intervalles[--$i]->creneau < $datestrFin){
                  $faisable_bool = 0;
              }
            }
          }
      }
      return $faisable_bool;
    }

    /**
     * retourne le nombre de jours dans le mois
     *  @param  le 1er jour du mois (ex: 2017-06-01)
     *  @return integer $nombre de jours dans le mois
     */

    public function getJoursMois($date)
    {
      $nbJoursMois = cal_days_in_month(CAL_GREGORIAN, $date->format('m'), $date->format('y'));
      return $nbJoursMois;
    }

    /**
     * Retourne le nombre de machine
     *
     * @param string $nomSalle le nom de la salle
     *
     * @return integer $nbMachine le nombre de machine
     */
    public function getNbMachine($nomSalle, $date)
    {
      $pdo = \Dao::getInstance();

      $datestr1 = $date->format("Y-m-d");
      $dateFinAnnee = clone $date;
      $dateFinAnnee->add(new \DateInterval('P1Y'));
      $datestr2 = $dateFinAnnee->format("Y-m-d");
      $diff = $dateFinAnnee->diff($date);


      //cette requete calcule le nombre moyen de machines présentes dans l'année
      //prend donc en compte le fait qu'une machine ne soit présente qu'à une
      // certaine période de l'année
      $sql = "SELECT sum(datediff(if(date_FIN > '".$datestr2."', '".$datestr2."', date_FIN), if(date_DEBUT < '".$datestr1."', '".$datestr1."', date_DEBUT))/".$diff->days.")".
             " FROM MachinesToSalles as ms INNER JOIN Salles s ON ms.RefSalle = s.IdSalle WHERE NomSalle = '".$nomSalle."' ".
             "AND NOT (ms.Date_FIN < '".$datestr1."' OR ms.date_DEBUT > '".$datestr2."');";
      $query = $pdo->prepare($sql);
      $query->execute();


      $results = $query->fetch();
      $result = $results[0];

      return $result;

    }


    /**
     * Retourne un array contenant les labels des abscisses
     *
     * @return array $arrayMois les labels des abscisses
     */
    public function getXAxis()
    {
        $pdo = \Dao::getInstance();

        $sql = 'SELECT NomSalle, IdSalle AS id ';

        $sql .= 'FROM MachinesToSalles as ms '.
                'INNER JOIN Salles s '.
                'ON ms.RefSalle = s.IdSalle '.
                'WHERE NOT (ms.Date_FIN < :dateDebut OR ms.date_DEBUT > :dateFin) ';

        if ($this->_composante != '') {

            $sql .= 'AND Composante = :composante ';

        } else {

            $clauseIn = implode(",", $this->_salles);

            $sql .= 'AND RefSalle IN ('.$clauseIn.') ';
        }

        $sql .= 'GROUP BY s.IdSalle';

        $query = $pdo->prepare($sql);

        $date = new \DateTime($this->_year.'-'.$this->_month.'-01');
	$dateDebut = $date->format("Y-m-d");
        $query->bindParam(":dateDebut", $dateDebut, \PDO::PARAM_STR);

        $date->add(new \DateInterval('P1Y'));
        $date->sub(new \DateInterval('P1D'));
	$dateFin = $date->format("Y-m-d");
        $query->bindParam(":dateFin", $dateFin, \PDO::PARAM_STR);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll(\PDO::FETCH_COLUMN);

        if ($this->_composante != '') {

            $this->_titre = 'Utilisation annuelle détaillée pour le groupe de salles '.$this->_composante;

        } else {

            $salles = '[ ';

            foreach ($results as $key => $val) {

                $salles .= $val.' ';
            }

            $salles .= ']';

                $this->_titre = 'Utilisation annuelle détaillée pour le groupe de salles '.$salles;
        }

        return $results;
    }


    /**
     * Récupère les données
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $pdo = \Dao::getInstance();

        $debut = new \DateTime($this->_year.'-'.$this->_month.'-01');

        $tableConnexions = \Dao::getTableConnexions($debut);

        $d = clone($debut);

        $fin = $d->add(new \DateInterval('P1Y'));

        $arraySalle = $this->getXAxis();

        foreach ($arraySalle as $nomSalle) {

 	   $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :heureFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :heureDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE NOT (DateFin < "'.$debut->format('Y-m-d').'" OR DateDebut > "'.$fin->format('Y-m-d').'") '.
                   'AND NOT (HeureFin < :heureDebut OR HeureDebut > :heureFin) '.
                   'AND NomSalle = "'.$nomSalle.'" ';

            $listeJO = implode(',', unserialize($this->_joursOuvres));

            $sql .= 'AND JourSemaine IN ('.$listeJO.') ';
            $sql .= 'AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN)';


            $query = $pdo->prepare($sql);

            $query->bindParam(':heureDebut', $this->_heureDebut, \PDO::PARAM_STR);
            $query->bindParam(':heureFin', $this->_heureFin, \PDO::PARAM_STR);

            $query->execute();

            $results = $query->fetch(\PDO::FETCH_OBJ);

            $result = $results->total;

           # Cas de la presence d'une plage d'exclusion
           if ($this->_excluDebut !== '' && $this->_excluFin !== '') {



               $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :excluFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :excluDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' AS c '.
                   'INNER JOIN MachinesToSalles AS ms '.
                   'ON c.NomMachine = ms.NomMachine '.
                   'INNER JOIN Salles AS s '.
                   'ON ms.RefSalle = s.IdSalle '.
                   'WHERE NOT (DateFin < "'.$debut->format('Y-m-d').'" OR DateDebut > "'.$fin->format('Y-m-d').'") '.
                   'AND NOT (HeureFin < :excluDebut OR HeureDebut > :excluFin) '.
                   'AND NomSalle = "'.$nomSalle.'" ';

                $listeJO = implode(',', unserialize($this->_joursOuvres));

                $sql .= 'AND JourSemaine IN ('.$listeJO.');';

                $query = $pdo->prepare($sql);

                $query->bindParam(':excluDebut', $this->_excluDebut, \PDO::PARAM_STR);
                $query->bindParam(':excluFin', $this->_excluFin, \PDO::PARAM_STR);

                $query->execute();

                $res = $query->fetch(\PDO::FETCH_OBJ);

                $result = $result - $res->total;
            }

	          # On divise à la fin par le nombre de machines de la salle sur la periode pour obtenir la moyenne par machine
            $dataArray[] = floatval(number_format($result/3600/$this->getNbMachine($nomSalle, $debut), 1, '.', ''));

        }

        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        if ($this->faisable()) {
          $graphData = $this->fetchData();
          $label     = $this->getXAxis();
          $date = new \DateTime($this->_year.'-01-01');


  	# On ajoute aux noms de salles, sur les labels des abscisses, le nombre de machines de la salle entre crochets en bout de chaîne
	#
  	# Et on en profite pour calculer la moyenne du nb d'heures d'utilisation de l'ensemble des machines considerees. Pour ca on recupere
	# la moyenne par salle ($graphData[$key]) qu'on multiplie par le nb de machine de la salle, on a alors le nb d'heures d'utilisation
	# total pour la salle. On fait ca pour chaque salle, on somme le tout et on divise par le nombre total des machines presentes dans
	# l'ensemble des salles consideres. Notons que ce calcul est très différent d'une moyenne des moyennes par salle, qui ne tient pas
	# compte du nombre de machines par salle... C'est a partir de la version 4.0 Rocky qu'on remplace la moyenne des moyennes (donnee
	# non vraiment significative car independante du nombre de machines par salle) par la moyenne sur l'ensemble des machines considerees 
	# (plus representative). 

          $moyenne = 0;
	        $NbTotalMachines = 0;
          foreach ($label as $key => $value)
          {
		              $NbMachinesDeLaSalle = round($this->getNbMachine($value,$date),2);
                  $label[$key] = $value." \n [".$NbMachinesDeLaSalle." terminaux]";
                  
		              $MoyenneParMachinePourLaSalle = $graphData[$key];
		              $moyenne += $MoyenneParMachinePourLaSalle * $NbMachinesDeLaSalle;
		              $NbTotalMachines += $NbMachinesDeLaSalle;
          }
          $moyenne /= $NbTotalMachines;

          /* Create and populate the pData object */
          $myData = new \pData();
          $myData->addPoints($graphData, 'Nombre d\'heures d\'utilisation moyen par terminal sur une année');

          $myData->setAxisName(0, 'Nombre d\'heures');
          $myData->addPoints($label, 'Labels');

          $myData->setAbscissa('Labels');

          $serieSettings = array("R"=>255,"G"=>0,"B"=>0,"Alpha"=>80);
          $myData->setPalette('Nombre d\'heures d\'utilisation', $serieSettings);

          /* Create the pChart object */
          $myPicture = new \pImage($this->_largeur, 370, $myData);

          /* Draw the background */
          $settings = array('R' => 170, 'G' => 183, 'B' => 87, 'Dash' => 1, 'DashR' => 190, 'DashG' => 203, 'DashB' => 107);
          $myPicture->drawFilledRectangle(0, 0, $this->_largeur, 370, $settings);

          /* Overlay with a gradient */
          $settings = array('StartR' => 219, 'StartG' => 231, 'StartB' => 139, 'EndR' => 1, 'EndG' => 138, 'EndB' => 68, 'Alpha' => 50);
          $myPicture->drawGradientArea(0, 0, $this->_largeur, 370, DIRECTION_VERTICAL, $settings);
          $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

          /* Add a border to the picture */
          $myPicture->drawRectangle(0, 0, $this->_largeur-1, 369, array('R' => 0, 'G' => 0, 'B' => 0));

          /* Write the picture title */

          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
          $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));

          /* Write the chart title */
  	# Ici on choisit la fonte pour les abscisses et les ordonnées qu'on va tracer avec drawScale
          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 9));

          /* Draw the scale and the 1st chart */
          $myPicture->setGraphArea(150, 60, $this->_largeur-20, 190);
          $myPicture->drawFilledRectangle(150, 60, $this->_largeur-20, 190, array('R' => 255, 'G' => 255, 'B' => 255, 'Surrounding' => -200, 'Alpha' => 10));

          $myPicture->drawScale(array('DrawSubTicks' => true, 'LabelRotation' => 45, 'Mode' => SCALE_MODE_START0));

          $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));
          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/calibri.ttf', 'FontSize' => 11));
          $myPicture->drawBarChart(array('DisplayValues' => true, 'Rounded' => true, 'Surrounding' => 30));
          $myPicture->setShadow(false);


          /* Write the chart legend */
          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
          $myPicture->drawLegend(480, 350, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL));

          # La moyenne...
          $myPicture->drawThreshold($moyenne, array('WriteCaption' => true, 'Caption' => "Moyenne : ".round($moyenne,2), 'BoxAlpha' => 40, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));

          /* Render the picture (choose the best way) */
          $myPicture->autoOutput('pictures/example.drawBarChart.png');
        } else {
          /* Create and populate the pData object */
          $myData = new \pData();

          /* Create the pChart object */
          $myPicture = new \pImage($this->_largeur, 60, $myData);

          /* Write the picture title */

          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 14));
          $myPicture->drawText(10, 55, "Impossible de générer le graphe : Nombre d'heures d'utilisation moyen par terminaux sur une année.\nUne des salles sélectionnées possède une période sans terminaux installés", array('R' => 231, 'G' => 76, 'B' => 60));

          $myPicture->autoOutput('pictures/example.drawBarChart.png');
        }

    }
  


    /**
     * Exporte les données du graphe
     *
     * @return le tableau des données du graphe
     */
    public function export()
    {
        if($this->faisable()){
            $graphData = $this->fetchData(); 
            $debut     = new \DateTime($this->_year.'-'.$this->_month.'-01');
            $label     = $this->getXAxis();
            $i         = 0;
	    $export    = array();
            foreach ($label as $key => $value)
            {
                $export[$i]["salle"] = $value;
                $export[$i]["nombre_terminaux"] = round($this->getNbMachine($value,$debut),2);
                $export[$i]["heures"] =  $graphData[$i];
                $i++;
            }
        }
        else {
            $export["erreur"]="Impossible de générer le graphe : Nombre d'heures d'utilisation moyen par terminaux sur une année.Une des salles sélectionnées possède une période sans terminaux installés";
        }
        return $export;
    }
}

/**
 * Classe décrivant les intervalles de temps à comparer
 *
 */
class IntervalleCPU
{
    var $creneau;

    var $deboufin;

    /**
     * Constructeur de la classe Intervalle
     *
     * @param string   $creneau    un creneau horaire
     * @param interger $deboufin   0 pour le début d'une connexion, 1 pour la fin
     */
    function __construct($creneau,$deboufin)
    {
        $this->creneau    = $creneau;
        $this->deboufin   = $deboufin;
    }
}


