<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
  /**
   * Ce fichier fait partie du projet QoQ-CoT
   *
   * @category Administration
   * @package  QoQ-CoT
   * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
   */

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
* Ce script traite la représentation des graphes de type occupation des salles
*
* Plus précisément, elle est utilisée pour tracer le graphe
* "Utilisation des salles en fonction d'un critère seuil" (le 5e dans l'interface).
*
* Il s'agit d'un graphe indiquant, pour chaque salle sélectionnée, si la salle était occupée
* ou libre, sur tous les créneaux horaires de la fenêtre temporelle sélectionnée.
* Ce caractère occupé ou libre est calculé en fonction de la valeur choisie pour le critère sélectionné. Si la
* salle est « moins » occupée que la valeur choisie, le créneau sera vert, sinon il sera rouge.
* Les critères disponibles sont : nombre d'ordinateurs différents utilisés, ou nombre
* de personnes différentes à s'être connectées, pourcentage de machines utilisées,
* nombre de connexions simultanées et taux d'utilisation de la salle.
* Ainsi le graphe permet d'un coup d’œil de voir a posteriori si la salle aurait pu être utilisée
* pour un cours, ou non : c'est un autre point de vue sur le taux d'occupation, orienté « planning ».
* IDGraphe : 5
* @category Administration
* @package  QoQ-CoT
* @author   Frédéric Bloise <Frederic.Bloise@univ-amu.fr>
* @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
*/


function heuresVersMinutes($heure) {
	$tab=explode(':',$heure);
	return $tab[0]*60+$tab[1];
}

function minutesVersHeures($minutes) {
	return sprintf("%02d:%02d",($minutes/60),($minutes%60));
}

// Retourne le nombre de colonnes maximales du graphe en fonction du nombre de jours et de créneaux
function maxColonnes($date,$datefin,$nombreJoursOuvres,$heureDebut,$heureFin,$pas) {
	$date1=date_create($date);
	$date2=date_create($datefin);
	$diff=date_diff($date1,$date2);
	$maxJours=(($diff->format("%d")/7)+1)*$nombreJoursOuvres;
	$maxCreneaux=(heuresVersMinutes($heureFin)-heuresVersMinutes($heureDebut)/$pas)+1;
	return $maxJours*$maxCreneaux;
}

// Fonction de comparaison pour ordonner des intervalles 
function cmp($a,$b) {
	 if ($a->creneau == $b->creneau) {
		if ($a->deboufin == $b->deboufin) {
        	return 0;
    	}
		return ($a->deboufin < $b->deboufin) ? -1 : 1; 
	}
    return ($a->creneau < $b->creneau) ? -1 : 1;
}

// Retourne le nombre de connexions simultanées
function calcul_max($intervalles) {
	$machines=Array();
	usort($intervalles,"cmp");
	$max=0;
	$ouverture=0;
	foreach ($intervalles as $intervalle) {
		if ($intervalle->deboufin==0) {
			if (! isset($machines[$intervalle->nomMachine]))
				$machines[$intervalle->nomMachine]=1;
			else
				$machines[$intervalle->nomMachine]++;
			if ($machines[$intervalle->nomMachine]<2)
				$ouverture++;
		}
		else {
			if ($machines[$intervalle->nomMachine]<2)
				$ouverture--;
			$machines[$intervalle->nomMachine]--;
		}
		if ($ouverture>$max) $max++;
	}
	return $max;
}

/* Classe intervalle
	creneau=heure de connexion (fin ou debut)
	deboufin=0 si debut de connexion, 1 sinon
*/
class Intervalle {
	var $creneau;
	var $deboufin;
	var $nomMachine;
	function __construct($creneau,$deboufin,$nomMachine)
	{
		$this->creneau=$creneau;
		$this->deboufin=$deboufin;
		$this->nomMachine=$nomMachine;
	}
}

// Initialisation

$pdo = \Dao::getInstance();

$tableConnexions = \Dao::getTableConnexions(new \DateTime($date));

$arraySallesCreneaux=array();
$nombreJoursOuvres=count($arrayJoursOuvres);
$mc=maxColonnes($date,$datefin,$nombreJoursOuvres,$heureDebut,$heureFin,$pas);
$nombreSalles=0;

$salles=unserialize($salles);
if ($composante != '') {
	$sqlSalleOuComposante = 'a.Composante="'.$composante.'" ';
} else {
	$clauseIn = implode(",", $salles);
	$sqlSalleOuComposante = 'a.IdSalle IN ('.$clauseIn.') ';
}

$sql =	'SELECT DISTINCT NomSalle '.
	'FROM Salles AS a, MachinesToSalles AS b '.
	'WHERE a.idSalle=b.RefSalle '.
	'AND '.$sqlSalleOuComposante.
	'AND NOT(b.Date_FIN<"'.$date.'" OR b.date_DEBUT>"'.$datefin.'") ';

$query = $pdo->prepare($sql);
$query->execute();
$results = $query->fetchAll(\PDO::FETCH_OBJ);
foreach ($results as $result) {
	$arraySallesCreneaux[$result->NomSalle]=array();
	$nombreSalles++;
	for ($i=0;$i<$mc;$i++) 
		$arraySallesCreneaux[$result->NomSalle][$i]='';
}

$arraySommes=array();
for ($i=0;$i<$mc;$i++) 
	$arraySommes[$i]=$nombreSalles;

$arrayIntervallesJours=array();
for ($i=0;$i<$nombreJoursOuvres-1;$i++) {
	$arrayIntervallesJours[$i]=$arrayJoursOuvres[$i+1]-$arrayJoursOuvres[$i];	
}
$arrayIntervallesJours[$i]=7-$arrayJoursOuvres[$i]+$arrayJoursOuvres[0];

$premierJour=date("w", strtotime($date));
$jourAjoutes=0;
while (($indexJoursOuvres=array_search($premierJour,$arrayJoursOuvres)) === false) {
	$premierJour=($premierJour+1)%7;
	$jourAjoutes++;
}
$jour=date('Y-m-d', strtotime($date . ' +'.$jourAjoutes.' day'));

$colonne=0;
$coljour=0;
$abscisseCreneaux='';
$abscisseJours='';
$colspan=(heuresVersMinutes($heureFin)-heuresVersMinutes($heureDebut))/$pas;
$bg_libre=array ('#bada6a','#badaaa');
$bg_occupe=array ('#e31f0c','#e31f5c');
$maxSeuil=1;

// Construction du graphe
while ($jour<=$datefin) {
	$arrayNombreMachinesSalles=array();
	$sql =	'SELECT NomSalle,COUNT(*) as total '.
		'FROM Salles AS a, MachinesToSalles AS b '.
		'WHERE a.idSalle=b.RefSalle '.
		'AND '.$sqlSalleOuComposante.
		'AND b.Date_FIN>="'.$jour.'" AND b.date_DEBUT<="'.$jour.'" '.
		'GROUP BY NomSalle';
	$query = $pdo->prepare($sql);
	$query->execute();
	$results = $query->fetchAll(\PDO::FETCH_OBJ);
	foreach ($results as $result) {
		$arrayNombreMachinesSalles[$result->NomSalle]= $result->total;
		if ($maxSeuil<$result->total) $maxSeuil=$result->total;
	}
	$creneauDebutMinutes=heuresVersMinutes($heureDebut);
	$creneauFinMinutes=heuresVersMinutes($heureFin);
	while ($creneauDebutMinutes<$creneauFinMinutes) {
		$creneauDebut=minutesVersHeures($creneauDebutMinutes);
		$creneauFin=minutesVersHeures($creneauDebutMinutes+$pas);
		switch ($critereSeuil) {
			case 'NomMachine' :
			case 'PourcentageMachines' :
			case 'Login' :
				$Critere=$critereSeuil;
				if ($critereSeuil=='PourcentageMachines') $Critere='NomMachine';
				$sql =	'SELECT a.NomSalle,UCASE(b.'.$Critere.') AS Critere,'.
					'TIME_TO_SEC(TIMEDIFF(if(b.HeureFin>"'.$creneauFin.'","'.$creneauFin.'",b.HeureFin),if(b.HeureDebut<"'.$creneauDebut.'","'.$creneauDebut.'",b.HeureDebut))) AS Duree '.
					'FROM Salles as a, '.$tableConnexions.' AS b , MachinesToSalles AS c '.
					'WHERE b.NomMachine=c.NomMachine '.
					'AND jour="'.$jour.'" '.
        	                        'AND NOT (b.HeureDebut>"'.$creneauFin.'" OR b.HeureFin<"'.$creneauDebut.'") '.
        	                        'AND NOT (b.dateDEBUT>c.Date_Fin OR b.DateFin<c.Date_Debut) '.
					'AND a.IdSalle=c.RefSalle '.
					'AND '.$sqlSalleOuComposante.
					'ORDER BY a.NomSalle,b.'.$Critere.'';
				$query = $pdo->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(\PDO::FETCH_OBJ);
				$NomSalle='';
				foreach ($results as $result) {
					if ($NomSalle!=$result->NomSalle) {
						if ($NomSalle!='') {
							if (($critereSeuil=='PourcentageMachines' && round(($nombreCriteres/$arrayNombreMachinesSalles[$NomSalle])*100,0)>=$valeurSeuil) || $nombreCriteres>=$valeurSeuil) {
								$style='style="background-color:'.$bg_occupe[$coljour%2].'"'; 
								$arraySommes[$colonne]--;
							} 
							else $style='style="background-color:'.$bg_libre[$coljour%2].'"';
							$pourcents=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
							$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres<br/>$pourcents%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
						}
						$NomSalle=$result->NomSalle;
						$Duree=0;
						$nombreCriteres=1;
					} else {
						if ($Critere!=$result->Critere)
							$nombreCriteres++;
					}
					$Critere=$result->Critere;
					$Duree+=$result->Duree;
				}
				if ($NomSalle!='') {
					if (($critereSeuil=='PourcentageMachines' && round(($nombreCriteres/$arrayNombreMachinesSalles[$NomSalle])*100,0)>=$valeurSeuil) || $nombreCriteres>=$valeurSeuil) {
						$style='style="background-color:'.$bg_occupe[$coljour%2].'"';
						$arraySommes[$colonne]--;
					} 
					else $style='style="background-color:'.$bg_libre[$coljour%2].'"'; 
					$pourcents=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
					$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres<br/>$pourcents%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
				}
				break;
			case 'Connexions' :
				$sql =	'SELECT a.NomSalle,b.HeureDebut,b.HeureFin,UCASE(b.NomMachine) AS NomMachine, '.
					'TIME_TO_SEC(TIMEDIFF(if(b.HeureFin>"'.$creneauFin.'","'.$creneauFin.'",b.HeureFin),if(b.HeureDebut<"'.$creneauDebut.'","'.$creneauDebut.'",b.HeureDebut))) AS Duree '.
					'FROM Salles as a, '.$tableConnexions.' AS b , MachinesToSalles AS c '.
					'WHERE b.NomMachine=c.NomMachine '.
					'AND jour="'.$jour.'" '.
        	                        'AND NOT (b.HeureDebut>"'.$creneauFin.'" OR b.HeureFin<"'.$creneauDebut.'") '.
        	                        'AND NOT (b.dateDEBUT>c.Date_Fin OR b.DateFin<c.Date_Debut) '.
					'AND a.IdSalle=c.RefSalle '.
					'AND '.$sqlSalleOuComposante.
					'ORDER BY a.NomSalle';
				$query = $pdo->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(\PDO::FETCH_OBJ);
				$NomSalle='';
				$intervalles=Array ();
				$i=0;
				foreach ($results as $result) {
					if ($NomSalle!=$result->NomSalle) {
						if ($NomSalle!='') {
							$nombreCriteres=calcul_max($intervalles);
							if ($nombreCriteres>=$valeurSeuil) {
								$style='style="background-color:'.$bg_occupe[$coljour%2].'"'; 
								$arraySommes[$colonne]--;
							} 
							else $style='style="background-color:'.$bg_libre[$coljour%2].'"';
							$pourcents=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
							$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres<br/>$pourcents%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
						}
						$NomSalle=$result->NomSalle;
						$Duree=0;
						$intervalles=Array ();
						$i=0;
					} 
					$Duree+=$result->Duree;
					$intervalles[$i++]=new Intervalle($result->HeureDebut,0,$result->NomMachine);
					$intervalles[$i++]=new Intervalle($result->HeureFin,1,$result->NomMachine);
				}
				if ($NomSalle!='') {
					$nombreCriteres=calcul_max($intervalles);
					if ($nombreCriteres>=$valeurSeuil) {
						$style='style="background-color:'.$bg_occupe[$coljour%2].'"';
						$arraySommes[$colonne]--;
					} 
					else $style='style="background-color:'.$bg_libre[$coljour%2].'"';
					$pourcents=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
					$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres<br/>$pourcents%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
				}
				break;
			case 'Taux' :
				$sql =	'SELECT a.NomSalle, '.
					'TIME_TO_SEC(TIMEDIFF(if(b.HeureFin>"'.$creneauFin.'","'.$creneauFin.'",b.HeureFin),if(b.HeureDebut<"'.$creneauDebut.'","'.$creneauDebut.'",b.HeureDebut))) AS Duree '.
					'FROM Salles as a, '.$tableConnexions.' AS b , MachinesToSalles AS c '.
					'WHERE b.NomMachine=c.NomMachine '.
					'AND jour="'.$jour.'" '.
        	                        'AND NOT (b.HeureDebut>"'.$creneauFin.'" OR b.HeureFin<"'.$creneauDebut.'") '.
        	                        'AND NOT (b.dateDEBUT>c.Date_Fin OR b.DateFin<c.Date_Debut) '.
					'AND a.IdSalle=c.RefSalle '.
					'AND '.$sqlSalleOuComposante.
					'ORDER BY a.NomSalle';
				$query = $pdo->prepare($sql);
				$query->execute();
				$results = $query->fetchAll(\PDO::FETCH_OBJ);
				$NomSalle='';
				$intervalles=Array ();
				$i=0;
				foreach ($results as $result) {
					if ($NomSalle!=$result->NomSalle) {
						if ($NomSalle!='') {
							$nombreCriteres=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
							if ($nombreCriteres>=$valeurSeuil) {
								$style='style="background-color:'.$bg_occupe[$coljour%2].'"'; 
								$arraySommes[$colonne]--;
							} 
							else $style='style="background-color:'.$bg_libre[$coljour%2].'"';
							$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
						}
						$NomSalle=$result->NomSalle;
						$Duree=0;
					} 
					$Duree+=$result->Duree;
				}
				if ($NomSalle!='') {
					$nombreCriteres=round(($Duree/($arrayNombreMachinesSalles[$NomSalle]*$pas*60))*100,0);
					if ($nombreCriteres>=$valeurSeuil) {
						$style='style="background-color:'.$bg_occupe[$coljour%2].'"';
						$arraySommes[$colonne]--;
					} 
					else $style='style="background-color:'.$bg_libre[$coljour%2].'"';
					$arraySallesCreneaux[$NomSalle][$colonne]="<td $style>$nombreCriteres%<br/>".$arrayNombreMachinesSalles[$NomSalle]."</td>";
				}
				break;
		}
		$creneauDebutMinutes+=$pas;
		$colonne++;
		$abscisseCreneaux.='<td>'.$creneauDebut.'</td>';
	}
	$abscisseJours.='<td colspan='.$colspan.'>'.$arrayJours[$arrayJoursOuvres[$indexJoursOuvres]].' '.$jour.'</td>';
	$jour=date('Y-m-d', strtotime($jour . '+'.$arrayIntervallesJours[$indexJoursOuvres] .' day'));
	$indexJoursOuvres=($indexJoursOuvres+1)%$nombreJoursOuvres;
	$coljour++;
}

if ($critereSeuil=='PourcentageMachines' || $critereSeuil=='Taux') $maxSeuil=100;
$maxSeuil++;
$image.='Seuil :<input type="range" name="ajustInputName" id="ajustInputId" value="'.$valeurSeuil.'" min="1" max="'.$maxSeuil.'" oninput="ajustOutputId.value = ajustInputId.value" onchange="nouveauSeuil(this.value,'.$colspan.',\''.$critereSeuil.'\')">';
$image.='<output name="ajustOutputName" id="ajustOutputId">'.$valeurSeuil.'</output>';
$image.='<br/><br/>';

$image.='<table id="tableau">';
$image.='<tr><td style="border:0px"></td>'.$abscisseJours.'</tr>';
$image.='<tr><td style="border:0px"></td>'.$abscisseCreneaux.'</tr>';
$image.='<tr><td>'.$nombreSalles.'</td>';
for ($i=0;$i<$colonne;$i++)
	$image.='<td>'.$arraySommes[$i].'</td>';
$image.='</tr>';

foreach ($arraySallesCreneaux as $key => $value){
	$image.='<tr><td>'.$key.'</td>';
	for ($i=0;$i<$colonne;$i++)
		if ($value[$i]=='') $image.='<td style="background-color:'.$bg_libre[($i/$colspan)%2].'"></td>';
		else $image.=$value[$i];
}

$image.='<tr><td style="border:0px"></td>'.$abscisseCreneaux.'</tr>';
$image.='<tr><td style="border:0px"></td>'.$abscisseJours.'</tr>';

$image.='</table>';

