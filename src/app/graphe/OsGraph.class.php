<?php
/**
*
* ********************************* ENGLISH *********************************
* 
* --- Copyright notice :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Statement of copying permission
* 
* This file is part of QoQ-CoT.
* 
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
* 
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
* 
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
* 
* Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
* 
* 
* --- Déclaration de permission de copie
* 
* Ce fichier fait partie de QoQ-CoT.
* 
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
* 
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
* 
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
* 
*/
  /**
   * Ce fichier fait partie du projet QoQ-CoT
   *
   * @category Administration
   * @package  QoQ-CoT
   * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
   * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
   */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Os
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Répartition des connexions par système d'exploitation" 
 * (le 4e dans l'interface).
 * Il s'agit d'un graphe de type secteurs (nom scientifique camembert)
 * qui indique la répartition en pourcentage du nombre de connexions
 * par OS dans la salle -- ou le groupe de salles -- pendant la période
 * sélectionnée.
 * IDGraphe : 4
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
class OsGraph
{
    private $_date;

    private $_datefin;

    private $_salles;

    private $_composante;

    private $_titre;


    /**
     * Constructeur
     *
     * @param string  $date        la date de début de la période observée
     * @param string  $datefin     la date de fin de la période observée
     * @param string  $salles     un array serialize de salles
     * @param string  $composante le nom de la composante
     */
    public function __construct($date, $datefin, $salles, $composante)
    {
        $this->_date       = $date;
        $this->_datefin    = $datefin;
        $this->_salles     = unserialize($salles);
        $this->_composante = $composante;
        $this->_titre      = $this->initTitre();
    }



    /**
     * Initialise le titre du graphe
     *
     *  @return string $titre le titre du graphe
     */
    public function initTitre()
    {
        $pdo = \Dao::getInstance();


        if ($this->_composante != '') {

            $titre = 'Répartition des connexions pour le groupe de salles '.$this->_composante.' du '.date('d/m/Y', strtotime($this->_date)).' au '.date('d/m/Y', strtotime($this->_datefin));

        } else if (is_array($this->_salles) && $this->_salles[0] != '') {

            $clauseIn = implode(",", $this->_salles);

            $sql = 'SELECT NomSalle FROM Salles '.
                   'WHERE IdSalle IN ('.$clauseIn.');';
            

            $query = $pdo->prepare($sql);

            $query->execute();

            $results = $query->fetchAll();

            $count = $query->rowCount();

            if ($count > 1) {

                $salles = '[ ';

                foreach ($results as $salle) {

                    $salles .= $salle['NomSalle'].' ';
                }

                $salles .= ']';

                $titre = 'Répartition des connexions pour le groupe de salles '.$salles.' du '.date('d/m/Y', strtotime($this->_date)).' au '.date('d/m/Y', strtotime($this->_datefin));

            } else {

                $titre = 'Répartition des connexions pour la salle '.$results[0]['NomSalle'].' du '.date('d/m/Y', strtotime($this->_date)).' au '.date('d/m/Y', strtotime($this->_datefin));
            }
        } else {

            $titre = 'Répartition des connexions pour toutes les salles';
        }

        return $titre;
    }


    /**
     * Retourne la date de début de période observée
     *
     * @return string $_date la date de début
     */
    function getDateDebut()
    {
        return $this->_date.' 00:00:00';
    }


    /**
     * Retourne la date de fin de période observée
     *
     * @return string $_datefin la date de fin
     */
    function getDateFin()
    {
        return $this->_datefin.' 23:59:59';
    }


    /**
     * Récupère les données
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $dataArray = array();

        $pdo = \Dao::getInstance();

        $tableConnexions = \Dao::getTableConnexions(new \DateTime($this->_date));

        $sql = 'SELECT COUNT(*) as c, NomOs FROM '.$tableConnexions.' AS c ';

        if ((is_array($this->_salles) && $this->_salles[0] != '') || $this->_composante != '') {

            $sql .= 'INNER JOIN MachinesToSalles AS ms '.
                    'ON c.NomMachine = ms.NomMachine '.
                    'INNER JOIN Salles AS s ON ms.RefSalle = s.IdSalle ';
                    
             }

        $sql .= 'WHERE NOT (DateFin < :dateDebut OR Datedebut > :dateFin) ';
        $sql .= 'AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN) ';

        if ($this->_composante != '') {

            $sql .= 'AND s.Composante = :composante ';

        } else if (is_array($this->_salles) && $this->_salles[0] != '') {

            $clauseIn = implode(",", $this->_salles);

            $sql .= 'AND s.IdSalle IN  ('.$clauseIn.') ';
        }

        $sql .= 'GROUP BY NomOs';

        $query = $pdo->prepare($sql);

        if ($this->_composante != '')
            $query->bindParam(':composante', $this->_composante, \PDO::PARAM_STR);

        $dateDebut = $this->getDateDebut();
        $dateFin   = $this->getDateFin();

        $query->bindParam(':dateDebut', $dateDebut, \PDO::PARAM_STR);
        $query->bindParam(':dateFin', $dateFin, \PDO::PARAM_STR);

        $query->execute();

        $results = $query->fetchAll(\PDO::FETCH_OBJ);

        if (empty($results)) {

            $dataArray['c'] = 0;
            $dataArray['nomos'] = 'Pas de données suffisantes pour cette période !!!';

        } else {

            foreach ($results as $result) {

                $dataArray['c'][] = $result->c;
                $dataArray['nomos'][] = $result->NomOs;
            }
        }

        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $graphData = $this->fetchData();

        /* Create and populate the pData object */
        $myData = new \pData();
        $myData->addPoints($graphData['c'], 'ScoreA');  
        $myData->setSerieDescription('ScoreA', 'Application A');

        /* Define the absissa serie */
        $myData->addPoints($graphData['nomos'], 'Labels');
        $myData->setAbscissa('Labels');

        /* Create the pChart object */
        $myPicture = new \pImage(900, 500, $myData, true);

        $myPicture->drawGradientArea(0, 0, 900, 500, DIRECTION_VERTICAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 100));
        $myPicture->drawGradientArea(0, 0, 900, 500, DIRECTION_HORIZONTAL, array('StartR' => 240, 'StartG' => 240, 'StartB' => 240, 'EndR' => 180, 'EndG' => 180, 'EndB' => 180, 'Alpha' => 20));

        $myPicture->drawGradientArea(0, 0, 900, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));


        /* Write the picture title */ 
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
        $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));


        /* Set the default font properties */ 
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Forgotte.ttf', 'FontSize' => 12,'R' => 80, 'G' => 80, 'B' => 80));

        /* Create the pPie object */ 
        $pieChart = new \pPie($myPicture, $myData);

        /* Enable shadow computing */ 
        $myPicture->setShadow(true, array('X' => 3, 'Y' => 3, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));

        /* Draw a splitted pie chart */ 
        $pieChart->draw3DPie(450, 250, array('WriteValues' => true, 'Radius' => 200, 'DataGapAngle' => 0, 'DataGapRadius' => 0, 'Border' => true, 'ValueR' => 0, 'ValueG' => 0, 'ValueB' => 0));


        /* Write the legend box */ 
        $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Forgotte.ttf', 'FontSize' => 12, 'R' => 0, 'G' => 0, 'B' => 0));
        $pieChart->drawPieLegend(40, 400, array('Style' => LEGEND_NOBORDER, 'Mode' => LEGEND_HORIZONTAL));

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('pictures/example.draw3DPie.transparent.png');
    }
}
