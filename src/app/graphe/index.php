<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Front Controller
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */

//récupération de la liste des salles pour le formulaire
$pdo = Dao::getInstance();

$where='';
if (isset($_SESSION['site']) && $_SESSION['site']!=='') {
    $where='WHERE Site="'.$_SESSION['site'].'"';
}

$sql = "SELECT * FROM Salles $where ORDER BY Composante,NomSalle ASC";

$query = $pdo->prepare($sql);

$query->execute();
$listeSalles = $query->fetchAll(PDO::FETCH_OBJ);

$sql = "SELECT DISTINCT(composante) FROM Salles $where ORDER BY composante ASC";

$query = $pdo->prepare($sql);

$query->execute();
$listeComposantes = $query->fetchAll(PDO::FETCH_OBJ);

$content    = '';
$image      = '';
$map        = '';
$script     = '';
$defaultPas = 60;
$annee      = 2012;
$largeur    = 900;

$validMenuArray = array('duree', 'cpu', 'humain', 'os', 'seuil');

if (isset($_GET['menu'])) {

    if (in_array($_GET['menu'], $validMenuArray)) {

        $menu = $_GET['menu'];

        //affichage du formulaire de recherche selon l'item du menu
        switch ($menu) {

	case 'duree':

		//définition des variables pour l'image
		$date             = date('Y-m-d');
		$datefin          = '';
		$datepicker       = date('d/m/Y');
		$datepickerfin    = '';
		$salle            = '0';
		$pas              = '';
		$heureDebut       = HEURE_DEBUT;
		$heureFin         = HEURE_FIN;
		$joursOuvresArray = JOURS_OUVRES;

		if (isset($_GET['rechercher']))
		{

			if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepicker'], $arrayDate))
			    $datepicker = $arrayDate[0];

			if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepickerfin'], $arrayDate))
			    $datepickerfin = $arrayDate[0];

			if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['date'], $arrayDate))
			    $date = $arrayDate[0];

			if ($datepickerfin !== '')
			    if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['datefin'], $arrayDate))
			        $datefin = $arrayDate[0];
			    else
			        $datefin = '';

			$salle            = intval($_GET['salle']);
			$pas              = intval($_GET['pas']);
			$largeur          = intval($_GET['largeur']);
			$heureDebut       = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
			$heureFin         = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
			$joursOuvresArray = serialize($_GET['joursouvres']);

			if ($datefin === '')
			{
				if ($heureDebut != '' and $heureFin != '')
				{
			        	if ($heureDebut < $heureFin)
					{

					######### Si on veut forcer la fenetre à être un multiple du pas #######
					#	$pas_en_secondes = $pas * 60;
					#	$nb_pas_dans_fenetre = (strtotime($heureFin)-strtotime($heureDebut))/$pas_en_secondes;
					#	$fenetre_est_un_multiple_du_pas = is_int($nb_pas_dans_fenetre);
					#	if ( $fenetre_est_un_multiple_du_pas )
					#	{

						$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;date='.urlencode($date).'&amp;salle='.urlencode($salle).'&amp;pas='.urlencode($pas).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;type=duree" alt="'.$menu.'" usemap="#mapduree" />';

						if ($_SESSION['role'] === 'ROLE_ADMIN')
						{

							require_once 'DureeGraph.class.php';
							$dureeMap = new  Dosicalu\QoQCoT\App\Graphe\DureeGraph($date, $pas, $salle, $largeur, $heureDebut, $heureFin, 0);

							$map = $dureeMap->display();
						}

					######### Suite si on veut forcer la fenêtre à être un multiple du pas
					#	}
					#	else
					#	{
					#			$content .= 'La durée totale de la fenêtre d\'observation doit être un multiple du pas : par exemple 8h-18h, ou 8h09-17h09 avec un pas de 60, 30, 20, 15, 10 ou 5\' ; ou encore 8h16-17h46 avec un pas de 5, 10, 15 ou 30\' (mais pas 20 ou 60). Par contre impossible (avec les pas prédéfinis proposés) pour 8h02-16h05 ou 9h16-13h47...';
					#	}
					######### FIN Si on veut forcer la fenetre à être un multiple du pas #######

			        	}
					else
					{
						$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
			        	}
				}
				else
				{
					$content .= '<span class="erreur">Vous devez fixer l\'heure de début ET l\'heure de fin de la fenêtre d\'observation</span>';
				}
			}
			else
			{
				if ($date <= $datefin)
				{
					if ($heureDebut != '' and $heureFin != '')
					{
				        	if ($heureDebut < $heureFin)
						{
							$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;date='.urlencode($date).'&amp;datefin='.urlencode($datefin).'&amp;salle='.urlencode($salle).'&amp;pas='.urlencode($pas).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;joursouvres='.urlencode($joursOuvresArray).'&amp;type=cumulduree" alt="'.$menu.'" />';
						}
						else
						{
							$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
				        	}
					}
					else
					{
						$content .= '<span class="erreur">Vous devez fixer l\'heure de début ET l\'heure de fin de la fenêtre d\'observation</span>';
					}

				}
				else
				{

					$content .= '<span class="erreur">La date de début doit être antérieure à la date de fin de période à visualiser</span>';
				}
			}
		}

		require_once 'view/DureeView.class.php';

		break;

        case 'cpu':


            //définition des variables
            $mois             = '01';
            $composante       = '';
            $arrayMonth       = array('01' => 'Janvier', '02' => 'Février', '03' => 'Mars', '04' => 'Avril', '05' => 'Mai', '06' => 'Juin', '07' => 'Juillet', '08' => 'Août', '09' => 'Septembre', '10' => 'Octobre', '11' => 'Novembre', '12' => 'Décembre');
            $arrayMoisValid   = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
            $year             = date('Y');
            $heureDebut       = HEURE_DEBUT;
            $heureFin         = HEURE_FIN;
            $joursOuvresArray = JOURS_OUVRES;
            $excluDebut       = '';
            $excluFin         = '';

            if (isset($_GET['rechercher'])) {

                if (in_array($_GET['mois'], $arrayMoisValid))
                    $mois = $_GET['mois'];

                if (isset($_GET['salles'])) {
                    $salles = serialize($_GET['salles']);

                } else {

                    $salles = '';
                }



                $year = $_GET['annee'];
                $largeur = intval($_GET['largeur']);

                $composante       = htmlspecialchars($_GET['composante'], ENT_QUOTES);
                $heureDebut       = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
                $heureFin         = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
                $joursOuvresArray = serialize($_GET['joursouvres']);
                $excluDebut       = htmlspecialchars($_GET['excludebut'], ENT_QUOTES);
                $excluFin         = htmlspecialchars($_GET['exclufin'], ENT_QUOTES);

		if ($salles == '' && $composante == '')
		{
			$content .= '<span class="erreur"> Veuillez choisir une salle ou un groupe de salles</span>';
		}
		else if ($heureDebut == '' or $heureFin == '')
		{
			$content .= '<span class="erreur"> La fenêtre d\'observation doit avoir une heure de début ET une heure de fin </span>';
		}
		else if ($heureDebut >= $heureFin)
		{
			$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
		}
		else if (($excluDebut != '' && $excluFin == '') || ($excluFin !='' && $excluDebut == ''))
		{
			$content .= '<span class="erreur">La plage d\'exclusion doit avoir un heure de début ET une heure de fin</span>';
		}
		else  if ($excluDebut != '' and $excluFin != '' and $excluDebut >= $excluFin)
		{
			$content .= '<span class="erreur">L\'heure de début de la plage d\'exclusion doit être antérieure à l\'heure de fin...</span>';
		}
		else if ($excluDebut != '' and $excluFin != '' and ($heureDebut>$excluDebut || $heureFin<$excluFin) )
		{
			$content .= '<span class="erreur">La plage d\'exclusion doit être incluse dans la fenêtre d\'observation</span>';
		}
		else # Enfin le bon cas !!! (comprend le cas où la plage d'exclusion peut être vide : excluDebut == '' and excluFin == '')
		{
			$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;mois='.urlencode($mois).'&amp;composante='.urlencode($composante).'&amp;salles='.urlencode($salles).'&amp;annee='.urlencode($year).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;joursouvres='.urlencode($joursOuvresArray).'&amp;excludebut='.urlencode($excluDebut).'&amp;exclufin='.urlencode($excluFin).'&amp;type=cpu" alt="'.$menu.'" />';
			//on affiche ces 2 graphes si on choisi une composante ou + d'une salle
			if ($composante !== '' || count(unserialize($salles)) > 1 )
			{
				$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;mois='.urlencode($mois).'&amp;composante='.urlencode($composante).'&amp;salles='.urlencode($salles).'&amp;annee='.urlencode($year).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;joursouvres='.urlencode($joursOuvresArray).'&amp;excludebut='.urlencode($excluDebut).'&amp;exclufin='.urlencode($excluFin).'&amp;type=cpudetail" alt="cpudetail" />';

				$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;mois='.urlencode($mois).'&amp;composante='.urlencode($composante).'&amp;salles='.urlencode($salles).'&amp;annee='.urlencode($year).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;joursouvres='.urlencode($joursOuvresArray).'&amp;excludebut='.urlencode($excluDebut).'&amp;exclufin='.urlencode($excluFin).'&amp;type=maxcomposante" alt="maxcomposante" />';
			}
		}
	}

            require_once 'view/CpuView.class.php';
            break;

        case 'humain':

		//définition des variables
		$date          = date('Y-m-d');
		$datepicker    = date('d/m/Y');
		$datefin       = '';
		$datepickerfin = '';
		$heureDebut    = HEURE_DEBUT;
		$heureFin      = HEURE_FIN;
		$composante    = '';
		$pas           = '60';
		$uniqueconn = (isset($_GET['uniqueconn'])) ? 'unique' : 'all';

		if (isset($_GET['rechercher']))
		{
			if (isset($_GET['salles'])) {
				$salles = serialize($_GET['salles']);
			}
			else
			{
				$salles = '';
			}
			if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['date'], $arrayDate))
				$date = $arrayDate[0];

			if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepicker'], $arrayDate))
				$datepicker = $arrayDate[0];

			if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepickerfin'], $arrayDate))
				$datepickerfin = $arrayDate[0];

			if ($datepickerfin !== '')
				if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['datefin'], $arrayDate))
					$datefin = $arrayDate[0];
				else
					$datefin = '';


			$pas        = intval($_GET['pas']);
			$largeur    = intval($_GET['largeur']);
			$heureDebut = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
			$heureFin   = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);


			$composante = htmlspecialchars($_GET['composante'], ENT_QUOTES);

			if ($salles == '' && $composante == '')
			{
			    $content .= '<span class="erreur">Veuillez choisir une salle ou un groupe de salles</span>';
			}
			else
			{
				if ($datefin === '')
				{
					if ($heureDebut != '' and $heureFin != '')
					{
				        	if ($heureDebut < $heureFin)
						{

						######### Si on veut forcer la fenetre à être un multiple du pas #######
						#	$pas_en_secondes = $pas * 60;
						#	$nb_pas_dans_fenetre = (strtotime($heureFin)-strtotime($heureDebut))/$pas_en_secondes;
						#	$fenetre_est_un_multiple_du_pas = is_int($nb_pas_dans_fenetre);
						#	if ( $fenetre_est_un_multiple_du_pas )
						#	{

	                        			$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;date='.urlencode($date).'&amp;salles='.urlencode($salles).'&amp;composante='.urlencode($composante).'&amp;pas='.urlencode($pas).'&amp;largeur='.urlencode($largeur).'&amp;heuredebut='.$heureDebut.'&amp;heurefin='.$heureFin.'&amp;type=humain" alt="'.$menu.'" />';
						######### Suite si on veut forcer la fenêtre à être un multiple du pas
						#	}
						#	else
						#	{
						#		$content .= 'La durée totale de la fenêtre d\'observation doit être un multiple du pas : par exemple 8h-18h, ou 8h09-17h09 avec un pas de 60, 30, 20, 15, 10 ou 5\' ; ou encore 8h16-17h46 avec un pas de 5, 10, 15 ou 30\' (mais pas 20 ou 60). Par contre impossible (avec les pas prédéfinis proposés) pour 8h02-16h05 ou 9h16-13h47...';
						#	}
						######### FIN Si on veut forcer la fenetre à être un multiple du pas #######
				        	}
						else
						{
							$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
				        	}
					}
					else
					{
						$content .= '<span class="erreur">Vous devez fixer l\'heure de début ET l\'heure de fin de la fenêtre d\'observation</span>';
					}
				}
				else
				{
					if ($date <= $datefin)
					{
						if ($heureDebut != '' and $heureFin != '')
						{
					        	if ($heureDebut < $heureFin)
							{
	                                			$image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;date='.urlencode($date).'&amp;datefin='.urlencode($datefin).'&amp;heuredebut='.urlencode($heureDebut).'&amp;heurefin='.urlencode($heureFin).'&amp;salles='.urlencode($salles).'&amp;composante='.urlencode($composante).'&amp;largeur='.urlencode($largeur).'&amp;uniqueconn='.urlencode($uniqueconn).'&amp;type=max" alt="'.$menu.'" />';
					        	}
							else
							{
								$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
					        	}
						}
						else
						{
							$content .= '<span class="erreur">Vous devez fixer l\'heure de début ET l\'heure de fin de la fenêtre d\'observation</span>';
						}

					}
					else
					{

						$content .= '<span class="erreur">La date de début doit être antérieure à la date de fin de période à visualiser.</span>';
					}
				}
			}
		}

		require_once 'view/HumainView.class.php';
		break;

        case 'os':

            //définition des variables
            $anneeAnt         = date('Y') -1;
            $date             = $anneeAnt.date('-m-d');
            $datefin          = date('Y-m-d');

            $datepicker       = date('d/m/').$anneeAnt;
            $datepickerfin    = date('d/m/Y');
            $composante = '';

            if (isset($_GET['rechercher'])) {

                if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepicker'], $arrayDate))
                    $datepicker = $arrayDate[0];

                if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepickerfin'], $arrayDate))
                    $datepickerfin = $arrayDate[0];

                if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['date'], $arrayDate))
                    $date = $arrayDate[0];

                if ($datepickerfin !== '')
                    if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['datefin'], $arrayDate))
                        $datefin = $arrayDate[0];
                    else
                        $datefin = '';

                if (isset($_GET['salles'])) {
                    $salles = serialize($_GET['salles']);

                } else {

                    $salles = '';
                }

                $composante = htmlspecialchars($_GET['composante'], ENT_QUOTES);

                $image .= '<img src="app/graphe/graph.php?menu='.$menu.'&amp;date='.urlencode($date).'&amp;datefin='.urlencode($datefin).'&amp;salles='.urlencode($salles).'&amp;composante='.urlencode($composante).'&amp;type=os" alt="'.$menu.'" id="osimg" />';
            }

            require_once 'view/OsView.class.php';
            break;

        case 'seuil':

            //définition des variables
	    $date             = date('Y-m-d');
	    $datefin          = date('Y-m-d');
	    $datepicker       = date('d/m/Y');
	    $datepickerfin    = date('d/m/Y');
            $composante       = '';
            $arrayMonth       = array('01' => 'Janvier', '02' => 'Février', '03' => 'Mars', '04' => 'Avril', '05' => 'Mai', '06' => 'Juin', '07' => 'Juillet', '08' => 'Août', '09' => 'Septembre', '10' => 'Octobre', '11' => 'Novembre', '12' => 'Décembre');
            $arrayMoisValid   = array('01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12');
            $year             = date('Y');
            $heureDebut       = HEURE_DEBUT;
            $heureFin         = HEURE_FIN;
            $pas              = 60;
            $arrayJoursOuvres = unserialize(JOURS_OUVRES);
            $arrayJours       = array('Dimanche','Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi');
            $arrayCriteresSeuil= array('Login'=>'Nombre de personnes différentes','NomMachine'=>'Nombre de terminaux utilisés','PourcentageMachines'=>'Pourcentage de machines utilisées','Connexions'=>'Nombre de connexions simultanées','Taux'=>'Taux d\'utilisation de la salle');
            $valeurSeuil='';

            if (isset($_GET['rechercher'])) {

		if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepicker'], $arrayDate))
		    $datepicker = $arrayDate[0];

		if (preg_match('#(\d{2})/(\d{2})/(\d{4})#', $_GET['datepickerfin'], $arrayDate))
		    $datepickerfin = $arrayDate[0];

		if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['date'], $arrayDate))
		    $date = $arrayDate[0];

		if ($datepickerfin !== '')
		    if (preg_match('#(\d{4})-(\d{2})-(\d{2})#', $_GET['datefin'], $arrayDate))
		        $datefin = $arrayDate[0];

                if (preg_match('#\d*#', $_GET['valeurSeuil'], $arrayValeurSeuil))
                   $valeurSeuil=$arrayValeurSeuil[0];

                if (isset($_GET['salles'])) 
                    $salles = serialize($_GET['salles']);
                else
                    $salles = '';

                $composante       = htmlspecialchars($_GET['composante'], ENT_QUOTES);
                $heureDebut       = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
                $heureFin         = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
                $pas              = $_GET['pas'];
                $arrayJoursOuvres = $_GET['joursouvres'];
                $critereSeuil     = htmlspecialchars($_GET['critereSeuil'], ENT_QUOTES);

		if ($date == '' or $datefin == '')
		{
			$content .= '<span class="erreur">La fenêtre d\'observation doit avoir une date de début ET une date de fin </span>';
		}
		else if ($date > $datefin)
		{
			$content .= '<span class="erreur">La date de début de la fenêtre d\'observation doit être antérieure à la date de fin '.$datefin.'</span>';
		}
		else if ($salles == '' && $composante == '')
		{
			$content .= '<span class="erreur">Veuillez choisir une salle ou un groupe de salles</span>';
		}
		else if ($heureDebut == '' or $heureFin == '')
		{
			$content .= '<span class="erreur">La fenêtre d\'observation doit avoir une heure de début ET une heure de fin </span>';
		}
		else if ($heureDebut >= $heureFin)
		{
			$content .= '<span class="erreur">L\'heure de début de la fenêtre d\'observation doit être antérieure à l\'heure de fin</span>';
		}
		else if ($arrayJoursOuvres == '')
		{
			$content .= '<span class="erreur">Veuillez sélectionner au moins un jour ouvré</span>';
		}
		else if ($critereSeuil == '')
		{
			$content .= '<span class="erreur">Veuillez choisir un critère seuil</span>';
		}
		else if ($valeurSeuil == '')
		{
			$content .= '<span class="erreur">Veuillez saisir une valeur seuil valide</span>';
		}
		else {
			require_once 'SeuilGraph.php';
                }
            }
            require_once 'view/SeuilView.php';
            break;

        }
    } else {
        $content .= '<span class="erreur">Veuillez saisir un type valide.</span>';
    }
}

require_once 'view/layout.php';
require_once 'layout.php';
