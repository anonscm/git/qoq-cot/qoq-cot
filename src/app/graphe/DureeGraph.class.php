<?php
/**
*
* ********************************* ENGLISH *********************************
*
* --- Copyright notice :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Statement of copying permission
*
* This file is part of QoQ-CoT.
*
* QoQ-CoT is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 3 of the License, or
* (at your option) any later version.
*
* QoQ-CoT is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with QoQ-CoT; if not, write to the Free Software
* Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
* *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
*
* --- Notice de Copyright :
*
* Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
*
*
* --- Déclaration de permission de copie
*
* Ce fichier fait partie de QoQ-CoT.
*
* QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
* selon les termes de la Licence Publique Générale GNU telle qu'elle est
* publiée par la Free Software Foundation ; soit la version 3 de la Licence,
* soit (à votre choix) une quelconque version ultérieure.
*
* QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
* GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
* d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
* pour plus de détails.
*
* Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
* QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
* 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
*
*/
  /**
   * Ce fichier fait partie du projet QoQ-CoT
   *
   * @category Administration
   * @package  QoQ-CoT
   * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
   * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
   */

namespace Dosicalu\QoQCoT\App\Graphe;


require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type Duree
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Utilisation detaillee par machine" (le 1er dans l'interface)
 * dans le cas où seulement la date de début DD est spécifiée : on a alors
 * seulement le jour DD affiché sous forme de créneaux horaires
 * "allumés" ou non selon que la machine correspondante était
 * utilisée ou non.
 * IDGraphe : 1 sans date de fin 
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */

class DureeGraph
{
    protected $date;

    protected $pas;

    protected $salle;

    protected $largeur;

    protected $heureDebut;

    protected $heureFin;

    protected $isGraph;

    private $_titre;

    private $_machines;


    /**
     * Constructeur
     *
     * @param string  $date       la date du graphe
     * @param integer $pas        le pas des échantillons en minute
     * @param string  $salle      la salle observée
     * @param integer $largeur    la largeur du graph
     * @param string  $heureDebut l'heure d'ouverture des salles
     * @param string  $heureFin   l'heure de fermeture des salles
     * @param integer $isGraph    0 = map ; 1 = graphe
     */
    public function __construct($date, $pas, $salle, $largeur, $heureDebut, $heureFin , $isGraph)
    {
        $this->date       = $date;
        $this->pas        = $pas;
        $this->salle      = $salle;
        $this->largeur    = $largeur;
        $this->heureDebut = $heureDebut;
        $this->heureFin   = $heureFin;
        $this->isGraph    = $isGraph;
        $this->_titre     = '';
        $this->_machines  = array();
    }

    /**
     * Transforme une heure au format hh:mm en minutes
     *
     * @return string 
     */
    public function hoursToMinutes($heure) 
    {
       $tab=explode(':',$heure);

       return $tab[0]*60+$tab[1];
    }


    /**
     * Retourne les libélés de l'axe des Y
     *
     * @return array $arrayHeure les libélés de l'axe des Y
     */
    public function getYAxis()
    {
        $pas = $this->pas;

        $debut = $this->hoursToMinutes($this->heureDebut);
        $fin   = $this->hoursToMinutes($this->heureFin);

        $i          = $debut;
        $arrayHeure = array();

        while ($i < $fin) {

            $item = sprintf("%02d:%02d-", $i/60, $i%60);

            $i = $i + $pas;

            $item .= sprintf("%02d:%02d", $i/60, $i%60);

            $arrayHeure[] = $item;
        }

        return $arrayHeure;
    }


    /**
     * Instancie le tableau machines avec les machines présentes dans la période requêtée par l'utilisateur
     *
     */
    public function setMachines()
    {
        $idSalle = $this->salle;
        $arrayMachine = array();

        $pdo = \Dao::getInstance();

        $sql = 'SELECT NomMachine '.
               'FROM MachinesToSalles '.
               'WHERE RefSalle = :idSalle '.
               'AND NOT (Date_FIN < :dateDebut OR date_DEBUT > :dateFin) '.
               'ORDER BY NomMachine ASC';

        $query = $pdo->prepare($sql);
        $query->bindParam(':idSalle', $idSalle, \PDO::PARAM_INT, 2);
        $query->bindParam(":dateDebut", $this->date, \PDO::PARAM_STR);
        $date2 = new \DateTime($this->date);
        $date2 = $date2->add(new \DateInterval('P1D'))->format("Y-m-d");
        $query->bindParam(":dateFin", $date2, \PDO::PARAM_STR);
        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_OBJ);

        foreach ($results as $machine) {

            $this->_machines[] = $machine->NomMachine;
        }
    }

    /**
     * Instancie le titre du graphe
     *
     */
    function setTitre()
    {
        $idSalle = $this->salle;

        $pdo = \Dao::getInstance();

        $sql = 'SELECT NomSalle,Composante '.
               'FROM Salles '.
               'WHERE IdSalle = :idSalle ';

        $query = $pdo->prepare($sql);
        $query->bindParam(':idSalle', $idSalle, \PDO::PARAM_INT, 2);
        $query->execute();
        $results = $query->fetch(\PDO::FETCH_OBJ);

        $this->_titre = 'Utilisation de la salle '.$results->NomSalle.' ('.$results->Composante.') le '.date('d/m/Y', strtotime($this->date));
    }


    /**
     * Récupère les données
     *
     * @return array $dataArray array de données du graphe ou de la map
     */
    public function fetchData()
    {
        $dataArray = array();

        $pdo = \Dao::getInstance();

        $idSalle = $this->salle;
       
        if (count($this->_machines)>0) { 

            $dateDebut = $this->date." ".$this->heureDebut.":00";
            $dateFin = $this->date." ".$this->heureFin.":00";
            $tableConnexions = \Dao::getTableConnexions($dateDebut);

            $sql = 'SELECT ms.NomMachine AS Machine, Login, NomOs, HeureDebut, HeureFin '.
                   'FROM '.$tableConnexions.' AS c,MachinesToSalles AS ms '.
                   'WHERE NOT (DateFin < :dateDebut OR Datedebut > :dateFin) '.
                   'AND NOT (ms.Date_FIN < :dateDebut OR ms.Date_DEBUT > :dateFin) '.
                   'AND ms.RefSalle= :idSalle '.
                   'AND ms.NomMachine=c.NomMachine '.
                   'ORDER BY c.NomMachine,DateDebut ASC';

            $query = $pdo->prepare($sql);

            $query->bindParam(':dateDebut', $dateDebut, \PDO::PARAM_STR);
            $query->bindParam(':dateFin', $dateFin, \PDO::PARAM_STR);
            $query->bindParam(':idSalle', $idSalle, \PDO::PARAM_STR);
            $query->execute();
            $results = $query->fetchAll(\PDO::FETCH_OBJ);

	          $currMachine = '';
            $x = 0;

            $heureDebutSalle=$this->hoursToMinutes($this->heureDebut);
            $heureFinSalle=$this->hoursToMinutes($this->heureFin);
	    
            $map='';
            foreach ($results as $result) {
	    	    if ($result->Machine != $currMachine) {
                    if ($map!='') {
                        $dataArray[]=array($x,$y,$map);
                    }
                    while ($result->Machine != $this->_machines[$x]) {
	    	               $x++;
                    }
	    	            $currMachine=$result->Machine;
                    $currCreneau=$heureDebutSalle;
                    $y=0;
                    $map='';
                }
                $heureDebutConnexion=$this->hoursToMinutes($result->HeureDebut);
                $heureFinConnexion=$this->hoursToMinutes($result->HeureFin);

                $next=0;
                while(! $next) {
                     if ($currCreneau+$this->pas < $heureDebutConnexion) {
                         if ($map!='') {
                             $dataArray[]=array($x,$y,$map);
                             $map='';
                         }
                         $currCreneau+=$this->pas;
                         $y++;
                         $next=0;
                     }
                     else if ($heureFinConnexion >= $currCreneau+$this->pas) {
                         $dataArray[]=array($x,$y,$map.$result->Login.' ('.$result->NomOs.') de '.$result->HeureDebut.' à '.$result->HeureFin.'<br />');
                         $map='';
                         $currCreneau+=$this->pas;
                         $y++;
                         $next=0;
                     }
                     else {
                         $map.=$result->Login.' ('.$result->NomOs.') de '.$result->HeureDebut.' à '.$result->HeureFin.'<br />';
                         $next=1;
                     }
                }

            }
            if ($map!='') {
                $dataArray[]=array($x,$y,$map);
            }
        }

        return $dataArray;
    }

    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $this->setMachines();
        if (count($this->_machines) > 0) {
            $this->setTitre();

            $Data  = $this->fetchData();
            $creneaux   = $this->getYAxis();

     	      $hauteurCreneau = 25;

            if ($this->isGraph) {
            
             $graphData=array();
             foreach ($Data as $key => $val) $graphData[]=array($val[0],$val[1]);

     	       /* Create the pChart object */
             $myData = new \pData();
     	       $myPicture = new \pImage($this->largeur, $hauteurCreneau * count($creneaux) + 320, $myData);

     	       /* Create a solid background */
     	       $settings = array('R' => 179, 'G' => 217, 'B' => 91, 'Dash' => 1, 'DashR' => 199, 'DashG' => 237, 'DashB' => 111);
     	       $myPicture->drawFilledRectangle(0, 0, $this->largeur, $hauteurCreneau * count($creneaux) + 320, $settings);

     	       /* Do a gradient overlay */
     	       $settings = array('StartR' => 194, 'StartG' => 231, 'StartB' => 44, 'EndR' => 43, 'EndG' => 107, 'EndB' => 58, 'Alpha' => 50);
     	       $myPicture->drawGradientArea(0, 0, $this->largeur, $hauteurCreneau * count($creneaux) + 400, DIRECTION_VERTICAL, $settings);
     	       $myPicture->drawGradientArea(0, 0, $this->largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 100));

     	       /* Add a border to the picture */
     	       $myPicture->drawRectangle(0, 0, $this->largeur-1, $hauteurCreneau * count($creneaux) + 319, array('R' => 0, 'G' => 0, 'B' => 0));

     	       /* Write the picture title */
     	       $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 12));
     	       $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));

     	       /* Define the charting area */
     	       $myPicture->setGraphArea(175, 70, $this->largeur-100, $hauteurCreneau * count($creneaux)+70);
     	       $myPicture->drawFilledRectangle(175, 70, $this->largeur-100, $hauteurCreneau * count($creneaux)+70, array('R' => 255, 'G' => 255, 'B' => 255, 'Surrounding' => -200, 'Alpha' => 20));

     	       $myPicture->setShadow(true, array('X' => 1, 'Y' => 1));

     	       /* Create the surface object */
     	       $mySurface = new \pSurface($myPicture);

     	       /* Set the grid size */
     	       $mySurface->setGrid(count($this->_machines)-1, count($creneaux)-1);

     	       /* Write the axis labels */
     	       $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Bedizen.ttf', 'FontSize' => 12));

     	       //Affichage des labels en bas, à 45°
     	       $mySurface->writeXLabels(array('Position' => LABEL_POSITION_BOTTOM, 'Angle' => 45, 'Labels' => $this->_machines));
     	       $mySurface->writeYLabels(array('Labels' => $creneaux));

     	       /* Add values */
     	       foreach ($graphData as $key => $val) {

     	           $mySurface->addPoint($val[0], $val[1], 100);
     	       }

     	       /* Draw the surface chart */
     	       $mySurface->drawSurface(array('Border' => true, 'Surrounding' => 40));

     	       /* Render the picture (choose the best way) */
     	       $myPicture->autoOutput('pictures/example.surface.simple.png');

             }  else {
	
                 $largeurCreneau = ($this->largeur-276) / count($this->_machines);

                 $originx        = 175;
                 $originy        = 70;

                 $map = '<map name="mapduree">';

                 foreach ($Data as $key => $val) {

                     $x1 = $originx + $val[0] * $largeurCreneau;
                     $y1 = $originy + $val[1] * $hauteurCreneau;

                     $x2 = $x1 + $largeurCreneau;
                     $y2 = $y1 + $hauteurCreneau;

                     $map .= '<area shape="rect" coords="'.$x1.','.$y1.','.$x2.','.$y2.'" href="" alt="" title="'.$val[2].'" />';
                 }

                 $map .= '</map>';

                 return $map;
           }
       } else {

          if ($this->isGraph) {
              /* Create and populate the pData object */
              $myData = new \pData();

              /* Create the pChart object */
              $myPicture = new \pImage($this->largeur, 60, $myData);

              /* Write the picture title */
              $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 14));
              $myPicture->drawText(10, 55, "Impossible de générer le graphe : Il n'y a aucune machine dans la salle pour la période choisie", array('R' => 231, 'G' => 76, 'B' => 60));

     	        /* Render the picture (choose the best way) */
     	        $myPicture->autoOutput('pictures/example.surface.simple.png');
         }
       }
    }
}
