<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Ce fichier fait partie du projet QoQ-CoT
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
namespace Dosicalu\QoQCoT\App\Graphe;

require_once dirname(__FILE__).'/../../lib/Dao.class.php';

/**
 * Cette classe traite la représentation des graphes de type CumulDuree
 *
 * Plus précisément, elle est utilisée pour tracer le graphe
 * "Utilisation detaillee par machine" (le 1er dans l'interface)
 * dans le cas où date de début ET date de fin sont spécifiées : on a alors
 * seulement le cumul d'heures d'utilisation par machine pour la
 * période considérée, sous forme d'histogrammes.
 * IDGraphe : 1 avec date de fin 
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */
class CumulDureeGraph
{
    private $_date;

    private $_datefin;

    private $_duree_periode;

    private $_pas;

    private $_salle;

    private $_largeur;

    private $_heureDebut;

    private $_heureFin;

    private $_joursOuvres;

    private $_titre;

    private $_machines;

    private $_presence_partielle;

    private $_palette;
 
    private $_labels;


    /**
     * Constructeur
     *
     * @param string  $date        la date de début de la période observée
     * @param string  $datefin     la date de fin de la période observée
     * @param integer $pas         le pas des échantillons en minute
     * @param string  $salle       la salle observée
     * @param integer $largeur     la largeur du graphe
     * @param string  $heureDebut  l'heure d'ouverture des salles
     * @param string  $joursOuvres array des jours ouvrés serializé
     * @param string  $heureFin    l'heure de fermeture des salles
     */
    public function __construct($date, $datefin, $pas, $salle, $largeur, $heureDebut, $heureFin, $joursOuvres)
    {
        $this->_date        = $date;
        $this->_datefin     = $datefin;
        $this->_pas         = $pas;
        $this->_salle       = $salle;
        $this->_largeur     = $largeur;
        $this->_heureDebut  = $heureDebut;
        $this->_heureFin    = $heureFin;
        $this->_joursOuvres = $joursOuvres;
        $this->_titre       = '';
	    $this->_machines    = array();
	    $this->_presence_partielle = array();
	    $this->_moyenne = 0;
	    $this->_palette = array();
	    $this->_labels = array();
    }


    /**
     * Retourne la date de début de période observée
     *
     * @return string $_date la date de début
     */
    function getDateDebut()
    {
        return $this->_date.' 00:00:00';
    }


    /**
     * Retourne la date de fin de période observée
     *
     * @return string $_datefin la date de fin
     */
    function getDateFin()
    {
        return $this->_datefin.' 23:59:59';
    }


    /**
     * Instancie le tableau machines avec les machines présentes dans la période requêtée par l'utilisateur
     *
     */
    function setMachines()
    {
        $idSalle = $this->_salle;

        $pdo = \Dao::getInstance();

        $sql = 'SELECT DISTINCT NomMachine '.
               'FROM MachinesToSalles AS ms '.
               'INNER JOIN Salles AS s '.
               'ON ms.RefSalle = s.IdSalle '.
               'WHERE IdSalle = :idSalle '.
               'AND NOT (ms.Date_FIN < :dateDebut OR ms.date_DEBUT > :dateFin) '.
               'ORDER BY NomMachine ASC';

        $query = $pdo->prepare($sql);
        $query->bindParam(':idSalle', $idSalle, \PDO::PARAM_INT, 2);
        $query->bindParam(":dateDebut", $this->_date, \PDO::PARAM_STR);
        $query->bindParam(":dateFin", $this->_datefin, \PDO::PARAM_STR);
        $query->execute();
        $results = $query->fetchAll(\PDO::FETCH_OBJ);

        foreach ($results as $machine) {
            $this->_machines[] = $machine->NomMachine;
        }
    }

    /**
     * Instancie le titre du graphe
     *
     */
    function setTitre()
    {
        $idSalle = $this->_salle;

        $pdo = \Dao::getInstance();

        $sql = 'SELECT NomSalle '.
               'FROM Salles '.
               'WHERE IdSalle = :idSalle ';

        $query = $pdo->prepare($sql);
        $query->bindParam(':idSalle', $idSalle, \PDO::PARAM_INT, 2);
        $query->execute();
        $results = $query->fetch(\PDO::FETCH_OBJ);

        $this->_titre = 'Nombre d\'heures d\'utilisation des terminaux de la salle '.$results->NomSalle.' du '.date('d/m/Y', strtotime($this->_date)).' au '.date('d/m/Y', strtotime($this->_datefin)).' entre '.$this->_heureDebut.' et '.$this->_heureFin;
    }


    /**
     * Instancie le tableau _presence_partielle avec la durée en jours de présence des machines dans la période requêtée par l'utilisateur
     *
     */
    function setDureePresenceMachines()
    {
        $idSalle = $this->_salle;

        $pdo = \Dao::getInstance();

        $date1 = new \DateTime($this->_date);
        $date2 = new \DateTime($this->_datefin);
	    // Nombre de jours de la période requêtée par l'utilisateur
        $interval=$date1->diff($date2);
	      $this->_duree_periode=$interval->format("%a")+1;

        foreach ($this->_machines as $machine) {

            // On ajoute 1 à la différence du nombre de jour car il s'agit d'une durée
            $sql = 'SELECT SUM(DATEDIFF(LEAST(:dateFin,Date_Fin),GREATEST(:dateDebut,Date_Debut)))+1 AS NbJours '.
                   'FROM MachinesToSalles '.
                   'WHERE NOT (Date_Fin < :dateDebut OR Date_Debut > :dateFin) '.
                   'AND NomMachine= :nomMachine';

            $query = $pdo->prepare($sql);

            $query->bindParam(':dateDebut', $this->_date, \PDO::PARAM_STR);
            $query->bindParam(':dateFin', $this->_datefin, \PDO::PARAM_STR);
            $query->bindParam(':nomMachine', $machine, \PDO::PARAM_STR);

            $query->execute();
            $results = $query->fetch(\PDO::FETCH_OBJ);

            $this->_presence_partielle[]=$results->NbJours;

        }
    }

    /**
     * Récupère les données, instancie les labels et la palette et calcule la moyenne ponderee
     *
     * @return array $dataArray array de données du graphe
     */
    public function fetchData()
    {
        $dataArray = array();

        $pdo = \Dao::getInstance();

        $i=0;

        $nb_machines_temps_plein = 0;

        $dateDebut = $this->getDateDebut();
        $dateFin   = $this->getDateFin();
        $heureDebut = $this->_heureDebut.':00';
        $heureFin   = $this->_heureFin.':00';

        $tableConnexions = \Dao::getTableConnexions($dateDebut);

        foreach ($this->_machines as $machine) {

            $sql = 'SELECT SUM(TIME_TO_SEC(TIMEDIFF(LEAST(CONCAT_WS(" ", Jour, :heureFin), DateFin), GREATEST(CONCAT_WS(" ", Jour, :heureDebut), DateDebut)))) AS total '.
                   'FROM '.$tableConnexions.' '.
                   'WHERE NOT (DateFin < :dateDebut OR Datedebut > :dateFin) '.
                   'AND NomMachine= :nomMachine AND NOT (HeureFin < :heureDebut OR HeureDebut > :heureFin) ';

            $listeJO = implode(',', unserialize($this->_joursOuvres));

            $sql .= 'AND JourSemaine IN ('.$listeJO.');';

            $query = $pdo->prepare($sql);

            $query->bindParam(':dateDebut', $dateDebut, \PDO::PARAM_STR);
            $query->bindParam(':dateFin', $dateFin, \PDO::PARAM_STR);
            $query->bindParam(':heureDebut', $heureDebut, \PDO::PARAM_STR);
            $query->bindParam(':heureFin', $heureFin, \PDO::PARAM_STR);
            $query->bindParam(':nomMachine', $machine, \PDO::PARAM_STR);

            $query->execute();
            $results = $query->fetch(\PDO::FETCH_OBJ);

            $ratio_presence=($this->_presence_partielle[$i]/$this->_duree_periode);
            $pourcentage=round($ratio_presence*100,2);

	        $nb_heures = $results->total/3600;

            $dataArray[] = round($nb_heures,2);

	        # On pondere la moyenne en fonction du ratio de presence, de façon a ce qu'une presence de 20% influe 4 fois moins qu'une
	        # prensence de 80%
            $this->_moyenne += $nb_heures * $ratio_presence;

	        # On calcule le nombre de machines « equivalent temps plein » (METP) sur la période, c'est-à-dire qu'on compte 1 pour 1 machine
	        # presente à 20% + 1 machine présente à 80%. Ce METP servira pour le calcul de la moyenne ponderee. 
	        $nb_machines_temps_plein += $ratio_presence;

            // On ajoute le pourcentage de présence sur la période dans le label 
            // on met en orange les machines partiellement présentes et en vert celles présentes sur toute la période
            if ($pourcentage<100) {
            	$this->_labels[] = sprintf("%s   \n(%.2f%s)",$this->_machines[$i],$pourcentage,"%");
            	$this->_palette[] = array("R"=>255,"G"=>165,"B"=>0,"Alpha"=>100);
            } else {
            	$this->_labels[] = $this->_machines[$i];
            	$this->_palette[] = array("R"=>0,"G"=>255,"B"=>0,"Alpha"=>100);
            }
       
            $i++;
        }


	    # La moyenne est obtenue par la somme des nombre d'heures de chaque machine au prorata de sa presence (on l'a dans $this->_moyenne)
	    # qu'on divise ensuite par le nombre de machines équivalent temps plein (et non pas le nombre de machines physiques presentes sur
	    # la periode, car on a pris en compte pour le calcul que certaines n'etaient la que pendant un pourcentage de la periode). 
	    $this->_moyenne /= $nb_machines_temps_plein; 

        return $dataArray;
    }


    /**
     * Affiche le graphe
     *
     * @return empty
     */
    public function display()
    {
        $this->setMachines();
        if (count($this->_machines) > 0) {
            $this->setTitre();
            $this->setDureePresenceMachines();

            $graphData = $this->fetchData();

            /* Create and populate the pData object */
            $myData = new \pData();
            $myData->addPoints($graphData, 'Nombre d\'heures');

            $myData->setAxisName(0, 'Nombre d\'heures');
            $myData->addPoints($this->_labels, 'Labels');

            $myData->setAbscissa('Labels');

            $serieSettings = array('R' => 255, 'G' => 0, 'B' => 0, 'Alpha' => 80);
            $myData->setPalette('Nombre d\'heures', $serieSettings);

            /* Create the pChart object */
            $myPicture = new \pImage($this->_largeur, 370, $myData);

            /* Draw the background */
            $settings = array('R' => 170, 'G' => 183, 'B' => 87, 'Dash' => 1, 'DashR' => 190, 'DashG' => 203, 'DashB' => 107);
            $myPicture->drawFilledRectangle(0, 0, $this->_largeur, 370, $settings);

            /* Overlay with a gradient */
            $settings = array('StartR' => 219, 'StartG' => 231, 'StartB' => 139, 'EndR' => 1, 'EndG' => 138, 'EndB' => 68, 'Alpha' => 50);
            $myPicture->drawGradientArea(0, 0, $this->_largeur, 370, DIRECTION_VERTICAL, $settings);
            $myPicture->drawGradientArea(0, 0, $this->_largeur, 20, DIRECTION_VERTICAL, array('StartR' => 0, 'StartG' => 0, 'StartB' => 0, 'EndR' => 50, 'EndG' => 50, 'EndB' => 50, 'Alpha' => 80));

            /* Add a border to the picture */
            $myPicture->drawRectangle(0, 0, $this->_largeur-1, 369, array('R' => 0, 'G' => 0, 'B' => 0));

            /* Write the picture title */
            $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 11));
            $myPicture->drawText(10, 20, $this->_titre, array('R' => 255, 'G' => 255, 'B' => 255));

            /* Write the chart title */
            $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/Forgotte.ttf', 'FontSize' => 12));

            /* Draw the scale and the 1st chart */
            $myPicture->setGraphArea(150, 60, $this->_largeur-100, 190);
            $myPicture->drawFilledRectangle(150, 60, $this->_largeur-100, 190, array('R' => 255, 'G' => 255, 'B' => 255, 'Surrounding' => -200, 'Alpha' => 10));

            $myPicture->drawScale(array('DrawSubTicks' => true, 'LabelRotation' => 45, 'Mode' => SCALE_MODE_START0));
            $myPicture->setShadow(true, array('X' => 1, 'Y' => 1, 'R' => 0, 'G' => 0, 'B' => 0, 'Alpha' => 10));
            $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/calibri.ttf', 'FontSize' => 11));
            $myPicture->drawBarChart(array('DisplayValues' => true, 'Rounded' => true, 'Surrounding' => 30, 'OverrideColors'=> $this->_palette));
            //$myPicture->drawBarChart(array('DisplayValues' => true, 'Rounded' => true, 'Surrounding' => 30));
            $myPicture->setShadow(false);

            # La moyenne...
            $myPicture->drawThreshold($this->_moyenne, array('WriteCaption' => true, 'Caption' => "Moyenne : ".round($this->_moyenne,2), 'BoxAlpha' => 40, 'BoxR' => 255, 'BoxG' => 40, 'BoxB' => 70, 'Alpha' => 70, 'Ticks' => 1, 'R' => 255, 'G' => 40, 'B' => 70));
        } else {

          /* Create and populate the pData object */
          $myData = new \pData();

          /* Create the pChart object */
          $myPicture = new \pImage($this->_largeur, 60, $myData);

          /* Write the picture title */
          $myPicture->setFontProperties(array('FontName' => '../../lib/pChart/fonts/verdana.ttf', 'FontSize' => 14));
          $myPicture->drawText(10, 55, "Impossible de générer le graphe : Il n'y a aucune machine dans la salle pour la période choisie", array('R' => 231, 'G' => 76, 'B' => 60));

        }

        /* Render the picture (choose the best way) */
        $myPicture->autoOutput('pictures/example.drawBarChart.png');
    }



    /**
     * Exporte les données du graphe
     *
     * @return le tableau des données du graphe
     */
    public function export()
    {
        $this->setMachines();
        if (count($this->_machines) > 0) {
            $this->setDureePresenceMachines();
            $graphData = $this->fetchData(); 
            $label     = $this->_labels;
            $i         = 0;
            $export    = array();
            foreach ($label as $key => $value)
            {
                $export[$i]["salle"] = $value;
                $export[$i]["heures"] =  $graphData[$i];
                $i++;
            }
        } else {
            $export["erreur"]="Impossible de générer le graphe : Il n'y a aucune machine dans la salle pour la période choisie";
        }
        return $export;
    }
}
