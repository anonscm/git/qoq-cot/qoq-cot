<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
/**
 * Front Controller
 *
 * @category Administration
 * @package  QoQ-CoT
 * @author   Arnaud Salvucci <arnaud.salvucci@univ-amu.fr>
 * @license  GPLv3 http://www.gnu.org/licenses/gpl-3.0.en.html
 */

$pdo = Dao::getInstance();

$format='json';
if (isset($_GET['format'])) {
	$format=$_GET['format'];
}

if (isset($_GET['liste'])) {
	switch ($_GET['liste']) {
		case 'salles' :
			$sql = "SELECT * FROM Salles ORDER BY Site,Composante,NomSalle ASC";
			$query = $pdo->prepare($sql);
			$query->execute();
			$listeSalles = $query->fetchAll(PDO::FETCH_OBJ);
			echo json_encode($listeSalles);
			break;
		case 'composantes' :
			$sql = "SELECT DISTINCT Composante,Site FROM Salles ORDER BY Site,Composante";
			$query = $pdo->prepare($sql);
			$query->execute();
			$listeSalles = $query->fetchAll(PDO::FETCH_OBJ);
			echo json_encode($listeSalles);
			break;
		case 'graphes' :
			$graphes = array ('cpu','cpudetail','maxcomposantes','cumulduree');
			echo json_encode($graphes);
			break;
	}
} else if (isset($_GET['graphe'])) {
	switch ($_GET['graphe']) {
		case 'cpu' :
		case 'cpudetail':
		case 'maxcomposante':
			$mois=$_GET['mois'];
			if (isset($_GET['salles'])) {
				$salles = serialize($_GET['salles']);
			} else {
				$salles = '';
			}
			$year             = $_GET['annee'];
			$largeur          = 0;
			$composante       = htmlspecialchars($_GET['composante'], ENT_QUOTES);
			$heureDebut       = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
			$heureFin         = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
			$joursOuvresArray = serialize($_GET['joursouvres']);
			$excluDebut       = htmlspecialchars($_GET['excludebut'], ENT_QUOTES);
			$excluFin         = htmlspecialchars($_GET['exclufin'], ENT_QUOTES);
			switch ($_GET['graphe']) {
				case 'cpu' :
					require_once 'app/graphe/CpuGraph.class.php';
					$graphe = new Dosicalu\QoQCoT\App\Graphe\CpuGraph($mois, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
					break;
				case 'cpudetail' :
					require_once 'app/graphe/CpuDetailGraph.class.php';
					$graphe = new Dosicalu\QoQCoT\App\Graphe\CpuDetailGraph($mois, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
					break;
				case 'maxcomposante' :
					require_once 'app/graphe/MaxComposanteGraph.class.php';
					$graphe = new Dosicalu\QoQCoT\App\Graphe\MaxComposanteGraph($mois, $year, $composante, $salles, $largeur, $heureDebut, $heureFin, $joursOuvresArray, $excluDebut, $excluFin);
					break;
			}
			break;
		case 'cumulduree' :
			$largeur          = 0;
			$salle            = $_GET['salle'];
			$date             = htmlspecialchars($_GET['date'], ENT_QUOTES);
			$datefin          = htmlspecialchars($_GET['datefin'], ENT_QUOTES);
			$heureDebut       = htmlspecialchars($_GET['heuredebut'], ENT_QUOTES);
			$heureFin         = htmlspecialchars($_GET['heurefin'], ENT_QUOTES);
			$joursOuvres      = serialize($_GET['joursouvres']);
			require_once 'app/graphe/CumulDureeGraph.class.php';
			$graphe = new Dosicalu\QoQCoT\App\Graphe\CumulDureeGraph($date, $datefin, null, $salle, $largeur, $heureDebut, $heureFin, $joursOuvres);
			break;
	}
	echo json_encode($graphe->export());
} else {
	if ($_SESSION['role'] === 'ROLE_ADMIN') {
		require_once 'app/suivi/index.php';
	} else {
		echo json_encode(array("erreur"=>"Ce compte n'est pas autorisé à accéder au suivi"));
	}
}
