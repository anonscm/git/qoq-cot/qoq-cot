<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of QoQ-CoT.
 * 
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Déclaration de permission de copie
 * 
 * Ce fichier fait partie de QoQ-CoT.
 * 
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */
$pdo = Dao::getInstance();

$sql = 'SELECT login FROM Admins WHERE login = "'.$_POST['login'].'"';

$query = $pdo->prepare($sql);

$query->execute();
$listeAdmin = $query->fetchAll(PDO::FETCH_OBJ);

if (!empty($listeAdmin)) {

    echo 1;

 } else if ($_POST['login'] === '') {

    echo 2;

 } else {

    $sql = 'INSERT INTO Admins (login) VALUES ( "'.$_POST['login'].'")';

    $query = $pdo->prepare($sql);

    $query->execute();

    $sqlAllAdmin = 'SELECT login FROM Admins ORDER BY login ASC';

    $queryAllAdmin = $pdo->prepare($sqlAllAdmin);

    $queryAllAdmin->execute();
    $listeAllAdmin = $queryAllAdmin->fetchAll(PDO::FETCH_OBJ);

    $liste = '';

    foreach ($listeAllAdmin as $admin) {

        if ($admin->login === $_SESSION['CURRENT_USER'])
            continue;
        $liste .= '<tr><td>'.$admin->login.'</td><td><a id="'.$admin->login.'" style="clear:both;padding:5px;" class="ui-button ui-widget-content ui-corner-all del_admin"><img  align="absmiddle" src="img/delete.png">&nbsp;Supprimer&nbsp;</a></td></tr>';
    }

    echo $liste;
 }

