<?php
/**
 *
 * ********************************* ENGLISH *********************************
 * 
 * --- Copyright notice :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Statement of copying permission
 * 
 * This file is part of QoQ-CoT.
 * 
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 * 
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 * 
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 * 
 * 
 * --- Déclaration de permission de copie
 * 
 * Ce fichier fait partie de QoQ-CoT.
 * 
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 * 
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 * 
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 * 
 */
if (isset($_GET['action'])) {

    switch ($_GET['action']) {

    case 'add':
        require_once 'add.php';
        exit;

    case 'delete':
        require_once 'delete.php';
        exit;
    }

 }

$pdo = Dao::getInstance();

$sql = 'SELECT login FROM Admins ORDER BY login ASC';

$query = $pdo->prepare($sql);

$query->execute();
$listeAdmin = $query->fetchAll(PDO::FETCH_OBJ);

$script  = '';
$wrapper = '';

$wrapper .= '<div id="container">';

$wrapper .= '<h2>Gestion des administrateurs</h2>';

$wrapper .= '<table>';
$wrapper .= '<thead>';
$wrapper .= '<td>Login</td>';
$wrapper .= '<td>Opérations</td>';
$wrapper .= '</thead>';

$wrapper .= '<tbody>';

foreach ($listeAdmin as $admin) {

    if ($admin->login === $_SESSION['CURRENT_USER'])
        continue;
    $wrapper .= '<tr><td>'.$admin->login.'</td><td><a id="'.$admin->login.'" style="clear:both;padding:5px;" class="ui-button ui-widget-content ui-corner-all del_admin"><img  align="absmiddle" src="img/delete.png">&nbsp;Supprimer&nbsp;</a></td></tr>';
}

$wrapper .= '</tbody>';
$wrapper .= '</table>';

$wrapper .= '<h3>Ajouter un nouvel administrateur</h3>';

$wrapper .= '<form method="post" action="index.php">';
$wrapper .= '<label for="login">Login : </label>';
$wrapper .= '<input id="login" name="login" />';
$wrapper .= '<a id="add_admin" style="clear:both;padding:5px;" class="ui-button ui-widget-content ui-corner-all"><img  align="absmiddle" src="img/add.png">&nbsp;Ajouter un admin&nbsp;</a>';



$wrapper .= '</form>';

$wrapper .= '<div id="dialog-confirm" title="Suppression">
<p><span class="ui-icon ui-icon-alert" style="float:left; margin:0 7px 20px 0;"></span>Désintégration de <span id="nomadmin"></span> ? Vraiment ?</p>
</div>';

$wrapper .= '</div>';

$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery.js"></script>';
$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui.js"></script>';
$script .= '<script type="text/javascript" src="js/controller.js"></script>';

require_once 'layout.php';
