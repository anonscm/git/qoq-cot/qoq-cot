<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */

function timePickerToMySQL($date) {
    return preg_replace('#(\d+)/(\d+)/(\d+)#','${3}-${2}-${1}', $date);
}

function MySQLTotimePicker($date) {
    return preg_replace('#(\d+)-(\d+)-(\d+)#','${3}/${2}/${1}', $date);
}

function firstSelection ($field,$multiple) {
	$wrapper = '<option value=""';
  if (! isset($_GET[$field]) || ($multiple && $_GET[$field][0] === '') ) 
      $wrapper .= 'selected="selected"';
  $wrapper .= '></option>';
	return $wrapper;
}

if ($_GET['app']=='suivi') {
	$ws=false;
} else {
	$ws=true;
}

if ( ! $ws ) {

	require_once 'utils.php';
	
	if (isset($_GET['dated']) && $_GET['dated'] !== '') {
	    $datedebut = timePickerToMysql($_GET['dated']);
	} else {
	    $datedebut = date('Y-m-d 00:00');
	}
	
	$tableConnexions = \Dao::getTableConnexions(new \DateTime($datedebut)); 
	
	if (isset($_GET['datef']) && $_GET['datef'] !== '') {
	    $datefin = timePickerToMysql($_GET['datef']);
	} else {
	    $datefin = date('Y-m-d 23:59:59');
	}
	
	$pdo = Dao::getInstance();
	
	$where='';
	if (isset($_SESSION['site']) && $_SESSION['site']!=='') {
	    $where='WHERE Site="'.$_SESSION['site'].'"';
	}
	
	$sql = "SELECT * FROM Salles $where ORDER BY Composante,NomSalle ASC";
	
	$query = $pdo->prepare($sql);
	
	$query->execute();
	$listeSalles = $query->fetchAll(PDO::FETCH_OBJ);
	
	$sql = "SELECT DISTINCT(Composante) FROM Salles $where ORDER BY composante ASC";
	
	$query = $pdo->prepare($sql);
	
	$query->execute();
	$listeComposantes = $query->fetchAll(PDO::FETCH_OBJ);
	
	$sql = 'SELECT DISTINCT(NomOs) FROM '.$tableConnexions.' ORDER BY NomOs ASC';
	
	$query = $pdo->prepare($sql);
	
	$query->execute();
	$listeOs = $query->fetchAll(PDO::FETCH_OBJ);
	
	$sql = 'SELECT DISTINCT(Pool) FROM '.$tableConnexions.' WHERE Pool IS NOT NULL ORDER BY Pool ASC';
	
	$query = $pdo->prepare($sql);
	
	$query->execute();
	$listePools = $query->fetchAll(PDO::FETCH_OBJ);

	$script  = '';
	$wrapper ='';
	
	$wrapper .= '<h2>Suivi des connexions <A HREF="https://sourcesup.renater.fr/wiki/qoq-cot/suivi_connexions_'.VERSION_FORMAT_WIKI.'" TARGET=_blank><FONT SIZE=-1><I>(?)</I></FONT></A></h2>';
	
	$wrapper .= '<div id="form-recherche">';

  $wrapper .= '<div id=container>';

	$wrapper .= '<form action="index.php" method="get" autocomplete="off">';
	
	$wrapper .= '<input type="hidden" name="app" value="suivi"/>';
	
	$wrapper .= '<p>';

	$wrapper .= '<label for="composante" id="l_composante">Groupe de salles: </label>';

	$wrapper .= '<select name="composante" id="composante" class="large">';

  $wrapper .= firstSelection('composante',false);

	foreach ($listeComposantes as $c) {
	
	    $wrapper .= '<option value="'.$c->Composante.'" ';
	
	    if (isset($_GET['composante']))
	        if ($c->Composante == $_GET['composante'])
	            $wrapper .= 'selected="selected"';
	
	    $wrapper .= '>'.$c->Composante.'</option>';
	}
	
	$wrapper .= '</select>';

	$wrapper .= '<label for="nommachine">Nom machine :</label>';
	
	$wrapper .= '<input type="text" id="nommachine" name="nommachine" ';
	
	if (isset($_GET['nommachine']) && $_GET['nommachine'] !== '')
	    $wrapper .= 'value="'.$_GET['nommachine'].'"';
	
	$wrapper .= ' />';

	$wrapper .= '<label for="ipmachine">IP machine :</label>';
	
	$wrapper .= '<input type="text" id="ipmachine" name="ipmachine" ';
	
	if (isset($_GET['ipmachine']) && $_GET['ipmachine'] !== '')
	    $wrapper .= 'value="'.$_GET['ipmachine'].'"';
	
	$wrapper .= ' />';
	
	
	$wrapper .= '</p>';

	$wrapper .= '<p>';
	$wrapper .= '<label for="salle" id="l_salle">Salle :</label>';
	
	$wrapper .= '<select name="salles[]" id="salle" multiple="multiple" class="large">';

	$wrapper .= firstSelection('salles',true);

	$optgroup = '';
	
	foreach ($listeSalles as $s) {
	
	    if ($s->Composante !== $optgroup) {
	        if ($optgroup != '') {
	            $wrapper .= '</optgroup>';
	        } 
	        $wrapper .= '<optgroup label="'.$s->Composante.'">';
	        $optgroup = $s->Composante;
	    }
	
	    $wrapper .= '<option value="'.$s->IdSalle.'" ';
	
	    if (isset($_GET['salles']))
	        if (in_array($s->IdSalle, $_GET['salles']))
	            $wrapper .= 'selected="selected"';
	
	    $wrapper .= '>'.$s->NomSalle.'</option>';
	}
	
	if ($optgroup != '') {
	    $wrapper .= '</optgroup>';
	}
	
	$wrapper .= '</select>';
	
	$wrapper .= '<label for="os">OS :</label>';
	
	$wrapper .= '<select id="os" name="os[]" multiple="multiple" class="medium">';
	
	$wrapper .= firstSelection('os',true);
	
	foreach ($listeOs as $os) {
	
	    $wrapper .= '<option value="'.$os->NomOs.'" ';
	
	    if (isset($_GET['os']))
          if (in_array($os->NomOs, $_GET['os']))
	            $wrapper .= 'selected="selected"';
	
	    $wrapper .= '>'.$os->NomOs.'</option>';
	}
	
	$wrapper .= '</select>';
	
	$wrapper .= '<label for="pool">Pool :</label>';
	
	$wrapper .= '<select id="pool" name="pool[]" multiple="multiple">';
	
	$wrapper .= firstSelection('pool',true);
	
	foreach ($listePools as $pool) {
	
	    $wrapper .= '<option value="'.$pool->Pool.'" ';
	
	    if (isset($_GET['pool']))
          if (in_array($pool->Pool, $_GET['pool']))
	        $wrapper .= 'selected="selected"';
	
	    $wrapper .= '>'.$pool->Pool.'</option>';
	}
	
	$wrapper .= '</select>';

	$wrapper .= '</p>';

	$wrapper .= '<p>';

	$wrapper .= '<label for="dated" id="l_dated">Entre: </label>';
	
	$wrapper .= '<input type="text" id="dated" name="dated" ';
	
	$wrapper .= 'value="'.MySQLTotimePicker($datedebut).'"';
	
	$wrapper .= ' />';
	
	$wrapper .= '<label for="datef" id="l_datef">Et:</label>';
	
	$wrapper .= '<input type="text" id="datef" name="datef" ';
	
	$wrapper .= 'value="'.MySQLTotimePicker($datefin).'"';
	
	$wrapper .= ' />';
	
	$wrapper .= '<label for="login">Login:</label>';

	$wrapper .= '<input type="text" name="login" id="login" ';
	
	if (isset($_GET['login']) && $_GET['login'] !== '')
	    $wrapper .= 'value="'.$_GET['login'].'"';
	
	$wrapper .= ' />';
	
	$wrapper .= '</p>';

	$wrapper .= '<p><center><input type="submit" name="submit" id="submit" value="Rechercher" /><center></p>';
	
	$wrapper .= '</form>';
	
	$wrapper .= '</div>';

	$wrapper .= '</div>';
	
} else {
	if (isset($_GET['dated']) && $_GET['dated'] !== '') {
	    $datedebut = $_GET['dated'];
	} else {
	    $datedebut = date('Y-m-d 00:00');
	}
	
	$tableConnexions = \Dao::getTableConnexions(new \DateTime($datedebut)); 

	if (isset($_GET['datef']) && $_GET['datef'] !== '') {
	    $datefin = $_GET['datef'];
	} else {
	    $datefin = date('Y-m-d 23:59:59');
	}

}

if (isset($_GET['submit']) || $ws) {

  if ( (isset($_SESSION['site']) && $_SESSION['site']!=='') || (isset($_GET['composante']) && $_GET['composante'] !== '') || (isset($_GET['salles']) && $_GET['salles'][0] !== '') ) {

  	$sql = 'SELECT DateDebut,DateFin,Duree,c.NomMachine,IPMachine,NomOs,NomVM,IPVM,Pool,Login FROM '.$tableConnexions.' AS c INNER JOIN MachinesToSalles AS ms INNER JOIN Salles as s ON c.NomMachine=ms.NomMachine AND ms.refSalle=s.idSalle WHERE NOT (DateFin < :datedebut OR DateDebut > :datefin) AND NOT (DateFin < ms.Date_DEBUT OR DateDebut > ms.Date_FIN) ';

  	if (isset($_SESSION['site']) && $_SESSION['site']!=='') {
      $sql .= 'AND site="'.$_SESSION['site'].'" ';
  	}
  
  	if (isset($_GET['composante']) && $_GET['composante'] !== '') {
  	    $sql .= 'AND Composante = :composante ';
  	    unset($_GET['salles']);
  	}
  	
  	if (isset($_GET['salles']) && $_GET['salles'][0] !=='' ) {
  	    $clauseIn = implode(",", $_GET['salles']);
  	    $sql .= 'AND RefSalle IN ('.$clauseIn.') ';
  	}
  } else {
  	$sql = 'SELECT * FROM '.$tableConnexions.' AS c WHERE NOT (DateFin < :datedebut OR DateDebut > :datefin) ';
  }
  
  if (isset($_GET['nommachine']) && $_GET['nommachine'] !== '')
      $sql .= 'AND (c.NomMachine LIKE :nommachine OR c.NomVM LIKE :nommachine) ';
  
  if (isset($_GET['ipmachine']) && $_GET['ipmachine'] !== '')
      $sql .= 'AND (c.IPMachine LIKE :ipmachine OR c.IPVM LIKE :ipmachine) ';
 
  if (isset($_GET['os']) && $_GET['os'][0] !=='' ) {
      $clauseIn = '"'.implode('","', $_GET['os']).'"';
      $sql .= 'AND NomOs IN ('.$clauseIn.') ';
  }
  
  if (isset($_GET['pool']) && $_GET['pool'][0] !=='') {
     $clauseIn = '"'.implode('","', $_GET['pool']).'"';
     $sql .= 'AND Pool IN ('.$clauseIn.') ';
  }
 
  if (isset($_GET['login']) && $_GET['login'] !== '')
      $sql .= 'AND Login LIKE :login ';
  
  $sql .= 'ORDER BY DateDebut DESC';
  
  $query = $pdo->prepare($sql);
  
  if (isset($_GET['composante']) && $_GET['composante'] !== '') {
      $composante = $_GET['composante'];
      $query->bindParam(':composante', $composante, PDO::PARAM_STR);
  }
  
  $query->bindParam(':datedebut', $datedebut, PDO::PARAM_STR);
  $query->bindParam(':datefin', $datefin, PDO::PARAM_STR);
  
  if (isset($_GET['nommachine']) && $_GET['nommachine'] !== '') {
      $nomMachine = $_GET['nommachine'];
      $query->bindParam(':nommachine', $nomMachine, PDO::PARAM_STR);
   }
  
  if (isset($_GET['ipmachine']) && $_GET['ipmachine'] !== '') {
      $ipMachine = '%'.$_GET['ipmachine'].'%';
      $query->bindParam(':ipmachine', $ipMachine, PDO::PARAM_STR);
   }
  
  if (isset($_GET['login']) && $_GET['login'] !== '') {
      $login = '%'.$_GET['login'].'%';
      $query->bindParam(':login', $login, PDO::PARAM_STR);
   }
  
  $query->execute();
  
  $connexions = $query->fetchAll(PDO::FETCH_OBJ);
  
  if ($ws) {
  
  	if (!empty($connexions)) {
  
  		echo json_encode($connexions);
  
  	} else {
  
  		echo json_encode(array("erreur"=>"Il n'y a pas de résultat correspondant à vos critères de recherche"));
  
  	}
  
  } else {
  
  	if (!empty($connexions)) {
  	    $wrapper .= '<div id="connexions">';
  	
  	    $wrapper .= '<table id="tableau_connexions">';
  	
  	    $wrapper .= '<thead><tr>';
  	
  	    $wrapper .= '<td>N°</td>';
  	
  	    $wrapper .= '<td>Date de début de connexion</td>';
  	
  	    $wrapper .= '<td>Date de fin de connexion</td>';
  	
  	    $wrapper .= '<td>Duree</td>';
  	
  	    $wrapper .= '<td>Nom terminal</td>';
  	
  	    $wrapper .= '<td>IP terminal</td>';
  	
  	    $wrapper .= '<td>OS</td>';
  	
  	    $wrapper .= '<td>Nom VM</td>';
  	
  	    $wrapper .= '<td>IP VM</td>';

  	    $wrapper .= '<td>Pool</td>';
  	
  	    $wrapper .= '<td>Login</td></tr></thead>';
  	
  	    $wrapper .= '<tbody>';
  	
  	    foreach ($connexions as $c) {
  	        $url = str_replace('_LOGIN_',$c->Login, URL_ANNUAIRE);
  	        $wrapper .= '<tr><td class="rownums"></td><td>'.$c->DateDebut.'</td><td>'.$c->DateFin.'</td><td>'.$c->Duree.'</td><td>'.$c->NomMachine.'</td><td>'.$c->IPMachine.'</td><td>'.$c->NomOs.'</td><td>'.$c->NomVM.'</td><td>'.$c->IPVM.'</td><td>'.$c->Pool.'</td><td><a target="_blank" href="'.$url.'">'.$c->Login.'</a></td></tr>';
  	    }
  	
  	    $wrapper .= '</tbody>';
  	
  	    $wrapper .= '</table>';
  	
  	    $wrapper .= '</div>';
  	
  	} else {
  	
  	    $wrapper .= '<p>Il n\'y a pas de résultat correspondant à vos critères de recherche</p>';
  	}
  
  }	
  
  }

if (! $ws ) {
	$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery.js"></script>';
	$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui.js"></script>';
	$script .= '<script type="text/javascript" src="js/jquery-ui/js/jquery-ui-timepicker-addon.js"></script>';
	$script .= '<script type="text/javascript" src="js/initTimepicker.js"></script>';
	$script .= '<script type="text/javascript" src="js/tablesorter/js/jquery.tablesorter.min.js"></script>';
	$script .= '<script type="text/javascript" src="js/initTableSorter.js"></script>';
	$script .= '<script type="text/javascript" src="js/toogleselect.js"></script>';
	
	require_once 'layout.php';
}
