<?php
/**
 *
 * ********************************* ENGLISH *********************************
 *
 * --- Copyright notice :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Statement of copying permission
 *
 * This file is part of QoQ-CoT.
 *
 * QoQ-CoT is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * QoQ-CoT is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with QoQ-CoT; if not, write to the Free Software
 * Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 * *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
 *
 * --- Notice de Copyright :
 *
 * Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
 *
 *
 * --- Déclaration de permission de copie
 *
 * Ce fichier fait partie de QoQ-CoT.
 *
 * QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
 * selon les termes de la Licence Publique Générale GNU telle qu'elle est
 * publiée par la Free Software Foundation ; soit la version 3 de la Licence,
 * soit (à votre choix) une quelconque version ultérieure.
 *
 * QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
 * GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
 * d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
 * pour plus de détails.
 *
 * Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
 * QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
 * 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
 *
 */
session_start();


# On stocke dans la variable de sessioon $_SESSION['CURRENT_USER'] la 1re des 3 variables $_SERVER['REMOTE_USER'], $_SERVER['REDIRECT_REMOTE_USER'], $_SERVER['PHP_AUTH_USER']
# qui est instanciée et on utilise partout la variable de session pour référencer l'utilisateur connecté. Ça permet d'être plus robuste aux différentes configurations Apache
# qu'on peut trouver dans la communauté des cocottes.
if (!isset($_SESSION['CURRENT_USER']))
{
	if (isset($_SERVER['REMOTE_USER']))
	{
		$_SESSION['CURRENT_USER'] = $_SERVER['REMOTE_USER'];
	}
	else if (isset($_SERVER['REDIRECT_REMOTE_USER']))
	{
		$_SESSION['CURRENT_USER'] = $_SERVER['REDIRECT_REMOTE_USER'];
	}
	else if (isset($_SERVER['PHP_AUTH_USER']))
	{
		$_SESSION['CURRENT_USER'] = $_SERVER['PHP_AUTH_USER'];
	}
}

if (!isset($_SESSION['CURRENT_USER'])) {
    header('WWW-Authenticate: Basic realm="My Realm"');
    header('HTTP/1.0 401 Unauthorized');
    echo 'Défaut d\'authentification : vous semblez ne pas exister, on ne va pas pouvoir entamer une relation sérieuse...';
    exit;
}

require_once 'config.php';
require_once 'config_interne.php';
require_once 'lib/Dao.class.php';
require_once 'checkRoles.php';

if (isset($_GET["site"])) {
    $sites=unserialize(SITES);
    if (in_array($_GET["site"],$sites) || $_GET["site"]==='') { 
        $_SESSION["site"]=$_GET["site"];
    }
}

$wrapper = '';
$script  = '';


if (isset($_GET['app'])) {

    if ($_GET['app'] === 'graphe') {

        require_once 'app/graphe/index.php';

    } else if ($_GET['app'] === 'ws') {

        if (defined('WS_ENABLED') && WS_ENABLED) {

           require_once 'app/ws/index.php'; 

        } else {
                  
  		     echo json_encode(array("erreur"=>"Le webservice n'est pas activé"));

        } 

        exit;

    } else if ($_GET['app'] === 'suivi' && $_SESSION['role'] === 'ROLE_ADMIN') {

        require_once 'app/suivi/index.php';

    } else if ($_GET['app'] === 'actu' && $_SESSION['role'] === 'ROLE_ADMIN') {

        require_once 'app/actu/index.php';

    } else if ($_GET['app'] === 'admin' && $_SESSION['role'] === 'ROLE_ADMIN') {

        require_once 'app/admin/index.php';
    }
} else {
    $wrapper = '<img src="img/logo1.2-800.png" alt="La QoQ-CoT" id="logo" />';
}

require_once 'layout.php';
