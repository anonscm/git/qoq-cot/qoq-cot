#!/bin/bash

##########################################################################################
# ********************************* ENGLISH *********************************
# 
# --- Copyright notice :
# 
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Statement of copying permission
# 
# This file is part of QoQ-CoT.
# 
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
# 
# *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
#
# --- Notice de Copyright :
# 
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Déclaration de permission de copie
# 
# Ce fichier fait partie de QoQ-CoT.
# 
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique Générale GNU telle qu'elle est
# publiée par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (à votre choix) une quelconque version ultérieure.
# 
# QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
# pour plus de détails.
# 
# Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
# QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
##########################################################################################

##########################################################################################
## SCRIPT de mise à jour de la structure de la DB QoQ-CoT pour passage V2 to V3         ##
## 1. Ajout du droit trigger pour l'utilisateur QoQ-CoT sur la base MySQL QoQ-CoT       ##
## 2. Modification propriétés champ IdProcess table Connexions                          ##
## 3. Suppression table inutile Aliases                                                 ##
## 4. Transformation des noms de machines longs (avec domaine) en noms courts dans les  ##
## tables concernées de la base MySQL QoQ-CoT                                           ##
##                                                                                      ##
## Ce script Peut être lancé plusieurs fois sans problèmes : chaque action peut être    ##
## interactivement sautée et au pire, la refaire n'aura pas d'impact et ne provoquera   ##
## pas d'erreur.                                                                        ##
##########################################################################################

noir='\033[0;30m'
gris='\033[1;30m'
rougefonce='\033[0;31m'
rose='\033[1;31m'
vertfonce='\033[0;32m'
vertclair='\033[1;32m'
orange='\033[0;33m'
jaune='\033[1;33m'
bleufonce='\033[0;34m'
bleuclair='\033[1;34m'
violetfonce='\033[0;35m'
violetclair='\033[1;35m'
cyanfonce='\033[0;36m'
cyanclair='\033[1;36m'
grisclair='\033[0;37m'
blanc='\033[1;37m'

neutre='\033[0;m'

printf "\n${rougefonce}Si vous exécutez ce script, c'est parce que vous avez upgradé votre vieille QoQ-CoT depuis une version < 3.0 vers une version >= 3.0. Vous avez par ailleurs pris soin de SAUVEGARDER votre base MySQL QoQ-CoT. OK sur ces deux points ? [o/n]${neutre}\n"

while true
do
        read reponse
        case $reponse in
        o|O)
                break
                ;;
        n|N)
                printf "ABORT\n"
                exit 1
                ;;
        *)
                printf "${rougefonce}? [o/n]${neutre}\n"
                ;;
        esac
done

# Chemin relatif vers votre fichier config.php correctement renseigné
CONFIG_PHP="./config.php"

MYSQL_SERVER=$(grep -w SQL_HOST $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
#MYSQL_PORT=$(grep -w SQL_PORT $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")
MYSQL_PORT=$(grep -w SQL_PORT $CONFIG_PHP | sed -e "s/[^,]*,[^0-9]*\([0-9]*\).*$/\1/")
QOQ_COT_MYSQL_DB=$(grep -w SQL_DBNAME $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
QOQ_COT_MYSQL_USER=$(grep -w SQL_USERNAME $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
QOQ_COT_MYSQL_USER_PASSWORD=$(grep -w SQL_PASSWORD $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")

printf "\n${vertclair}Paramètres MySQL récupérés depuis $CONFIG_PHP (ils vont être utilisés pour lancer automatiquement les commandes) :${neutre}\n"
printf -- "--> Serveur MySQL : ${vertclair}$MYSQL_SERVER${neutre}\n"
printf -- "--> Port MySQL : ${vertclair}$MYSQL_PORT${neutre}\n"
printf -- "--> Base MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_DB${neutre}\n"
printf -- "--> Utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER${neutre}\n"
printf -- "--> Mot de passe de l'Utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER_PASSWORD${neutre}\n"

# On recupere le pass root MySQL pour ne le demander qu'un fois
printf "\n${rougefonce}Password root MySQL (nécessaire pour certaines commandes) : ${neutre}\n"
read -s mysql_root_password

#######################
# Ajout droit TRIGGER #
#######################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Lancer l'ajout du droit MySQL \"TRIGGER\" sur la base $QOQ_COT_MYSQL_DB pour l'utilisateur $QOQ_COT_MYSQL_USER ? [o/n]${neutre}\n"

while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done


if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then

	printf "${rougefonce}Nom DNS complet du serveur web QoQ-CoT (pour autoriser depuis cette machine, d'où le trigger sera créé via le setup.php), ou localhost si le serveur web tourne sur la même machine que le serveur MySQL :${neutre}\n"
	while true
	do
		read DNS_QoQ_CoT_WEB_SERVER
		printf "${vertclair}Serveur web = $DNS_QoQ_CoT_WEB_SERVER. OK ? [o/n]${neutre}\n"
		read reponse
		case $reponse in
		o|O)
			break
			;;
		*) 
			printf "${rougefonce}Nom DNS complet du serveur web QoQ-CoT :${neutre}\n"
			;;
		esac
	done
	SQL_COMMAND="";
	if [ "$DNS_QoQ_CoT_WEB_SERVER" = "localhost" ] 
	then
		SQL_COMMAND="GRANT TRIGGER ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'localhost';" 
	else
		SQL_COMMAND="GRANT TRIGGER ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'$DNS_QoQ_CoT_WEB_SERVER';" 
	fi
	
	printf "On lance la commande suivante : ${vertclair}$SQL_COMMAND${neutre}\n"

	mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT -p$mysql_root_password -e "$SQL_COMMAND"
	
	printf "\n${vertclair}OK. $QOQ_COT_MYSQL_USER a désormais les droits pour créer un TRIGGER sur la base $QOQ_COT_MYSQL_DB depuis $DNS_QoQ_CoT_WEB_SERVER.\nIl ne vous reste plus qu'à lancer le setup.php sur $DNS_QoQ_CoT_WEB_SERVER qui créera le TRIGGER permettant d'empêcher l'inscription d'une date de fin de connexion antérieure à celle déjà inscrite. La marge de manoeuvre de l'indélicat qui veut abuser de la cocotte se réduit fortement...${neutre}\n"

fi
###########################
# FIN Ajout droit TRIGGER #
###########################

#####################################################
# Modif proriétés champ IdProcesss table Connexions #
#####################################################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Modification des propriétés du champs IdProcess de la table Connexions, base $QOQ_COT_MYSQL_DB ? [o/n]${neutre}\n"
while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done

if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then
	SQL_COMMAND_2="";
	SQL_COMMAND_2="USE $QOQ_COT_MYSQL_DB; ALTER TABLE Connexions CHANGE IdProcess IdProcess VARCHAR(255) NULL;"
	
	printf "On lance la commande suivante : ${vertclair}$SQL_COMMAND_2${neutre}\n"
	
	mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT -p$mysql_root_password -e "$SQL_COMMAND_2"
	
	printf "\n${vertclair}OK. La table Connexions de la base $QOQ_COT_MYSQL_DB est désormais MySQL strict mode ready. Une source de bug potentiel en moins...${neutre}\n"
fi
#########################################################
# FIN modif proriétés champ IdProcesss table Connexions #
#########################################################

#####################################
# Suppression Table inutile Aliases #
#####################################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Suppression table inutile Aliases, base $QOQ_COT_MYSQL_DB ? [o/n]${neutre}\n"
while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done

if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then
	SQL_COMMAND_3="";
	SQL_COMMAND_3="USE $QOQ_COT_MYSQL_DB; DROP TABLE IF EXISTS Aliases;"
	
	printf "On lance la commande suivante : ${vertclair}$SQL_COMMAND_3${neutre}\n"
	
	mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT -p$mysql_root_password -e "$SQL_COMMAND_3"
	
	printf "\n${vertclair}OK. La table inutile de la base $QOQ_COT_MYSQL_DB a été supprimée. Lq QoQ-CoT a maigri et ça lui va bien.${neutre}\n"
fi

#########################################
# Fin suppression Table inutile Aliases #
#########################################

###########################################
# Passages des noms longs aux noms courts #
###########################################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Lancer la raccourcissation des noms de machines dans les tables 'MachinesToSalles' et 'Connexions' de la base $QOQ_COT_MYSQL_DB par l'utilisateur $QOQ_COT_MYSQL_USER.\n*ATTENTION TRÈS IMPORTANT* : vous DEVEZ avoir désactivé le cron de peuplade.sh AVANT de lancer la manip. !!!\nOn y va ? [o/n]${neutre}\n"

while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done

if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then
	# On montre le nombre d'enregistrements qui va être modifié 
	SQL_COMMAND_VIEW="SELECT count(*) as NbNomsLongsDansConnexions FROM Connexions WHERE NomMachine LIKE '%.%';SELECT count(*) as NbNomsLongsDansMachinesToSalles FROM MachinesToSalles WHERE NomMachine LIKE '%.%';"
	printf "\n${vertclair}Nombre d'enregistrements comportant des noms longs dans les 2 tables concernées AVANT la corection :${neutre}\n"
	#echo "On lance la commande suivante : $SQL_COMMAND_VIEW\n"
	mysql -u $QOQ_COT_MYSQL_USER -h $MYSQL_SERVER -P $MYSQL_PORT -p$QOQ_COT_MYSQL_USER_PASSWORD $QOQ_COT_MYSQL_DB -e "$SQL_COMMAND_VIEW"
	
	# On envoie l'UPDATE qui va modifier
	SQL_COMMAND_UPDATE="UPDATE IGNORE Connexions SET NomMachine = SUBSTRING_INDEX(NomMachine, '.', 1) WHERE NomMachine LIKE '%.%'; UPDATE IGNORE MachinesToSalles SET NomMachine = SUBSTRING_INDEX(NomMachine, '.', 1) WHERE NomMachine LIKE '%.%';"
	printf "\n${rougefonce}Application du correctif raccourcissant (pas d'affolance, ça peut durer un petit moment)...\n${neutre}"
	echo "On lance la commande suivante : $SQL_COMMAND_UPDATE"
	mysql -u $QOQ_COT_MYSQL_USER -h $MYSQL_SERVER -P $MYSQL_PORT -p$QOQ_COT_MYSQL_USER_PASSWORD $QOQ_COT_MYSQL_DB -e "$SQL_COMMAND_UPDATE"
	printf "${vertclair}done !\n\n${neutre}"
	
	# On montre le nombre d'enregistrements en noms longs APRES le correctif : 0 sauf si doublons nom long/nom court...
	printf "\n${vertclair}Nombre d'enregistrements comportant des noms longs dans les 2 tables concernées APRÈS la corection (peut ne pas être 0 dans certains cas rares : on pourra sans soucis négliger ce résidu...) :${neutre}\n"
	#echo "On lance la commande suivante : $SQL_COMMAND_VIEW"
	mysql -u $QOQ_COT_MYSQL_USER -h $MYSQL_SERVER -P $MYSQL_PORT -p$QOQ_COT_MYSQL_USER_PASSWORD $QOQ_COT_MYSQL_DB -e "$SQL_COMMAND_VIEW"
	
	# Bilan
	printf "\n${vertclair}C'est fait !! Votre base ne contient désormais plus que des noms courts (c.-à-d. sans domaine). Bravo ;-)${neutre}\n"
	printf "\n${rougefonce}TRÈS IMPORTANT : si vous êtes en mode hybride (des clients syslog et des clients poussins/coq), \nalors remplacez le script peuplade.php par celui fourni en version 3.1 ou supérieure, \npuis recronez-le (s'il l'était). Réutiliser l'ancien peuplade.php réinsèrerait des noms longs dans votre base...${neutre}\n"
fi

###############################################
# FIN passages des noms longs aux noms courts #
###############################################
