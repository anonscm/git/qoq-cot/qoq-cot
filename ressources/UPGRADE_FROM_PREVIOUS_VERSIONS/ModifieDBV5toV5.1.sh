#!/bin/bash

##########################################################################################
# ********************************* ENGLISH *********************************
# 
# --- Copyright notice :
# 
#Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Statement of copying permission
# 
# This file is part of QoQ-CoT.
# 
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
# 
# *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
#
# --- Notice de Copyright :
# 
#Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Déclaration de permission de copie
# 
# Ce fichier fait partie de QoQ-CoT.
# 
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique Générale GNU telle qu'elle est
# publiée par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (à votre choix) une quelconque version ultérieure.
# 
# QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
# pour plus de détails.
# 
# Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
# QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
##########################################################################################

##########################################################################################
## SCRIPT de mise à jour de la structure de la DB QoQ-CoT pour passage V5.0 to V5.1     ##
## 1. Modification table MachinesToSalles : ajout des champs de date d'apparition/      ##
## disparition des machines dans la salle et suppression des anciens index desormais    ##
## non pertinents.                                                                      ##
##                                                                                      ##
## Ce script Peut être lancé plusieurs fois sans problèmes : chaque action peut être    ##
## interactivement sautée et au pire, la refaire n'aura pas d'impact et ne provoquera   ##
## pas d'erreur.                                                                        ##
##########################################################################################

noir='\033[0;30m'
gris='\033[1;30m'
rougefonce='\033[0;31m'
rose='\033[1;31m'
vertfonce='\033[0;32m'
vertclair='\033[1;32m'
orange='\033[0;33m'
jaune='\033[1;33m'
bleufonce='\033[0;34m'
bleuclair='\033[1;34m'
violetfonce='\033[0;35m'
violetclair='\033[1;35m'
cyanfonce='\033[0;36m'
cyanclair='\033[1;36m'
grisclair='\033[0;37m'
blanc='\033[1;37m'

neutre='\033[0;m'

printf "\n${rougefonce}Si vous exécutez ce script, c'est parce que vous avez upgradé votre vieille QoQ-CoT depuis une version 5.0 vers une version 5.1. Vous avez par ailleurs pris soin de SAUVEGARDER votre base MySQL QoQ-CoT. OK sur ces deux points ? [o/n]${neutre}\n"

while true
do
        read reponse
        case $reponse in
        o|O)
                break
                ;;
        n|N)
                printf "ABORT\n"
                exit 1
                ;;
        *)
                printf "${rougefonce}? [o/n]${neutre}\n"
                ;;
        esac
done

# Chemin relatif vers votre fichier config.php correctement renseigné
CONFIG_PHP="./config.php"

MYSQL_SERVER=$(grep -w SQL_HOST $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
MYSQL_PORT=$(grep -w SQL_PORT $CONFIG_PHP | sed -e "s/[^,]*,[^0-9]*\([0-9]*\).*$/\1/")
QOQ_COT_MYSQL_DB=$(grep -w SQL_DBNAME $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
QOQ_COT_MYSQL_USER=$(grep -w SQL_USERNAME $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
QOQ_COT_MYSQL_USER_PASSWORD=$(grep -w SQL_PASSWORD $CONFIG_PHP | sed -e "s/[^,]*,[^'\"]*['\"]\([^\"']*\).*$/\1/")
CONNEXIONS_CACHE=$((grep "^define('CONNEXIONS_CACHE" $CONFIG_PHP || echo 0 ) | cut -f2 -d, | cut -f1 -d")")

printf "\n${vertclair}Paramètres MySQL récupérés depuis $CONFIG_PHP (ils vont être utilisés pour lancer automatiquement les commandes) :${neutre}\n"
printf -- "--> Serveur MySQL : ${vertclair}$MYSQL_SERVER${neutre}\n"
printf -- "--> Port MySQL : ${vertclair}$MYSQL_PORT${neutre}\n"
printf -- "--> Base MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_DB${neutre}\n"
printf -- "--> Utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER${neutre}\n"
printf -- "--> Mot de passe de l'Utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER_PASSWORD${neutre}\n"

# On recupere le pass root MySQL pour ne le demander qu une fois
printf "\n${rougefonce}Password root MySQL (nécessaire pour certaines commandes) : ${neutre}\n"
read -s mysql_root_password

#################################################
# Modification structure table Connexions       #
#################################################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Lancer la modification de la structure de la table Connexions de la base $QOQ_COT_MYSQL_DB sur le serveur $MYSQL_SERVER
 ? [o/n]${neutre}\n"

while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done

if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then
	if [ "$CONNEXIONS_CACHE" != "0" ]
	then
		SQL_COMMAND="USE $QOQ_COT_MYSQL_DB; ALTER TABLE Connexions ADD Pool VARCHAR(64) NULL AFTER IPVM;ALTER TABLE Connexions_Cache ADD Pool VARCHAR(64) NULL AFTER IPVM;" 
	else
		SQL_COMMAND="USE $QOQ_COT_MYSQL_DB; ALTER TABLE Connexions ADD Pool VARCHAR(64) NULL AFTER IPVM;"
	fi

	printf "On lance la commande suivante : ${vertclair}$SQL_COMMAND${neutre}\n"
	
	mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT -p$mysql_root_password -e "$SQL_COMMAND"
	
	printf "\n${vertclair}OK. La table Connexions de la base $QOQ_COT_MYSQL_DB est désormais 5.1 ready.${neutre}\n"
fi

#####################################################
# FIN Modification structure table Connexions       #
#####################################################

#################################################
# Modification structure table Salles           #
#################################################

JUMP_TO_NEXT_ACTION=0

printf "\n${rougefonce}Lancer la modification de la structure de la table Salles de la base $QOQ_COT_MYSQL_DB sur le serveur $MYSQL_SERVER
 ? [o/n]${neutre}\n"

while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		JUMP_TO_NEXT_ACTION=1
		break
		;;
	*) 
		printf "${rougefonce}? [o/n]${neutre}\n"
		;;
	esac
done

if [ "$JUMP_TO_NEXT_ACTION" = "0" ]
then
	SQL_COMMAND="";
	SQL_COMMAND="USE $QOQ_COT_MYSQL_DB; ALTER TABLE Salles DROP INDEX NomSalle, ADD UNIQUE NomSalle (NomSalle, Composante, Site);"
	printf "On lance la commande suivante : ${vertclair}$SQL_COMMAND${neutre}\n"
	
	mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT -p$mysql_root_password -e "$SQL_COMMAND"
	
	printf "\n${vertclair}OK. La table Salles de la base $QOQ_COT_MYSQL_DB est désormais 5.1 ready.${neutre}\n"
fi

#####################################################
# FIN Modification structure table Salles           #
#####################################################

printf "\n${vertclair}La base $QOQ_COT_MYSQL_DB est prête pour l'expérience 5.1 !${neutre}\n"
