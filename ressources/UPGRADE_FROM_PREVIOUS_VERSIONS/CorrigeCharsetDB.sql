# CORRECTIF TRES IMPORTANT POUR LA PERTINENCE DES DONNEES RECOLTEES 
# Permet de passer en UTF8 "case insensitive" sur les tables de la base, afin que l'ecriture en 
# majuscules ou minuscules (qui s'avère être aléatoire dans les logs remontés par les clients windows...)
# des id de connexions ne gene pas l'appairage des enregistrements debut et fin de connexions

# Correctif a lancer en ligne de commande, par exemple de la facon suivante
#	mysql -h <votre serveur mysql> -u root -p <nom de votre base qoq-cot, par defaut qoq-cot> < CorrigeCharsetDB.sql

alter table Connexions convert to character set utf8 collate utf8_unicode_ci;

alter table Informations convert to character set utf8 collate utf8_unicode_ci;

alter table MachinesToSalles convert to character set utf8 collate utf8_unicode_ci;

alter table Salles convert to character set utf8 collate utf8_unicode_ci;
