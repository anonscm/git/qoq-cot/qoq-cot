CREATE TABLE IF NOT EXISTS `Connexions_tmp` (
  `IdConnexion` bigint(20) NOT NULL auto_increment,
  `DateDebutInitiale` datetime NOT NULL,
  `DateDebut` datetime NOT NULL,
  `HeureDebut` time NOT NULL,
  `DateFin` datetime default NULL,
  `HeureFin` time NOT NULL,
  `Login` varchar(32) NOT NULL,
  `NomMachine` varchar(255) NOT NULL,
  `NomOs` varchar(255) NOT NULL,
  `Duree` time default NULL,
  `IdProcess` varchar(255) NOT NULL,
  `Jour` date NOT NULL,
  `JourSemaine` tinyint(4) NOT NULL,
  PRIMARY KEY  (`IdConnexion`),
  UNIQUE KEY `DateMachineLogin` (`DateDebutInitiale`,`DateDebut`,`NomMachine`,`Login`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO Connexions_tmp SELECT * FROM Connexions ORDER BY IdConnexion ON DUPLICATE KEY UPDATE IdProcess=VALUES(IdProcess),DateFin=VALUES(DateFin),HeureFin=VALUES(HeureFin),Duree=VALUES(Duree);

RENAME TABLE Connexions to Connexions_old;

RENAME TABLE Connexions_tmp to Connexions;
