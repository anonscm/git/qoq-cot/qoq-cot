# Pour créer une tâche planifiée qui va lancer poussindows au démarrage de l'OS
# REMPLACER CHEMIN_VERS_POUSSINDOWS par le chemin complet vers l'exécutable, par exemple C:\poussindows
# Puis lancer 
# - soit une invite de commandes cmd.exe en tant qu'adminstrateur et exécuter la commande suivante :
#   Powershell.exe -ExecutionPolicy Bypass -File lancer_poussindows_au_demarrage.ps1
# - soit un environnement Powershell en tant qu'adminstrateur et copier/coller la commande ci-dessous :
 Register-ScheduledTask `
 -User 'NT AUTHORITY\SYSTEM' `
 -RunLevel Highest `
 -TaskName 'Poussindows' `
 -Trigger $(New-ScheduledTaskTrigger -AtStartup) `
 -Action $(New-ScheduledTaskAction -Execute 'poussindows64.exe' -WorkingDirectory 'c:\CHEMIN_VERS_POUSSINDOWS') `
 -Settings $(New-ScheduledTaskSettingsSet -ExecutionTimeLimit '00:00:00')
