#!/bin/sh
#
#
# Start/stops poussinux
#
#
### BEGIN INIT INFO
# Provides:          poussinux
# Required-Start:    $network
# Required-Stop: 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: poussinux
# Description:
### END INIT INFO

if [ -f /etc/default/poussinuxd ] ; then
	. /etc/default/poussinuxd
else
	exit 0
fi

test -x ${POUSSINUXPATH} || exit 0

case "$1" in
	start)
		echo -n "Starting Poussinux: "
		start-stop-daemon --start --quiet -b -m --pidfile ${POUSSINUXPID} --chdir ${POUSSINUXDIR} --exec ${POUSSINUXPATH}
		echo "done."
		;;
	stop)
		echo -n "Stopping Poussinux: "
		start-stop-daemon --stop --quiet --pidfile ${POUSSINUXPID}
		sleep 1
		if [ -f ${POUSSINUXPID} ] && ! ps h `cat ${POUSSINUXPID}` > /dev/null
		then
			rm -f ${POUSSINUXPID}
		fi
		echo "done."

		;;
	restart)
		$0 stop
		sleep 1
		$0 start
		;;
	status)
		if [ -f ${POUSSINUXPID} ] && ps h `cat ${POUSSINUXPID}` > /dev/null
		then
			echo "Poussinux is running (pid `cat ${POUSSINUXPID}`)."
		else 
			echo "Poussinux is not running."
		fi
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 1
		;;
esac

exit 0
