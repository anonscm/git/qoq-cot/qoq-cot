#!/usr/bin/perl

##########################################################################################
# ********************************* ENGLISH *********************************
#
# --- Copyright notice :
#
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
#
#
# --- Statement of copying permission
#
# This file is part of QoQ-CoT.
#
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#
# *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
#
# --- Notice de Copyright :
#
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
#
#
# --- Déclaration de permission de copie
#
# Ce fichier fait partie de QoQ-CoT.
#
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique Générale GNU telle qu'elle est
# publiée par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (à votre choix) une quelconque version ultérieure.
#
# QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
# pour plus de détails.
#
# Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
# QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
##########################################################################################

#cpan install User::Utmp
#apt install libio-socket-timeout-perl
#apt install libproc-processtable-perl
#apt install libyaml-tiny-perl

use strict;
use User::Utmp qw(:constants :utmpx);
use IO::Socket::INET;
use IO::Socket::Timeout;
use Date::Format;
use Encode qw( decode_utf8 encode_utf8 );
use Sys::Hostname;
use YAML::Tiny;
use Proc::ProcessTable;
use MIME::Base64;

sub encode {
	my ($rep,$k0,$k1,$k2,$k3) = @_;
	my $result="";
	encode_utf8($rep);
	my $s_bytes=unpack "H*",$rep;
	my @bytes = map hex, $s_bytes =~ /../g;
	my $r=8-($#bytes%8);
	for (my $i = 0; $i < $r; $i++) { push (@bytes, 0); }
	for (my $i = 0; $i <= ($#bytes-8) ; $i+=8) {
	my $v0=$bytes[$i+3] + ( $bytes[$i+2]<<8 )+ ($bytes[$i+1]<<16 ) + ($bytes[$i]<<24);
	my $v1=$bytes[$i+7] + ( $bytes[$i+6]<<8 )+ ($bytes[$i+5]<<16 ) + ($bytes[$i+4]<<24);
		my $sum = 0;
		my $n = 0;
		while ($n<32) {
		        $sum += 0x9e3779b9;
		        $v0 += ((($v1<<4)+$k0) ^ ($v1+$sum) ^ ((0x07FFFFFF & ($v1>>5))+$k1));
		        $v0 &= 0xFFFFFFFF;
		        $v1 += ((($v0<<4)+$k2) ^ ($v0+$sum) ^ ((0x07FFFFFF & ($v0>>5))+$k3));
		        $v1 &= 0xFFFFFFFF;
		        $n++;
		}
		$result.=pack("LL",($v0,$v1));
	}
	return encode_base64($result,'');
}

sub PIDExists {
	my ($pid)=@_;
	my $t = Proc::ProcessTable->new;
	foreach my $procs ( @{$t->table} ) {
  		if ($procs->pid == $pid) {
   			return 1; 
   		}
  	}
	return 0;
}

sub timeToSec {
        my ($t)=@_;
        return substr($t,0,2)*3600+substr($t,3,2)*60+substr($t,6,2);
}
sub secToTime {
        use integer;
        my ($t)=@_;
        my $h=$t/3600;
        my $m=($t-$h*3600)/60;
        my $s=$t%60;
        return sprintf("%02d:%02d:%02d",$h,$m,$s);
}

sub timeDiff {
        my ($t1,$t2)=@_;
        return secToTime(timeToSec($t1)-timeToSec($t2));
}

sub isValidEntry {
	my ($vdi,$entry)=@_;
	my $pdate="[0-9]{4}-[0-9]{2}-[0-9]{2}";
	my $ptime="[0-9]{2}:[0-9]{2}:[0-9]{2}";
	my $phost="[0-9A-Za-z_\.\-]+";
	if (defined $vdi) {
		return $entry=~/^$phost $pdate $ptime $pdate $ptime $pdate $ptime $ptime [0-6]+ $phost [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/;
	}
	return $entry=~/^$phost $pdate $ptime $pdate $ptime $pdate $ptime $ptime [0-6]+$/;
}

sub vdiInfo {
	my ($vdi,$lastModify,$host,$ip,$pool)=@_;
	my ($timestamp,$ip_address,$machine_name);
	if ($vdi eq "vmware") {
		if (-f "/var/log/vmware/Environment.txt") {
			$timestamp = (stat("/var/log/vmware/Environment.txt"))[9];
			if ($timestamp > $lastModify) {
				$ip_address="";
				$machine_name="";
				open(my $fh, "<", "/var/log/vmware/Environment.txt");
				while (<$fh>) {
					if ( $_ =~ /^IP_Address: / ) {
						$ip_address=(split(/\s/,$_))[1];
					}
					if ( $_ =~ /^Machine_Name: /) {
						$machine_name=(split(/\s/,$_))[1];
						$machine_name=~ tr/0-9A-Za-z_\.\-//cd;
					}
					if ( (! $pool) &&  $_ =~ /^Launch_ID: /) {
						$pool=(split(/\s/,$_))[1];
					}
					if ($machine_name && $ip_address && $pool) {
						last;
					}
				}
				close ($fh);
				if ($ip_address ne $ip || $machine_name ne $host) {
					$host=$machine_name;
					$ip=$ip_address;
					$lastModify=$timestamp;
				}	
			}
		} else {
			if ($ip ne "0.0.0.0") {
				$lastModify=time();
				$host="-";
				$ip="0.0.0.0";
			}
		}
	}
	if ( ! ($host && $ip) ) {
		$host="-";
		$ip="0.0.0.0";
	}
	return ($lastModify,$host,$ip,$pool);
}

my $host = hostname;
my $user;
my $jourDebutInitial;
my $heureDebutInitiale;
my $jourDebut;
my $heureDebut;
my $jour;
my $heure;
my $duree;
my $jourSemaine;
my %lines;
my $n=1;
my $bufFile = "poussin.buf";
my $key;
my $value;
my $vdiHost;
my $vdiHostIP;
my $vdiLastModify=0;
my $vdiPool="";

my $yaml = YAML::Tiny->read('poussin.yml');
my $tea_keys = $yaml->[0]->{tea_keys};
my $server = $yaml->[0]->{server};
my $frequencies = $yaml->[0]->{frequencies};
my $os = $yaml->[0]->{os};
my $hostname = $yaml->[0]->{hostname};
my $exclude = $yaml->[0]->{exclude};
my $vdi= $yaml->[0]->{vdi};

if ( defined $hostname) {
	$host=$hostname;
}

if (-f $bufFile) {
	open(my $fh, "<", $bufFile);
	while( my $bufLine = <$fh>)  {   
		chomp($bufLine);
		if (isValidEntry($vdi,$bufLine)) {
    			($user,$jourDebutInitial,$heureDebutInitiale,$jourDebut,$heureDebut,$jour,$heure,$duree,$jourSemaine,$vdiHost,$vdiHostIP,$vdiPool)=split(" ",$bufLine);
			$key=$user." ".$jourDebutInitial." ".$heureDebutInitiale." ".$jourDebut." ".$heureDebut;   
			$value=$jour." ".$heure." ".$duree." ".$jourSemaine;
			if (defined $vdi) {
				$value.=" ".$vdiHost." ".$vdiHostIP." ".$vdiPool;
			}
			$lines{$key}=$value;
		}
	}
	close($fh);
}
while (1) {
	my @lt = localtime(time);
	my @utmp = sort {$a->{ut_time} cmp $b->{ut_time}} getutx();
	$jour=strftime("%Y-%m-%d",@lt);
	$heure=strftime("%H:%M:%S",@lt);
	$jourSemaine=strftime("%w",@lt);
	my %users=();
	if (defined $vdi) {
		($vdiLastModify,$vdiHost,$vdiHostIP,$vdiPool)=vdiInfo($vdi,$vdiLastModify,$vdiHost,$vdiHostIP,$vdiPool);
	}
	foreach my $utent (@utmp) {
		# on teste le PID pour ne pas prendre les "gone - no logout"
		# ut_type 7 = USER_PROCESS
		if ( ($utent->{ut_type} == 7) && (PIDExists($utent->{ut_pid})) ) {
			$user=$utent->{ut_user};
			if (defined $vdi) {
				$user.="@".$vdiHost;
			}
			if ( !(defined $users{$user}) && (!(defined $exclude && $utent->{ut_user}=~ /$exclude/i)) ) {
				if (defined $vdi && $vdiLastModify > $utent->{ut_time}) {
					$jourDebutInitial=time2str("%Y-%m-%d",$vdiLastModify);
					$heureDebutInitiale=time2str("%H:%M:%S",$vdiLastModify);
				} else {
					$jourDebutInitial=time2str("%Y-%m-%d",$utent->{ut_time});
					$heureDebutInitiale=time2str("%H:%M:%S",$utent->{ut_time});
				}
				$jourDebut=$jourDebutInitial;
				if ($jourDebut lt $jour) {
                			$jourDebut=$jour;
                			$heureDebut="00:00:00";
                			$duree=$heure;
        			}
        			else {
                			$heureDebut=$heureDebutInitiale;
                			$duree=timeDiff($heure,$heureDebut);
        			}
				$key=$utent->{ut_user}." ".$jourDebutInitial." ".$heureDebutInitiale." ".$jourDebut." ".$heureDebut;
				$value=$jour." ".$heure." ".$duree." ".$jourSemaine;
				if (defined $vdi) {
					$value.=" ".$vdiHost." ".$vdiHostIP." ".$vdiPool;
				}
				$lines{$key}=$value;
				$users{$user}=1;
			}
		}
	}
	if ($n==$frequencies->{send}) {
		if (scalar keys %lines) {
			$| = 1;
			my $connexion=1;
			my $datasent=0;
			my $socket = new IO::Socket::INET (
				PeerHost => $server->{ip},
				PeerPort => $server->{port},
				Proto => 'tcp',
				Timeout => 2,
			) or $connexion=0;
			if ($connexion) { 
				IO::Socket::Timeout->enable_timeouts_on($socket);
				$socket->read_timeout(0.5);
  				$socket->write_timeout(0.5);
				while (($key, $value) = each(%lines)) {
					print $socket encode("$host ".inet_ntoa($socket->sockaddr())." $os $key $value",$tea_keys->{k1},$tea_keys->{k2},$tea_keys->{k3},$tea_keys->{k4})."\n";
				}
				print $socket "FIN\n";
				$datasent=<$socket>;
				chomp($datasent);
				$socket->close();
			}
			if ($datasent != scalar keys %lines) { 
				open(my $fh, ">", $bufFile);
				while (($key, $value) = each(%lines)) {
					print $fh "$key $value\n";
				}
				close($fh);
			}
			else {
				%lines=();
				if (-f $bufFile) {
					unlink($bufFile);
				}
			}
		}
		$n=0;
	}
	$n++;
	sleep $frequencies->{scan};
}
