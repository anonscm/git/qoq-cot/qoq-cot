#!perl

##########################################################################################
# ********************************* ENGLISH *********************************
#
# --- Copyright notice :
#
# Copyright 2013-2021 DOSI AMU (Arnaud Salvucci, Fr�d�ric Bloise, Fr�d�ric Giudicelli, G�rard Milhaud)
#
#
# --- Statement of copying permission
#
# This file is part of QoQ-CoT.
#
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#
# *********** TRADUCTION FRAN�AISE PERSONNELLE SANS VALEUR L�GALE ***********
#
# --- Notice de Copyright :
#
# Copyright 2013-2021 DOSI AMU (Arnaud Salvucci, Fr�d�ric Bloise, Fr�d�ric Giudicelli, G�rard Milhaud)
#
#
# --- D�claration de permission de copie
#
# Ce fichier fait partie de QoQ-CoT.
#
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique G�n�rale GNU telle qu'elle est
# publi�e par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (� votre choix) une quelconque version ult�rieure.
#
# QoQ-CoT est distribu� dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans m�me la garantie implicite de COMMERCIALISATION ou
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique G�n�rale GNU
# pour plus de d�tails.
#
# Vous devriez avoir re�u une copie de la Licence Publique G�n�rale GNU avec
# QoQ-CoT ; si �a n'�tait pas le cas, �crivez � la Free Software Foundation,
##########################################################################################

# Strawberry Perl
#cpan install IO:Socket::Timeout
#

use strict;
use Win32::OLE('in');
use IO::Socket::INET;
use IO::Socket::Timeout;
use Date::Format;
use Date::Parse qw(str2time);
use Encode qw( decode_utf8 encode_utf8 );
use Sys::Hostname;
use YAML::Tiny;
use MIME::Base64;
use Math::BigInt;
use Win32::TieRegistry ( Delimiter=>"/" );

sub encode {
	my ($rep,$k0,$k1,$k2,$k3) = @_;
	my $result="";
	encode_utf8($rep);
	my $s_bytes=unpack "H*",$rep;
	my @bytes = map hex, $s_bytes =~ /../g;
	my $r=8-($#bytes%8);
	for (my $i = 0; $i < $r; $i++) { push (@bytes, 0); }
	for (my $i = 0; $i <= ($#bytes-8) ; $i+=8) {
	my $v0=$bytes[$i+3] + ( $bytes[$i+2]<<8 )+ ($bytes[$i+1]<<16 ) + ($bytes[$i]<<24);
	my $v1=$bytes[$i+7] + ( $bytes[$i+6]<<8 )+ ($bytes[$i+5]<<16 ) + ($bytes[$i+4]<<24);
		my $sum = 0;
		my $n = 0;
		while ($n<32) {
		        $sum += 0x9e3779b9;
		        $v0 += ((($v1<<4)+$k0) ^ ($v1+$sum) ^ ((0x07FFFFFF & ($v1>>5))+$k1));
		        $v0 &= 0xFFFFFFFF;
		        $v1 += ((($v0<<4)+$k2) ^ ($v0+$sum) ^ ((0x07FFFFFF & ($v0>>5))+$k3));
		        $v1 &= 0xFFFFFFFF;
		        $n++;
		}
		$result.=pack("LL",($v0,$v1));
	}
	return encode_base64($result,'');
}

sub PIDExists {
	my ($pid)=@_;
	my $t = Proc::ProcessTable->new;
	foreach my $procs ( @{$t->table} ) {
  		if ($procs->pid == $pid) {
   			return 1; 
   		}
  	}
	return 0;
}

sub timeToSec {
        my ($t)=@_;
        return substr($t,0,2)*3600+substr($t,3,2)*60+substr($t,6,2);
}
sub secToTime {
        use integer;
        my ($t)=@_;
        my $h=$t/3600;
        my $m=($t-$h*3600)/60;
        my $s=$t%60;
        return sprintf("%02d:%02d:%02d",$h,$m,$s);
}

sub timeDiff {
        my ($t1,$t2)=@_;
        return secToTime(timeToSec($t1)-timeToSec($t2));
}

sub isValidEntry {
	my ($vdi,$entry)=@_;
	my $pdate="[0-9]{4}-[0-9]{2}-[0-9]{2}";
	my $ptime="[0-9]{2}:[0-9]{2}:[0-9]{2}";
	my $phost="[0-9A-Za-z_\.\-]+";
	if (defined $vdi) {
		return $entry=~/^$phost $pdate $ptime $pdate $ptime $pdate $ptime $ptime [0-6]+ $phost [0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$/;
	}
	return $entry=~/^$phost $pdate $ptime $pdate $ptime $pdate $ptime $ptime [0-6]+$/;
}

sub vdiInfo {
  my ($vdi,$lastModify,$host,$ip,$pool)=@_;
	my ($timestamp,$ip_address,$machine_name);
	if ($vdi eq "vmware") {
		my $reg=$Registry->{"HKEY_LOCAL_MACHINE/SOFTWARE/VMware, Inc./VMware VDM/SessionData/1"};
		if (defined $reg) {	
			my @filetime=unpack("II",$reg->Information("LastWrite"));
			$timestamp=MSFileTimeToDateTime(@filetime);
			if (! $pool) { $pool=$reg->{"ViewClient_Launch_ID"}; }
			if ($timestamp gt $lastModify) {
				$machine_name=(split(/\s/,$reg->{"ViewClient_Machine_Name"}))[0];
				$ip_address=$reg->{"ViewClient_IP_Address"};
				if ($ip_address ne $ip || $machine_name ne $host) {
					$host=$machine_name;
					$ip=$ip_address;
					$lastModify=$timestamp;
				}	
			}
		} else {
			if ($ip ne "0.0.0.0") {
				my @lt=localtime(time);
				$lastModify=strftime("%Y-%m-%d %H:%M:%S",@lt);
				$host="-";
				$ip="0.0.0.0";
			}
		}
	}
	if ( ! ($host && $ip) ) {
		$host="-";
		$ip="0.0.0.0";
	}
	return ($lastModify,$host,$ip,$pool);
}

sub MSStartTime {
	my ($startTime,$CTZ)=@_;
	my $date=substr($startTime,0,4)."-".substr($startTime,4,2)."-".substr($startTime,6,2)." ".substr($startTime,8,2).":".substr($startTime,10,2).":".substr($startTime,12,2);
	my $offset=substr($startTime,22,3);
	if ($offset ne $CTZ) {
		return time2str("%Y-%m-%d %H:%M:%S",str2time($date)+($CTZ-$offset)*60);
	}
	return $date;
}

sub MSFileTimeToDateTime {
	my ($lsFT,$msFT) = @_;
	my $bias1601 = Math::BigInt->new(11644473600);  
	my $nsecbias = Math::BigInt->new(10000000);    
	my $ls = Math::BigInt->new($lsFT);
	my $ms = Math::BigInt->new($msFT);
	my $epoch = Math::BigInt::numify ((($ms << 32) | $ls) / $nsecbias - $bias1601);
	return time2str("%Y-%m-%d %H:%M:%S",$epoch);
}

sub MSWindowsVersion {
	my ($objWMIService)=@_;
	my $version;
	my $colOs = $objWMIService->ExecQuery("Select * from Win32_OperatingSystem");
	foreach my $objOs (in $colOs) {
		$version=$objOs->{Version};
	}
	if ($version =~ /^5\.1/) {
			return "WindowsXP";
	}
	if ($version =~ /^5\.2/) {
			return "Windows2003";
	}
	if ($version =~ /^6\.0/) {
			return "WindowsVista";
	}
	if ($version =~ /^6\.1/) {
			return "Windows7";
	}	
	if ($version =~ /^6\.2/) {
			return "Windows8";
	}
	if ($version =~ /^6\.3/) {
			return "Windows8.1";
	}
	if ($version =~ /^10\.0\.(.*)/) {
		if ($1 ge "2200") {
			return "Windows11";
		} 
		return "Windows10";
	}
	return "Inconnu";
}

sub MSWindowsCurrentTimeZone {
	my ($objWMIService)=@_;
	my $CTZ;
	my $colCTZ = $objWMIService->ExecQuery("Select * from Win32_ComputerSystem");
	foreach my $objCTZ (in $colCTZ) {
		$CTZ=sprintf("%03d",$objCTZ->{CurrentTimeZone});
	}
	return $CTZ;
}

my $host = hostname;
my $user;
my $jourDebutInitial;
my $heureDebutInitiale;
my $jourDebut;
my $heureDebut;
my $jour;
my $heure;
my $duree;
my $jourSemaine;
my %lines;
my $n=1;
my $bufFile = "poussin.buf";
my $key;
my $value;
my $vdiHost;
my $vdiHostIP;
my $vdiLastModify=0;
my $vdiPool="";
my $CTZ;

my $yaml = YAML::Tiny->read('poussin.yml');
my $tea_keys = $yaml->[0]->{tea_keys};
my $server = $yaml->[0]->{server};
my $frequencies = $yaml->[0]->{frequencies};
my $os = $yaml->[0]->{os};
my $hostname = $yaml->[0]->{hostname};
my $exclude = $yaml->[0]->{exclude};
my $vdi = $yaml->[0]->{vdi};

if ( defined $hostname) {
	$host=$hostname;
}

my $computer = ".";
my $objWMIService = Win32::OLE->GetObject("winmgmts:\\\\$computer\\root\\CIMV2") or die "WMI connection failed.\n";
if ( !defined $os) {
	$os=MSWindowsVersion($objWMIService);
}
$CTZ=MSWindowsCurrentTimeZone($objWMIService);

if (-f $bufFile) {
	open(my $fh, "<", $bufFile);
	while( my $bufLine = <$fh>)  {   
		chomp($bufLine);
		if (isValidEntry($vdi,$bufLine)) {
    		($user,$jourDebutInitial,$heureDebutInitiale,$jourDebut,$heureDebut,$jour,$heure,$duree,$jourSemaine,$vdiHost,$vdiHostIP,$vdiPool)=split(" ",$bufLine);
			$key=$user." ".$jourDebutInitial." ".$heureDebutInitiale." ".$jourDebut." ".$heureDebut;   
			$value=$jour." ".$heure." ".$duree." ".$jourSemaine;
			if (defined $vdi) {
				$value.=" ".$vdiHost." ".$vdiHostIP." ".$vdiPool;
			}
			$lines{$key}=$value;
		}
	}
	close($fh);
}
while (1) {
	my @lt = localtime(time);
	$jour=strftime("%Y-%m-%d",@lt);
	$heure=strftime("%H:%M:%S",@lt);
	$jourSemaine=strftime("%w",@lt);
	if (defined $vdi) {
		($vdiLastModify,$vdiHost,$vdiHostIP,$vdiPool)=vdiInfo($vdi,$vdiLastModify,$vdiHost,$vdiHostIP,$vdiPool);
	}
	my $colLogons = $objWMIService->ExecQuery("Select * from Win32_LogonSession WHERE LogonType=2 OR LogonType=10");
	foreach my $objLogon (in $colLogons){	
		my $colProcesses = $objWMIService->ExecQuery("Associators of {Win32_LogonSession.LogonId=".$objLogon->{LogonId}."} Where Resultclass = Win32_Process Assocclass = Win32_SessionProcess");
		if ( $colProcesses->{count} > 0 ) {
			#Requ�te abandon�e car trop co�teuse en temps
			#my $colConnections=$objWMIService->ExecQuery("Associators of {Win32_LogonSession.LogonId=".$objLogon->{LogonId}."} Where AssocClass=Win32_LoggedOnUser Role=Dependent" );
			my $colConnections=$objWMIService->ExecQuery("Select * From Win32_LoggedOnUser" );
			foreach my $objConnection (in $colConnections) {
				if ($objConnection->{dependent} =~m /LogonId="$objLogon->{LogonId}"/) {
					my $name=$objConnection->{antecedent};
				    $name=~ s/.*,Name="([^"]*)"/$1/;
					if (!(defined $exclude && $name =~ m/$exclude/i)) {
						my $startTime=MSStartTime($objLogon->{StartTime},$CTZ);
						if (defined $vdi && $vdiLastModify gt $startTime) {
							($jourDebutInitial,$heureDebutInitiale)=split(" ",$vdiLastModify);
						} else {
							($jourDebutInitial,$heureDebutInitiale)=split(" ",$startTime);
						}
						$jourDebut=$jourDebutInitial;
						if ($jourDebut lt $jour) {
                				$jourDebut=$jour;
                				$heureDebut="00:00:00";
                				$duree=$heure;
        					}
        					else {
                				$heureDebut=$heureDebutInitiale;
                				$duree=timeDiff($heure,$heureDebut);
        					}
						$key=$name." ".$jourDebutInitial." ".$heureDebutInitiale." ".$jourDebut." ".$heureDebut;
						$value=$jour." ".$heure." ".$duree." ".$jourSemaine;;
						if (defined $vdi) {
							$value.=" ".$vdiHost." ".$vdiHostIP." ".$vdiPool;
						}
						$lines{$key}=$value;
					}
				}
			}
		}
	}
	if ($n==$frequencies->{send}) {
		if (scalar keys %lines) {
			$| = 1;
			my $connexion=1;
			my $datasent=0;
			my $socket = new IO::Socket::INET (
				PeerHost => $server->{ip},
				PeerPort => $server->{port},
				Proto => 'tcp',
				Timeout => 2,
			) or $connexion=0;
			if ($connexion) { 
				IO::Socket::Timeout->enable_timeouts_on($socket);
				$socket->read_timeout(0.5);
  				$socket->write_timeout(0.5);
				while (($key, $value) = each(%lines)) {
					print $socket encode("$host ".inet_ntoa($socket->sockaddr())." $os $key $value",$tea_keys->{k1},$tea_keys->{k2},$tea_keys->{k3},$tea_keys->{k4})."\n";
				}
				print $socket "FIN\n";
				$datasent=<$socket>;
				chomp($datasent);
				$socket->close();
			}
			if ($datasent != scalar keys %lines) { 
				open(my $fh, ">", $bufFile);
				while (($key, $value) = each(%lines)) {
					print $fh "$key $value\n";
				}
				close($fh);
			}
			else {
				%lines=();
				if (-f $bufFile) {
					unlink($bufFile);
				}
			}
		}
		$n=0;
	}
	$n++;
	sleep $frequencies->{scan};
}
