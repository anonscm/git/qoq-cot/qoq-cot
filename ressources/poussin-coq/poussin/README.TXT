* Installation et configuration du poussin

La méthode va varier selon les OS, mais l'idée est toujours la même :
1. copier le binaire du poussin sur la machine cliente depuis le répertoire correspondant à l'OS de ladite machine ;
2. copier au même endroit le fichier de configuration du poussin conf/poussin.yml et le renseigner à l'aide des indications qu'il contient ;
3. faire en sorte que le poussin soit lancé au démarrage de la machine en utilisant les fichiers fournis dans le répertoire associé à l'OS.

Tous les détails sont dans les fichiers README.TXT des répertoires correspondant aux différents OS.

Le répertoire SOURCES contient les sources des poussins. Si vous souhaitez les compiler, il faut installer les différents (et nombreux...) modules Perl nécessaires puis utiliser PerlPacker pour tout englober dans un binaire, c'est comme ça que nous avons procédé pour créer les binaires fournis...
