#!/bin/sh
#
#
# Start/stops coq
#
#
### BEGIN INIT INFO
# Provides:          coq
# Required-Start:    $network
# Required-Stop: 
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: coq
# Description:
### END INIT INFO

if [ -f /etc/default/coqd ] ; then
	. /etc/default/coqd
else
	exit 0
fi

test -x ${COQPATH} || exit 0

case "$1" in
	start)
		if [ ! -e ${COQLOG} ]; then
			touch ${COQLOG}
			chown ${COQUSER}: ${COQLOG}
		fi
		echo -n "Starting Coq: "
		start-stop-daemon --start --quiet -b -m -c ${COQUSER} --pidfile ${COQPID} --chdir ${COQDIR} --startas /bin/bash -- -c "exec ${COQPATH} >> ${COQLOG}"
		echo "done."
		;;
	stop)
		echo -n "Stopping Coq: "
		start-stop-daemon --stop --quiet --pidfile ${COQPID}
		sleep 1
		if [ -f ${COQPID} ] && ! ps h `cat ${COQPID}` > /dev/null
		then
			rm -f ${COQPID}
		fi
		echo "done."

		;;
	restart)
		$0 stop
		sleep 1
		$0 start
		;;
	status)
		if [ -f ${COQPID} ] && ps h `cat ${COQPID}` > /dev/null
		then
			echo "Coq is running (pid `cat ${COQPID}`)."
		else 
			echo "Coq is not running."
		fi
		;;
	*)
		echo "Usage: $0 {start|stop|restart|status}"
		exit 1
		;;
esac

exit 0
