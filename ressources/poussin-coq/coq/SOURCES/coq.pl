#!/usr/bin/perl -w

##########################################################################################
# ********************************* ENGLISH *********************************
#
# --- Copyright notice :
#
#Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
#
#
# --- Statement of copying permission
#
# This file is part of QoQ-CoT.
#
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
#
# *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
#
# --- Notice de Copyright :
#
#Copyright 2013-2024 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
#
#
# --- Déclaration de permission de copie
#
# Ce fichier fait partie de QoQ-CoT.
#
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique Générale GNU telle qu'elle est
# publiée par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (à votre choix) une quelconque version ultérieure.
#
# QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
# pour plus de détails.
#
# Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec
# QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
##########################################################################################

use strict;
use warnings;
use IO::Socket;
use Encode qw( decode_utf8 encode_utf8 );
use DBI;
use YAML::Tiny;
use Data::Dumper;
use MIME::Base64;
use POSIX qw( strftime );
use integer;

$SIG{CHLD} = 'IGNORE';

sub decode  {
	my ($rep,$k0,$k1,$k2,$k3) = @_;
	my @v=unpack("L*",decode_base64($rep));
	my $l=@v;
	if ($l<1) {
		return 0;
	}
	my $r_utf8="";
	for (my $i=0;$i<$l;$i+=2) {
		my $v0=$v[$i];
		my $v1=$v[$i+1];
		my $sum = 0; my $n = 0;
		$sum = 0x9e3779b9 << 5 ;
		while ($n<32) {
			$v1 -= (($v0<<4)+$k2) ^ ($v0+$sum) ^ ((0x07FFFFFF & ($v0>>5))+$k3) ;
			$v1 &= 0xFFFFFFFF;
			$v0 -= (($v1<<4)+$k0) ^ ($v1+$sum) ^ ((0x07FFFFFF & ($v1>>5))+$k1) ;
			$v0 &= 0xFFFFFFFF;
			$sum -= 0x9e3779b9 ;
			$n++;
		}
		$r_utf8.=sprintf("%08x%08x",$v0,$v1);
	}
	my @bytes = map hex, $r_utf8 =~ /../g;
	while ( ! $bytes[$#bytes] ) { pop (@bytes); }
	my $b_utf8 = join '', map chr, @bytes;
	decode_utf8($b_utf8);
	return $b_utf8;
}

sub isValidEntry {
	my ($entry)=@_;
	my $phost="[0-9A-Za-z_\.\-]+";
	my $pip="[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}";
	my $pos="[0-9A-Za-z\._]+";
	my $plogin="[0-9A-Za-z_\.\-]+";
	my $pdate="[0-9]{4}-[0-9]{2}-[0-9]{2}";
	my $ptime="[0-9]{2}:[0-9]{2}:[0-9]{2}";
	my $pool="[0-9A-Za-z_\.\-]+";
	return $entry=~/^$phost $pip $pos $plogin $pdate $ptime $pdate $ptime $pdate $ptime $ptime [0-6](| $phost $pip| $phost $pip $pool)$/;
}

my $yaml = YAML::Tiny->read('coq.yml');
my $server = $yaml->[0]->{server};
my $tea_keys = $yaml->[0]->{tea_keys};
my $db = $yaml->[0]->{db};
my $exclude = $yaml->[0]->{exclude};

my $serveur = IO::Socket::INET->new(LocalPort => $server->{port},
                                    Type      => SOCK_STREAM,
                                    Reuse     => 1,
                                    Listen    => 10)
or die "Impossible serveur tcp sur le port $server->{port} : $@\n";

$| = 1;

my ($hote,$hoteIP,$vdiHote,$vdiHoteIP,$vdiHotePool,$os,$login,$jourDebutInitial,$heureDebutInitiale,$jour,$heure,$jourDebut,$heureDebut,$duree,$jourSemaine,$sqlQuery);

while (my $client = $serveur->accept()) {
	next if my $pid = fork;
	my $connexion=1;
	my $insert=0;
	my $dbh=DBI->connect("dbi:mysql:database=".$db->{SQL_DBNAME}.";host=".$db->{SQL_HOST}.";port=".$db->{SQL_PORT}.";mysql_connect_timeout=1",$db->{SQL_USERNAME},$db->{SQL_PASSWORD}) or $connexion=0;
	if ($connexion) {
		while (my $rep = <$client>) {
			if ($rep =~ /FIN/ ) {
				print $client "$insert\n";
			}
			else {
				my $entry=decode($rep,$tea_keys->{k1},$tea_keys->{k2},$tea_keys->{k3},$tea_keys->{k4});
				if (isValidEntry($entry)) {
					($hote,$hoteIP,$os,$login,$jourDebutInitial,$heureDebutInitiale,$jourDebut,$heureDebut,$jour,$heure,$duree,$jourSemaine,$vdiHote,$vdiHoteIP,$vdiHotePool)=split(/ /,$entry);
					if (!(defined $exclude && $login=~m/$exclude/i)) {
						if (defined $vdiHote) {
							if (defined $vdiHotePool) {
								$sqlQuery = "INSERT INTO Connexions(DateDebutInitiale,DateDebut,HeureDebut,DateFin,HeureFin,Login,NomMachine,IPMAchine,NomOs,Duree,Jour,JourSemaine,NomVM,IPVM,Pool) VALUES ('$jourDebutInitial $heureDebutInitiale','$jourDebut $heureDebut','$heureDebut','$jour $heure','$heure','$login','$vdiHote','$vdiHoteIP','$os','$duree','$jour','$jourSemaine','$hote','$hoteIP','$vdiHotePool') ON DUPLICATE KEY UPDATE DateFin='$jour $heure',HeureFin='$heure',Duree='$duree'";
							} else {
							$sqlQuery = "INSERT INTO Connexions(DateDebutInitiale,DateDebut,HeureDebut,DateFin,HeureFin,Login,NomMachine,IPMAchine,NomOs,Duree,Jour,JourSemaine,NomVM,IPVM) VALUES ('$jourDebutInitial $heureDebutInitiale','$jourDebut $heureDebut','$heureDebut','$jour $heure','$heure','$login','$vdiHote','$vdiHoteIP','$os','$duree','$jour','$jourSemaine','$hote','$hoteIP') ON DUPLICATE KEY UPDATE DateFin='$jour $heure',HeureFin='$heure',Duree='$duree'";
							}
						} else {
							$sqlQuery = "INSERT INTO Connexions(DateDebutInitiale,DateDebut,HeureDebut,DateFin,HeureFin,Login,NomMachine,IPMAchine,NomOs,Duree,Jour,JourSemaine) VALUES ('$jourDebutInitial $heureDebutInitiale','$jourDebut $heureDebut','$heureDebut','$jour $heure','$heure','$login','$hote','$hoteIP','$os','$duree','$jour','$jourSemaine') ON DUPLICATE KEY UPDATE DateFin='$jour $heure',HeureFin='$heure',Duree='$duree'"; 
						}
						my $sth=$dbh->prepare($sqlQuery);
						my $succes=1;
  						$sth->execute() or $succes=0;
  						$sth->finish();
						$insert+=$succes;
					} else {
						$insert++;
					}
				}
				else {
					printf("%s Paquet mal formé : %s.\n",(strftime "[%d/%m/%Y %H:%M:%S]" , localtime),$entry);
					$insert++;
				}
			}
		}
	} else {
		print $client "0\n";
		printf("%s Connexion MySQL Impossible \n",(strftime "[%d/%m/%Y %H:%M:%S]" , localtime));
	}
	$dbh->disconnect();
	close($client);
	exit 0;
}
close($serveur);
