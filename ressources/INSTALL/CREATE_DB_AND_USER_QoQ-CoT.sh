#!/bin/bash

##########################################################################################
# ********************************* ENGLISH *********************************
# 
# --- Copyright notice :
# 
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Statement of copying permission
# 
# This file is part of QoQ-CoT.
# 
# QoQ-CoT is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
# 
# QoQ-CoT is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with QoQ-CoT; if not, write to the Free Software
# Foundation, 51 Franklin Street, Fifth Floor Boston, MA 02110-1301 USA
# 
# *********** TRADUCTION FRANÇAISE PERSONNELLE SANS VALEUR LÉGALE ***********
#
# --- Notice de Copyright :
# 
#Copyright 2013-2021 DOSI AMU & al. (Frédéric Bloise, Hugo Di Giovanni, Frédéric Giudicelli, Gérard Milhaud, Arnaud Salvucci)
# 
# 
# --- Déclaration de permission de copie
# 
# Ce fichier fait partie de QoQ-CoT.
# 
# QoQ-CoT est un logiciel libre : vous pouvez le redistribuer ou le modifier
# selon les termes de la Licence Publique Générale GNU telle qu'elle est
# publiée par la Free Software Foundation ; soit la version 3 de la Licence,
# soit (à votre choix) une quelconque version ultérieure.
# 
# QoQ-CoT est distribué dans l'espoir qu'il soit utile, mais SANS AUCUNE
# GARANTIE ; sans même la garantie implicite de COMMERCIALISATION ou 
# d'ADAPTATION DANS UN BUT PARTICULIER. Voir la Licence publique Générale GNU
# pour plus de détails.
# 
# Vous devriez avoir reçu une copie de la Licence Publique Générale GNU avec 
# QoQ-CoT ; si ça n'était pas le cas, écrivez à la Free Software Foundation,
##########################################################################################

##########################################################################################
## SCRIPT DE CREATION DE LA BASE DE DONNÉES MYSQL QoQ-CoT ET DE SON UTILISATEUR ASSOCIÉ ##
##########################################################################################

# Chemin relatif vers votre fichier config.php correctement renseigné
CONFIG_PHP="./config.php"

MYSQL_SERVER=$(grep -w SQL_HOST $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")
MYSQL_PORT=$(grep -w SQL_PORT $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")
QOQ_COT_MYSQL_DB=$(grep -w SQL_DBNAME $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")
QOQ_COT_MYSQL_USER=$(grep -w SQL_USERNAME $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")
QOQ_COT_MYSQL_USER_PASSWORD=$(grep -w SQL_PASSWORD $CONFIG_PHP | sed -e "s/[^,]*,[^']*'\([^']*\)'.*$/\1/")

noir='\033[0;30m'
gris='\033[1;30m'
rougefonce='\033[0;31m'
rose='\033[1;31m'
vertfonce='\033[0;32m'
vertclair='\033[1;32m'
orange='\033[0;33m'
jaune='\033[1;33m'
bleufonce='\033[0;34m'
bleuclair='\033[1;34m'
violetfonce='\033[0;35m'
violetclair='\033[1;35m'
cyanfonce='\033[0;36m'
cyanclair='\033[1;36m'
grisclair='\033[0;37m'
blanc='\033[1;37m'

neutre='\033[0;m'

printf "\n${vertclair}Paramètres MySQL récupérés depuis $CONFIG_PHP :${neutre}\n"
printf -- "--> Serveur MySQL : ${vertclair}$MYSQL_SERVER${neutre}\n"
printf -- "--> Port MySQL : ${vertclair}$MYSQL_PORT${neutre}\n"
printf -- "--> Base MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_DB${neutre}\n"
printf -- "--> Utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER${neutre}\n"
printf -- "--> Password utilisateur MySQL QoQ-CoT : ${vertclair}$QOQ_COT_MYSQL_USER_PASSWORD${neutre}\n"

printf "\n${rougefonce}Lancer la création de la base $QOQ_COT_MYSQL_DB et de l'utilisateur $QOQ_COT_MYSQL_USER avec les droits SELECT, INSERT, UPDATE, CREATE, DROP et TRIGGER sur $QOQ_COT_MYSQL_DB, sur le serveur $MYSQL_SERVER ? [o/n]${neutre}\n"

while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		printf "ABORT\n"
		exit 1
		;;
	*) 
		printf "${rougefonce}? [o/n]\n${neutre}"
		;;
	esac
done

SQL_COMMAND="CREATE DATABASE IF NOT EXISTS $QOQ_COT_MYSQL_DB;";


printf "${rougefonce}Nom DNS complet du serveur web QoQ-CoT (pour autoriser l'écriture dans la base depuis cette machine), ou localhost si le serveur web tourne sur la même machine que le serveur MySQL :${neutre}\n"
while true
do
	read DNS_QoQ_CoT_WEB_SERVER
	printf "${vertclair}Serveur web = $DNS_QoQ_CoT_WEB_SERVER. OK ? [o/n]${neutre}\n"
	read reponse
	case $reponse in
	o|O)
		break
		;;
	*) 
		printf "${rougefonce}Nom DNS complet du serveur web QoQ-CoT :${neutre}\n"
		;;
	esac
done
if [ "$DNS_QoQ_CoT_WEB_SERVER" = "localhost" ] 
then
	SQL_COMMAND="$SQL_COMMAND CREATE USER '$QOQ_COT_MYSQL_USER'@'localhost' IDENTIFIED BY '$QOQ_COT_MYSQL_USER_PASSWORD'; GRANT SELECT,INSERT,DELETE,UPDATE,CREATE,DROP,TRIGGER,EVENT ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'localhost';" 
else
	SQL_COMMAND="$SQL_COMMAND CREATE USER '$QOQ_COT_MYSQL_USER'@'$DNS_QoQ_CoT_WEB_SERVER' IDENTIFIED BY '$QOQ_COT_MYSQL_USER_PASSWORD'; GRANT SELECT,INSERT,DELETE,UPDATE,CREATE,DROP,TRIGGER,EVENT ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'$DNS_QoQ_CoT_WEB_SERVER';" 
fi

printf "${rougefonce}Nom DNS complet de la machine où tourne le coq (pour autoriser l'écriture dans la base depuis cette machine), ou localhost si le serveur web tourne sur la même machine que le serveur MySQL :${neutre}\n"
while true
do
	read DNS_COQ
	printf "${vertclair}Coq = $DNS_COQ. OK ? [o/n]${neutre}\n"
	read reponse
	case $reponse in
	o|O)
		break
		;;
	*) 
		printf "${rougefonce}Nom DNS complet de la machine où tourne le coq :${neutre}\n"
		;;
	esac
done
if [ $DNS_COQ != $DNS_QoQ_CoT_WEB_SERVER ]
then
	if [ $DNS_COQ = "localhost" ]
	then
	        SQL_COMMAND="$SQL_COMMAND CREATE USER '$QOQ_COT_MYSQL_USER'@'localhost' IDENTIFIED BY '$QOQ_COT_MYSQL_USER_PASSWORD'; GRANT SELECT,INSERT,DELETE,UPDATE,CREATE,DROP,TRIGGER ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'localhost';"
	else
	        SQL_COMMAND="$SQL_COMMAND CREATE USER '$QOQ_COT_MYSQL_USER'@'$DNS_COQ' IDENTIFIED BY '$QOQ_COT_MYSQL_USER_PASSWORD'; GRANT SELECT,INSERT,DELETE,UPDATE,CREATE,DROP,TRIGGER ON $QOQ_COT_MYSQL_DB.* TO '$QOQ_COT_MYSQL_USER'@'$DNS_COQ';"
	fi
fi

printf "On va lancer la commande suivante : ${vertclair}$SQL_COMMAND${neutre}\n"
printf "\n${rougefonce}On y va pour de vrai ? [o/n]${neutre}\n"
while true
do
	read reponse
	case $reponse in
	o|O)
		break
		;;
	n|N) 
		printf "ABORT\n"
		exit 1
		;;
	*) 
		printf "${rougefonce}? [o/n]\n${neutre}"
		;;
	esac
done
printf "\n${rougefonce}C'est parti, saisissez le password root MySQL au prompt...${neutre}\n"
printf "mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT --password -e "
printf "$SQL_COMMAND\n"
mysql -u root -h $MYSQL_SERVER -P $MYSQL_PORT --password -e "$SQL_COMMAND"

printf "\n${vertclair}And voilà, your QoQ-CoT DB is born and ready to serve. Congratulations !!!${neutre}\n"
